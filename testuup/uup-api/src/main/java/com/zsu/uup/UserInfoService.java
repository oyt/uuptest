package com.zsu.uup;

import java.util.List;
import java.util.Map;

import com.zsu.uup.entity.UserInfo;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 
 * <p>
 * Title: UserInfoService
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Kouxu (Create on:2018年6月19日)
 * @version 1.0
 * @fileName UserInfoService.java
 */
public interface UserInfoService {
	/**
	 * 
	 * <p>Title: getUserInfoList(个人资料审核   未审核)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public PageInfo<UserInfo> getUserInfoList(Map<String, String> map);
	/**
	 * 
	 * <p>Title: getUserInfoListById(根据ID获取个人审核资料详情)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @param id
	 * @return
	 */
	public List<UserInfo> getUserInfoListById(Integer id);
	/**
	 * 
	 * <p>Title: getUserInfoListEnd(个人审核资料   已审核)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	public PageInfo<UserInfo> getUserInfoListEnd(Map<String,String> map);
	/**
	 * 
	 * <p>Title: getUserInfoAllthing(获取平台账号列表)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public PageInfo<UserInfo> getUserInfoAllthing(Map<String, String> map);
	
	/**
	 * 
	 * <p>Title: queryLoginName</p>  
	 * 根据用户名查询用户对象
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map 用户名/用户类型
	 * @return
	 */
	public UserInfo queryLoginName(Map<String,Object> map);
	
	/**
	 * <p>Title: queryLoginInfo</p>  
	 * 根据登录信息查询用户对象
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map 用户名/密码/用户类型
	 * @return
	 */
	public UserInfo queryLoginInfo(Map<String,Object> map);
	
	/**
	 * <p>Title: insertUserRegister</p>  
	 * 保存注册信息
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @param userInfoDto注册账号/密码用户对象
	 * @return
	 */
	public Integer insertUserRegister(UserInfo userInfoDto);
	
	/**
	 * <p>Title: updateUserPassWd</p>  
	 *根据用户id更新用户数据
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @param userInfoDto 用户对象
	 * @return
	 */
	public Integer updateUserPassWd(UserInfo userInfoDto);
	
	/**
	 * <p>Title: updateUserPassWd</p>  
	 * 根据用户名称和类型更新密码
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @param item 用户名/用户类型/密码
	 */
	public void updateUserPassWd(Map<String,Object> item);
}
