package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Table(name = "zu_uup_product_info")
public class ProductInfo  implements Serializable {
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 行业分类ID
     */
    @Column(name = "CATEGORY_ID")
    private String categoryId;

    /**
     * 行业分类名称
     */
    @Column(name = "CATEGORY_NAME")
    private String categoryName;

    /**
     * 产品符号ID
     */
    @Column(name = "PRODUCT_SYMBOL_ID")
    private String productSymbolId;

    /**
     * 产品符号名称
     */
    @Column(name = "PRODUCT_SYMBOL_NAME")
    private String productSymbolName;

    /**
     * 中文名称
     */
    @Column(name = "PRODUCT_NAME_ZH")
    private String productNameZh;

    /**
     * 英文名称
     */
    @Column(name = "PRODUCT_NAME_EN")
    private String productNameEn;

    /**
     * 生产厂家ID
     */
    @Column(name = "MFRS_FACTORY_ID")
    private String mfrsFactoryId;

    /**
     * 生产厂家名称
     */
    @Column(name = "MFRS_FACTORY_NAME")
    private String mfrsFactoryName;

    /**
     * 生产厂家简称
     */
    @Column(name = "MFRS_FACTORY_SHORT")
    private String mfrsFactoryShort;

    /**
     * 生产厂家（英文）
     */
    @Column(name = "MFRS_FACTORY_EN")
    private String mfrsFactoryEn;

    /**
     * 品牌ID
     */
    @Column(name = "PRODUCT_BRAND_ID")
    private String productBrandId;

    /**
     * 品牌名称
     */
    @Column(name = "PRODUCT_BRAND_NAME")
    private String productBrandName;

    /**
     * 牌号（唯一性）
     */
    @Column(name = "PRODUCT_SPLIT_CODE")
    private String productSplitCode;

    /**
     * 产品图片
     */
    @Column(name = "PRODUCT_PIC")
    private String productPic;

    /**
     * 状态（0 草稿 1 下架 2 上架）
     */
    @Column(name = "PRODUCT_STATE")
    private Integer productState;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 产品说明
     */
    @Column(name = "PRODUCT_EXPLAIN")
    private String productExplain;
    /**
     * 
     */
   
	private static final long serialVersionUID = 1L;

    public ProductInfo(Integer id, String uuid, String categoryId, String categoryName, String productSymbolId, String productSymbolName, String productNameZh, String productNameEn, String mfrsFactoryId, String mfrsFactoryName, String mfrsFactoryShort, String mfrsFactoryEn, String productBrandId, String productBrandName, String productSplitCode, String productPic, Integer productState, Date createDate, String createUser, Date updateDate, String updateUser, String productExplain) {
        this.id = id;
        this.uuid = uuid;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.productSymbolId = productSymbolId;
        this.productSymbolName = productSymbolName;
        this.productNameZh = productNameZh;
        this.productNameEn = productNameEn;
        this.mfrsFactoryId = mfrsFactoryId;
        this.mfrsFactoryName = mfrsFactoryName;
        this.mfrsFactoryShort = mfrsFactoryShort;
        this.mfrsFactoryEn = mfrsFactoryEn;
        this.productBrandId = productBrandId;
        this.productBrandName = productBrandName;
        this.productSplitCode = productSplitCode;
        this.productPic = productPic;
        this.productState = productState;
        this.createDate = createDate;
        this.createUser = createUser;
        this.updateDate = updateDate;
        this.updateUser = updateUser;
        this.productExplain = productExplain;
    }

    public ProductInfo() {
        super();
    }

    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取行业分类ID
     *
     * @return CATEGORY_ID - 行业分类ID
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 设置行业分类ID
     *
     * @param categoryId 行业分类ID
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId == null ? null : categoryId.trim();
    }

    /**
     * 获取行业分类名称
     *
     * @return CATEGORY_NAME - 行业分类名称
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 设置行业分类名称
     *
     * @param categoryName 行业分类名称
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName == null ? null : categoryName.trim();
    }

    /**
     * 获取产品符号ID
     *
     * @return PRODUCT_SYMBOL_ID - 产品符号ID
     */
    public String getProductSymbolId() {
        return productSymbolId;
    }

    /**
     * 设置产品符号ID
     *
     * @param productSymbolId 产品符号ID
     */
    public void setProductSymbolId(String productSymbolId) {
        this.productSymbolId = productSymbolId == null ? null : productSymbolId.trim();
    }

    /**
     * 获取产品符号名称
     *
     * @return PRODUCT_SYMBOL_NAME - 产品符号名称
     */
    public String getProductSymbolName() {
        return productSymbolName;
    }

    /**
     * 设置产品符号名称
     *
     * @param productSymbolName 产品符号名称
     */
    public void setProductSymbolName(String productSymbolName) {
        this.productSymbolName = productSymbolName == null ? null : productSymbolName.trim();
    }

    /**
     * 获取中文名称
     *
     * @return PRODUCT_NAME_ZH - 中文名称
     */
    public String getProductNameZh() {
        return productNameZh;
    }

    /**
     * 设置中文名称
     *
     * @param productNameZh 中文名称
     */
    public void setProductNameZh(String productNameZh) {
        this.productNameZh = productNameZh == null ? null : productNameZh.trim();
    }

    /**
     * 获取英文名称
     *
     * @return PRODUCT_NAME_EN - 英文名称
     */
    public String getProductNameEn() {
        return productNameEn;
    }

    /**
     * 设置英文名称
     *
     * @param productNameEn 英文名称
     */
    public void setProductNameEn(String productNameEn) {
        this.productNameEn = productNameEn == null ? null : productNameEn.trim();
    }

    /**
     * 获取生产厂家ID
     *
     * @return MFRS_FACTORY_ID - 生产厂家ID
     */
    public String getMfrsFactoryId() {
        return mfrsFactoryId;
    }

    /**
     * 设置生产厂家ID
     *
     * @param mfrsFactoryId 生产厂家ID
     */
    public void setMfrsFactoryId(String mfrsFactoryId) {
        this.mfrsFactoryId = mfrsFactoryId == null ? null : mfrsFactoryId.trim();
    }

    /**
     * 获取生产厂家名称
     *
     * @return MFRS_FACTORY_NAME - 生产厂家名称
     */
    public String getMfrsFactoryName() {
        return mfrsFactoryName;
    }

    /**
     * 设置生产厂家名称
     *
     * @param mfrsFactoryName 生产厂家名称
     */
    public void setMfrsFactoryName(String mfrsFactoryName) {
        this.mfrsFactoryName = mfrsFactoryName == null ? null : mfrsFactoryName.trim();
    }

    /**
     * 获取生产厂家简称
     *
     * @return MFRS_FACTORY_SHORT - 生产厂家简称
     */
    public String getMfrsFactoryShort() {
        return mfrsFactoryShort;
    }

    /**
     * 设置生产厂家简称
     *
     * @param mfrsFactoryShort 生产厂家简称
     */
    public void setMfrsFactoryShort(String mfrsFactoryShort) {
        this.mfrsFactoryShort = mfrsFactoryShort == null ? null : mfrsFactoryShort.trim();
    }

    /**
     * 获取生产厂家（英文）
     *
     * @return MFRS_FACTORY_EN - 生产厂家（英文）
     */
    public String getMfrsFactoryEn() {
        return mfrsFactoryEn;
    }

    /**
     * 设置生产厂家（英文）
     *
     * @param mfrsFactoryEn 生产厂家（英文）
     */
    public void setMfrsFactoryEn(String mfrsFactoryEn) {
        this.mfrsFactoryEn = mfrsFactoryEn == null ? null : mfrsFactoryEn.trim();
    }

    /**
     * 获取品牌ID
     *
     * @return PRODUCT_BRAND_ID - 品牌ID
     */
    public String getProductBrandId() {
        return productBrandId;
    }

    /**
     * 设置品牌ID
     *
     * @param productBrandId 品牌ID
     */
    public void setProductBrandId(String productBrandId) {
        this.productBrandId = productBrandId == null ? null : productBrandId.trim();
    }

    /**
     * 获取品牌名称
     *
     * @return PRODUCT_BRAND_NAME - 品牌名称
     */
    public String getProductBrandName() {
        return productBrandName;
    }

    /**
     * 设置品牌名称
     *
     * @param productBrandName 品牌名称
     */
    public void setProductBrandName(String productBrandName) {
        this.productBrandName = productBrandName == null ? null : productBrandName.trim();
    }

    /**
     * 获取牌号（唯一性）
     *
     * @return PRODUCT_SPLIT_CODE - 牌号（唯一性）
     */
    public String getProductSplitCode() {
        return productSplitCode;
    }

    /**
     * 设置牌号（唯一性）
     *
     * @param productSplitCode 牌号（唯一性）
     */
    public void setProductSplitCode(String productSplitCode) {
        this.productSplitCode = productSplitCode == null ? null : productSplitCode.trim();
    }

    /**
     * 获取产品图片
     *
     * @return PRODUCT_PIC - 产品图片
     */
    public String getProductPic() {
        return productPic;
    }

    /**
     * 设置产品图片
     *
     * @param productPic 产品图片
     */
    public void setProductPic(String productPic) {
        this.productPic = productPic == null ? null : productPic.trim();
    }

    /**
     * 获取状态（0 草稿 1 下架 2 上架）
     *
     * @return PRODUCT_STATE - 状态（0 草稿 1 下架 2 上架）
     */
    public Integer getProductState() {
        return productState;
    }

    /**
     * 设置状态（0 草稿 1 下架 2 上架）
     *
     * @param productState 状态（0 草稿 1 下架 2 上架）
     */
    public void setProductState(Integer productState) {
        this.productState = productState;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_DATE - 修改时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取修改人
     *
     * @return UPDATE_USER - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取产品说明
     *
     * @return PRODUCT_EXPLAIN - 产品说明
     */
    public String getProductExplain() {
        return productExplain;
    }

    /**
     * 设置产品说明
     *
     * @param productExplain 产品说明
     */
    public void setProductExplain(String productExplain) {
        this.productExplain = productExplain == null ? null : productExplain.trim();
    }
}