package com.zsu.uup;

import java.util.List;
import java.util.Map;

import com.zsu.uup.entity.ProductInfo;
import com.zsu.uup.pageinfo.PageInfo;

public interface ProductInfoService {
	/**
	 * 
	 * <p>Title: getAllProductInfo</p>  
	 * 		获得所有的产品信息
	 * @author WangShiPing (Create on:2018年6月19日)
	 * @version 1.0
	 * @return	产品信息集合
	 */
	List<ProductInfo> getAllProductInfo();
	 /**
     * <p>
     * 
     * </p>
     * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
     * @author WangShiPing (Create on:2018年6月19日)
     * @version 1.0
     * @param map 
     * @return	PageInfo分页插件，里面数据是产品信息
     */
	PageInfo<ProductInfo> getListProductInfo(Map<String, String> map);
	/**
	 * 
	 * <p>Title: getProductInfoAndGeneralDes</p>  
	 * 		查询出产品信息，总体描述，物理性能等
	 * @author WangShiPing (Create on:2018年6月20日)
	 * @version 1.0
	 * @param id	根据ID查询出产品信息
	 * @return
	 */
	int getProductInfoAndGeneralDes(int id);
	
}
