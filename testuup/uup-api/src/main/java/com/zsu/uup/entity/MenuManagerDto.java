package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Table(name = "zs_uup_menu_manager")
public class MenuManagerDto implements Serializable {
	@Id
	@Column(name = "ID")
	private Integer id;

	@Column(name = "UUID")
	private String uuid;

	/**
	 * 菜单名称
	 */
	@Column(name = "MENU_NAME")
	private String menuName;

	/**
	 * 上级编号
	 */
	@Column(name = "PARENT_ID")
	private String parentId;

	/**
	 * 排序
	 */
	@Column(name = "MENU_SORT")
	private Integer menuSort;

	/**
	 * 菜单链接
	 */
	@Column(name = "LINK_CODE")
	private String linkCode;

	/**
	 * 菜单状态（用户中心/运营中心）
	 */
	@Column(name = "STATE")
	private Integer state;

	/**
	 * 所属系统
	 */
	@Column(name = "BELONGS_SYS")
	private String belongsSys;

	/**
	 * 创建时间
	 */
	@Column(name = "CREATE_DATE")
	private Date createDate;

	/**
	 * 创建人
	 */
	@Column(name = "CREATE_USER")
	private String createUser;

	/**
	 * 修改时间
	 */
	@Column(name = "UPDATE_DATE")
	private Date updateDate;

	/**
	 * 修改人
	 */
	@Column(name = "UPDATE_USER")
	private String updateUser;

	@Transient
	public List<MenuManagerDto> menuList;
	private static final long serialVersionUID = 1L;

	public MenuManagerDto(Integer id, String uuid, String menuName,
			String parentId, Integer menuSort, String linkCode, Integer state,
			String belongsSys, Date createDate, String createUser,
			Date updateDate, String updateUser) {
		this.id = id;
		this.uuid = uuid;
		this.menuName = menuName;
		this.parentId = parentId;
		this.menuSort = menuSort;
		this.linkCode = linkCode;
		this.state = state;
		this.belongsSys = belongsSys;
		this.createDate = createDate;
		this.createUser = createUser;
		this.updateDate = updateDate;
		this.updateUser = updateUser;
	}

	public MenuManagerDto() {
		super();
	}

	/**
	 * @return ID
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return UUID
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * @param uuid
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid == null ? null : uuid.trim();
	}

	/**
	 * 获取菜单名称
	 *
	 * @return MENU_NAME - 菜单名称
	 */
	public String getMenuName() {
		return menuName;
	}

	/**
	 * 设置菜单名称
	 *
	 * @param menuName
	 *            菜单名称
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName == null ? null : menuName.trim();
	}

	/**
	 * 获取上级编号
	 *
	 * @return PARENT_ID - 上级编号
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * 设置上级编号
	 *
	 * @param parentId
	 *            上级编号
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId == null ? null : parentId.trim();
	}

	/**
	 * 获取排序
	 *
	 * @return MENU_SORT - 排序
	 */
	public Integer getMenuSort() {
		return menuSort;
	}

	/**
	 * 设置排序
	 *
	 * @param menuSort
	 *            排序
	 */
	public void setMenuSort(Integer menuSort) {
		this.menuSort = menuSort;
	}

	/**
	 * 获取菜单链接
	 *
	 * @return LINK_CODE - 菜单链接
	 */
	public String getLinkCode() {
		return linkCode;
	}

	/**
	 * 设置菜单链接
	 *
	 * @param linkCode
	 *            菜单链接
	 */
	public void setLinkCode(String linkCode) {
		this.linkCode = linkCode == null ? null : linkCode.trim();
	}

	/**
	 * 获取菜单状态（用户中心/运营中心）
	 *
	 * @return STATE - 菜单状态（用户中心/运营中心）
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * 设置菜单状态（用户中心/运营中心）
	 *
	 * @param state
	 *            菜单状态（用户中心/运营中心）
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * 获取所属系统
	 *
	 * @return BELONGS_SYS - 所属系统
	 */
	public String getBelongsSys() {
		return belongsSys;
	}

	/**
	 * 设置所属系统
	 *
	 * @param belongsSys
	 *            所属系统
	 */
	public void setBelongsSys(String belongsSys) {
		this.belongsSys = belongsSys == null ? null : belongsSys.trim();
	}

	/**
	 * 获取创建时间
	 *
	 * @return CREATE_DATE - 创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * 设置创建时间
	 *
	 * @param createDate
	 *            创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 获取创建人
	 *
	 * @return CREATE_USER - 创建人
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * 设置创建人
	 *
	 * @param createUser
	 *            创建人
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser == null ? null : createUser.trim();
	}

	/**
	 * 获取修改时间
	 *
	 * @return UPDATE_DATE - 修改时间
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * 设置修改时间
	 *
	 * @param updateDate
	 *            修改时间
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 获取修改人
	 *
	 * @return UPDATE_USER - 修改人
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置修改人
	 *
	 * @param updateUser
	 *            修改人
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser == null ? null : updateUser.trim();
	}

	public List<MenuManagerDto> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<MenuManagerDto> menuList) {
		this.menuList = menuList;
	}
}