package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_role_info")
public class RoleInfoDto implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 角色名称
     */
    @Column(name = "ROLE_NAME")
    private String roleName;

    /**
     * 员工数量
     */
    @Column(name = "EMPLOYEE_NUM")
    private Integer employeeNum;

    /**
     * 是否启用（0 是 1否）
     */
    @Column(name = "IS_WORK")
    private Integer isWork;

    /**
     * 是否删除（0否  1是）
     */
    @Column(name = "IS_DELETE")
    private Integer isDelete;

    /**
     * 状态（0默认   1否）
     */
    @Column(name = "STATE")
    private Integer state;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 职能描述
     */
    @Column(name = "FUNCTION_INFO")
    private String functionInfo;

    private static final long serialVersionUID = 1L;

    public RoleInfoDto(Integer id, String uuid, String roleName, Integer employeeNum, Integer isWork, Integer isDelete, Integer state, Date createDate, String createUser, Date updateDate, String updateUser, String functionInfo) {
        this.id = id;
        this.uuid = uuid;
        this.roleName = roleName;
        this.employeeNum = employeeNum;
        this.isWork = isWork;
        this.isDelete = isDelete;
        this.state = state;
        this.createDate = createDate;
        this.createUser = createUser;
        this.updateDate = updateDate;
        this.updateUser = updateUser;
        this.functionInfo = functionInfo;
    }

    public RoleInfoDto() {
        super();
    }

    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取角色名称
     *
     * @return ROLE_NAME - 角色名称
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置角色名称
     *
     * @param roleName 角色名称
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    /**
     * 获取员工数量
     *
     * @return EMPLOYEE_NUM - 员工数量
     */
    public Integer getEmployeeNum() {
        return employeeNum;
    }

    /**
     * 设置员工数量
     *
     * @param employeeNum 员工数量
     */
    public void setEmployeeNum(Integer employeeNum) {
        this.employeeNum = employeeNum;
    }

    /**
     * 获取是否启用（0 是 1否）
     *
     * @return IS_WORK - 是否启用（0 是 1否）
     */
    public Integer getIsWork() {
        return isWork;
    }

    /**
     * 设置是否启用（0 是 1否）
     *
     * @param isWork 是否启用（0 是 1否）
     */
    public void setIsWork(Integer isWork) {
        this.isWork = isWork;
    }

    /**
     * 获取是否删除（0否  1是）
     *
     * @return IS_DELETE - 是否删除（0否  1是）
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除（0否  1是）
     *
     * @param isDelete 是否删除（0否  1是）
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取状态（0默认   1否）
     *
     * @return STATE - 状态（0默认   1否）
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态（0默认   1否）
     *
     * @param state 状态（0默认   1否）
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_DATE - 修改时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取修改人
     *
     * @return UPDATE_USER - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取职能描述
     *
     * @return FUNCTION_INFO - 职能描述
     */
    public String getFunctionInfo() {
        return functionInfo;
    }

    /**
     * 设置职能描述
     *
     * @param functionInfo 职能描述
     */
    public void setFunctionInfo(String functionInfo) {
        this.functionInfo = functionInfo == null ? null : functionInfo.trim();
    }
}