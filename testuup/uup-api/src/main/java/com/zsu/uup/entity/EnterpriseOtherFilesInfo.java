package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_enterprise_other_files")
public class EnterpriseOtherFilesInfo implements Serializable {
    /**
     * 自增id
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 公司id
     */
    @Column(name = "CORP_ID")
    private String corpId;

    /**
     * 文件标题
     */
    @Column(name = "FILE_TITLE")
    private String fileTitle;

    /**
     * 上传文件名称
     */
    @Column(name = "FILE_NAME")
    private String fileName;

    /**
     * 文件地址
     */
    @Column(name = "FILE_URI")
    private String fileUri;

    /**
     * 状态0审核中1审核通过2审核未通过
     */
    @Column(name = "CHECK_STATUS")
    private Integer checkStatus;

    /**
     * 是否删除0未删除1已删除
     */
    @Column(name = "IS_DELETE")
    private Integer isDelete;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    /**
     * 文件说明
     */
    @Column(name = "FILE_EXPLAIN")
    private String fileExplain;

    private static final long serialVersionUID = 1L;

    public EnterpriseOtherFilesInfo(Integer id, String uuid, String corpId, String fileTitle, String fileName, String fileUri, Integer checkStatus, Integer isDelete, String createUser, Date createDate, String updateUser, Date updateDate, String fileExplain) {
        this.id = id;
        this.uuid = uuid;
        this.corpId = corpId;
        this.fileTitle = fileTitle;
        this.fileName = fileName;
        this.fileUri = fileUri;
        this.checkStatus = checkStatus;
        this.isDelete = isDelete;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
        this.fileExplain = fileExplain;
    }

    public EnterpriseOtherFilesInfo() {
        super();
    }

    /**
     * 获取自增id
     *
     * @return ID - 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增id
     *
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取公司id
     *
     * @return CORP_ID - 公司id
     */
    public String getCorpId() {
        return corpId;
    }

    /**
     * 设置公司id
     *
     * @param corpId 公司id
     */
    public void setCorpId(String corpId) {
        this.corpId = corpId == null ? null : corpId.trim();
    }

    /**
     * 获取文件标题
     *
     * @return FILE_TITLE - 文件标题
     */
    public String getFileTitle() {
        return fileTitle;
    }

    /**
     * 设置文件标题
     *
     * @param fileTitle 文件标题
     */
    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle == null ? null : fileTitle.trim();
    }

    /**
     * 获取上传文件名称
     *
     * @return FILE_NAME - 上传文件名称
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置上传文件名称
     *
     * @param fileName 上传文件名称
     */
    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    /**
     * 获取文件地址
     *
     * @return FILE_URI - 文件地址
     */
    public String getFileUri() {
        return fileUri;
    }

    /**
     * 设置文件地址
     *
     * @param fileUri 文件地址
     */
    public void setFileUri(String fileUri) {
        this.fileUri = fileUri == null ? null : fileUri.trim();
    }

    /**
     * 获取状态0审核中1审核通过2审核未通过
     *
     * @return CHECK_STATUS - 状态0审核中1审核通过2审核未通过
     */
    public Integer getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置状态0审核中1审核通过2审核未通过
     *
     * @param checkStatus 状态0审核中1审核通过2审核未通过
     */
    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取是否删除0未删除1已删除
     *
     * @return IS_DELETE - 是否删除0未删除1已删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除0未删除1已删除
     *
     * @param isDelete 是否删除0未删除1已删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取修改人
     *
     * @return UPDATE_USER - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_DATE - 修改时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取文件说明
     *
     * @return FILE_EXPLAIN - 文件说明
     */
    public String getFileExplain() {
        return fileExplain;
    }

    /**
     * 设置文件说明
     *
     * @param fileExplain 文件说明
     */
    public void setFileExplain(String fileExplain) {
        this.fileExplain = fileExplain == null ? null : fileExplain.trim();
    }
}