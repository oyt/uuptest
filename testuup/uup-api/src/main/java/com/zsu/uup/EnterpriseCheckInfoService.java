package com.zsu.uup;

import java.util.List;
import java.util.Map;

import com.zsu.uup.entity.EnterpriseCheckInfo;
import com.zsu.uup.pageinfo.PageInfo;

public interface EnterpriseCheckInfoService {
	/**
	 * 
	 * <p>Title: getEnterpriseCheckList(企业资料审核    未审核)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public PageInfo<EnterpriseCheckInfo> getEnterpriseCheckList(Map<String, String> map);
	/**
	 * 
	 * <p>Title: getEnterpriseCheckListEnd(企业资料审核    已审核)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public PageInfo<EnterpriseCheckInfo> getEnterpriseCheckListEnd(Map<String, String> map);
	
	/**
	 * 
	 * <p>Title: getEnterpriseCheckSomethingById(根据ID获取企业提交的审核资料详情)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param ids
	 * @return
	 */
	public List<EnterpriseCheckInfo> getEnterpriseCheckSomethingById(Integer id);
}
