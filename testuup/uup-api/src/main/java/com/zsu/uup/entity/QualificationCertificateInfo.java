package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_qualification_certificate")
public class QualificationCertificateInfo implements Serializable {
    /**
     * 自增id
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 公司id
     */
    @Column(name = "CORP_ID")
    private String corpId;

    /**
     * 证书名称
     */
    @Column(name = "CERT_NAME")
    private String certName;

    /**
     * 证书编号
     */
    @Column(name = "CERT_NUM")
    private String certNum;

    /**
     * 颁发机构
     */
    @Column(name = "CERT_ORGANIZATION")
    private String certOrganization;

    /**
     * 颁发日期
     */
    @Column(name = "CERT_START_DATE")
    private Date certStartDate;

    /**
     * 有效期结束日期
     */
    @Column(name = "CERT_END_DATE")
    private Date certEndDate;

    /**
     * 上传文件名
     */
    @Column(name = "CERT_FILE_NAME")
    private String certFileName;

    /**
     * 文件地址
     */
    @Column(name = "CERT_FILE_URI")
    private String certFileUri;

    /**
     * 状态0审核中1已审核2审核未通过
     */
    @Column(name = "CHECK_STATUS")
    private Integer checkStatus;

    /**
     * 是否删除
     */
    @Column(name = "IS_DELETE")
    private Integer isDelete;

    /**
     * 备注
     */
    @Column(name = "CERT_REMARK")
    private String certRemark;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    public QualificationCertificateInfo(Integer id, String uuid, String corpId, String certName, String certNum, String certOrganization, Date certStartDate, Date certEndDate, String certFileName, String certFileUri, Integer checkStatus, Integer isDelete, String certRemark, String createUser, Date createDate, String updateUser, Date updateDate) {
        this.id = id;
        this.uuid = uuid;
        this.corpId = corpId;
        this.certName = certName;
        this.certNum = certNum;
        this.certOrganization = certOrganization;
        this.certStartDate = certStartDate;
        this.certEndDate = certEndDate;
        this.certFileName = certFileName;
        this.certFileUri = certFileUri;
        this.checkStatus = checkStatus;
        this.isDelete = isDelete;
        this.certRemark = certRemark;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
    }

    public QualificationCertificateInfo() {
        super();
    }

    /**
     * 获取自增id
     *
     * @return ID - 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增id
     *
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取公司id
     *
     * @return CORP_ID - 公司id
     */
    public String getCorpId() {
        return corpId;
    }

    /**
     * 设置公司id
     *
     * @param corpId 公司id
     */
    public void setCorpId(String corpId) {
        this.corpId = corpId == null ? null : corpId.trim();
    }

    /**
     * 获取证书名称
     *
     * @return CERT_NAME - 证书名称
     */
    public String getCertName() {
        return certName;
    }

    /**
     * 设置证书名称
     *
     * @param certName 证书名称
     */
    public void setCertName(String certName) {
        this.certName = certName == null ? null : certName.trim();
    }

    /**
     * 获取证书编号
     *
     * @return CERT_NUM - 证书编号
     */
    public String getCertNum() {
        return certNum;
    }

    /**
     * 设置证书编号
     *
     * @param certNum 证书编号
     */
    public void setCertNum(String certNum) {
        this.certNum = certNum == null ? null : certNum.trim();
    }

    /**
     * 获取颁发机构
     *
     * @return CERT_ORGANIZATION - 颁发机构
     */
    public String getCertOrganization() {
        return certOrganization;
    }

    /**
     * 设置颁发机构
     *
     * @param certOrganization 颁发机构
     */
    public void setCertOrganization(String certOrganization) {
        this.certOrganization = certOrganization == null ? null : certOrganization.trim();
    }

    /**
     * 获取颁发日期
     *
     * @return CERT_START_DATE - 颁发日期
     */
    public Date getCertStartDate() {
        return certStartDate;
    }

    /**
     * 设置颁发日期
     *
     * @param certStartDate 颁发日期
     */
    public void setCertStartDate(Date certStartDate) {
        this.certStartDate = certStartDate;
    }

    /**
     * 获取有效期结束日期
     *
     * @return CERT_END_DATE - 有效期结束日期
     */
    public Date getCertEndDate() {
        return certEndDate;
    }

    /**
     * 设置有效期结束日期
     *
     * @param certEndDate 有效期结束日期
     */
    public void setCertEndDate(Date certEndDate) {
        this.certEndDate = certEndDate;
    }

    /**
     * 获取上传文件名
     *
     * @return CERT_FILE_NAME - 上传文件名
     */
    public String getCertFileName() {
        return certFileName;
    }

    /**
     * 设置上传文件名
     *
     * @param certFileName 上传文件名
     */
    public void setCertFileName(String certFileName) {
        this.certFileName = certFileName == null ? null : certFileName.trim();
    }

    /**
     * 获取文件地址
     *
     * @return CERT_FILE_URI - 文件地址
     */
    public String getCertFileUri() {
        return certFileUri;
    }

    /**
     * 设置文件地址
     *
     * @param certFileUri 文件地址
     */
    public void setCertFileUri(String certFileUri) {
        this.certFileUri = certFileUri == null ? null : certFileUri.trim();
    }

    /**
     * 获取状态0审核中1已审核2审核未通过
     *
     * @return CHECK_STATUS - 状态0审核中1已审核2审核未通过
     */
    public Integer getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置状态0审核中1已审核2审核未通过
     *
     * @param checkStatus 状态0审核中1已审核2审核未通过
     */
    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取是否删除
     *
     * @return IS_DELETE - 是否删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除
     *
     * @param isDelete 是否删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取备注
     *
     * @return CERT_REMARK - 备注
     */
    public String getCertRemark() {
        return certRemark;
    }

    /**
     * 设置备注
     *
     * @param certRemark 备注
     */
    public void setCertRemark(String certRemark) {
        this.certRemark = certRemark == null ? null : certRemark.trim();
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取修改人
     *
     * @return UPDATE_USER - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_DATE - 修改时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}