package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_business_info")
public class BusinessInfo implements Serializable {
    /**
     * 自增id
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 公司id
     */
    @Column(name = "CORP_ID")
    private String corpId;

    /**
     * 名称
     */
    @Column(name = "BUSINESS_NAME")
    private String businessName;

    /**
     * 类型
     */
    @Column(name = "BUSINESS_TYPE")
    private String businessType;

    /**
     * 省
     */
    @Column(name = "PRIVINCE")
    private String privince;

    /**
     * 市
     */
    @Column(name = "CITY")
    private String city;

    /**
     * 地区
     */
    @Column(name = "DISTRICE")
    private String districe;

    /**
     * 详细地址
     */
    @Column(name = "ADDRESS")
    private String address;

    /**
     * 营业执照
     */
    @Column(name = "LICENSE")
    private String license;

    /**
     * 营业执照号码
     */
    @Column(name = "LICENSE_NUMBER")
    private String licenseNumber;

    /**
     * 统一社会信用代码
     */
    @Column(name = "CREDIT_CODE")
    private String creditCode;

    /**
     * 营业执照生效日期
     */
    @Column(name = "LICENSE_START_DATE")
    private Date licenseStartDate;

    /**
     * 营业执照失效日期
     */
    @Column(name = "LICENSE_END_DATE")
    private Date licenseEndDate;

    /**
     * 法人代表
     */
    @Column(name = "LEGAL_REPRESENTATIVE")
    private String legalRepresentative;

    /**
     * 注册资本
     */
    @Column(name = "REGISTERED_CAPITAL")
    private Integer registeredCapital;

    /**
     * 是否多证合一0否1是
     */
    @Column(name = "MULTIPLE")
    private Integer multiple;

    /**
     * 组织机构代码证文件地址
     */
    @Column(name = "ORGANIZATION_CODE_CERTIFICATE_URI")
    private String organizationCodeCertificateUri;

    /**
     * 组织机构代码
     */
    @Column(name = "ORGANIZATION_CODE")
    private String organizationCode;

    /**
     * 组织机构代码证生效日期
     */
    @Column(name = "ORGANIZATION_CODE_START_DATE")
    private Date organizationCodeStartDate;

    /**
     * 组织机构代码证失效日期
     */
    @Column(name = "ORGANIZATION_CODE_END_DATE")
    private Date organizationCodeEndDate;

    /**
     * 税务登记证文件地址
     */
    @Column(name = "TAX_REGISTERATION_CERTIFICATE_URI")
    private String taxRegisterationCertificateUri;

    /**
     * 税务登记证号码（税号）
     */
    @Column(name = "DUTY_PARAGRAPH")
    private String dutyParagraph;

    /**
     * 税务登记证号码生效日期
     */
    @Column(name = "TAX_REGISTERATION_START_DATE")
    private Date taxRegisterationStartDate;

    /**
     * 税务登记证号码失效日期
     */
    @Column(name = "TAX_REGISTERATION_END_DATE")
    private Date taxRegisterationEndDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Column(name = "UPDATE_DATA")
    private Date updateData;

    /**
     * 经营范围
     */
    @Column(name = "BUSINESS_SCOPE")
    private String businessScope;

    private static final long serialVersionUID = 1L;

    public BusinessInfo(Integer id, String uuid, String corpId, String businessName, String businessType, String privince, String city, String districe, String address, String license, String licenseNumber, String creditCode, Date licenseStartDate, Date licenseEndDate, String legalRepresentative, Integer registeredCapital, Integer multiple, String organizationCodeCertificateUri, String organizationCode, Date organizationCodeStartDate, Date organizationCodeEndDate, String taxRegisterationCertificateUri, String dutyParagraph, Date taxRegisterationStartDate, Date taxRegisterationEndDate, String createUser, Date createDate, String updateUser, Date updateData, String businessScope) {
        this.id = id;
        this.uuid = uuid;
        this.corpId = corpId;
        this.businessName = businessName;
        this.businessType = businessType;
        this.privince = privince;
        this.city = city;
        this.districe = districe;
        this.address = address;
        this.license = license;
        this.licenseNumber = licenseNumber;
        this.creditCode = creditCode;
        this.licenseStartDate = licenseStartDate;
        this.licenseEndDate = licenseEndDate;
        this.legalRepresentative = legalRepresentative;
        this.registeredCapital = registeredCapital;
        this.multiple = multiple;
        this.organizationCodeCertificateUri = organizationCodeCertificateUri;
        this.organizationCode = organizationCode;
        this.organizationCodeStartDate = organizationCodeStartDate;
        this.organizationCodeEndDate = organizationCodeEndDate;
        this.taxRegisterationCertificateUri = taxRegisterationCertificateUri;
        this.dutyParagraph = dutyParagraph;
        this.taxRegisterationStartDate = taxRegisterationStartDate;
        this.taxRegisterationEndDate = taxRegisterationEndDate;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateData = updateData;
        this.businessScope = businessScope;
    }

    public BusinessInfo() {
        super();
    }

    /**
     * 获取自增id
     *
     * @return ID - 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增id
     *
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取公司id
     *
     * @return CORP_ID - 公司id
     */
    public String getCorpId() {
        return corpId;
    }

    /**
     * 设置公司id
     *
     * @param corpId 公司id
     */
    public void setCorpId(String corpId) {
        this.corpId = corpId == null ? null : corpId.trim();
    }

    /**
     * 获取名称
     *
     * @return BUSINESS_NAME - 名称
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * 设置名称
     *
     * @param businessName 名称
     */
    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    /**
     * 获取类型
     *
     * @return BUSINESS_TYPE - 类型
     */
    public String getBusinessType() {
        return businessType;
    }

    /**
     * 设置类型
     *
     * @param businessType 类型
     */
    public void setBusinessType(String businessType) {
        this.businessType = businessType == null ? null : businessType.trim();
    }

    /**
     * 获取省
     *
     * @return PRIVINCE - 省
     */
    public String getPrivince() {
        return privince;
    }

    /**
     * 设置省
     *
     * @param privince 省
     */
    public void setPrivince(String privince) {
        this.privince = privince == null ? null : privince.trim();
    }

    /**
     * 获取市
     *
     * @return CITY - 市
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置市
     *
     * @param city 市
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * 获取地区
     *
     * @return DISTRICE - 地区
     */
    public String getDistrice() {
        return districe;
    }

    /**
     * 设置地区
     *
     * @param districe 地区
     */
    public void setDistrice(String districe) {
        this.districe = districe == null ? null : districe.trim();
    }

    /**
     * 获取详细地址
     *
     * @return ADDRESS - 详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置详细地址
     *
     * @param address 详细地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取营业执照
     *
     * @return LICENSE - 营业执照
     */
    public String getLicense() {
        return license;
    }

    /**
     * 设置营业执照
     *
     * @param license 营业执照
     */
    public void setLicense(String license) {
        this.license = license == null ? null : license.trim();
    }

    /**
     * 获取营业执照号码
     *
     * @return LICENSE_NUMBER - 营业执照号码
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * 设置营业执照号码
     *
     * @param licenseNumber 营业执照号码
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber == null ? null : licenseNumber.trim();
    }

    /**
     * 获取统一社会信用代码
     *
     * @return CREDIT_CODE - 统一社会信用代码
     */
    public String getCreditCode() {
        return creditCode;
    }

    /**
     * 设置统一社会信用代码
     *
     * @param creditCode 统一社会信用代码
     */
    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode == null ? null : creditCode.trim();
    }

    /**
     * 获取营业执照生效日期
     *
     * @return LICENSE_START_DATE - 营业执照生效日期
     */
    public Date getLicenseStartDate() {
        return licenseStartDate;
    }

    /**
     * 设置营业执照生效日期
     *
     * @param licenseStartDate 营业执照生效日期
     */
    public void setLicenseStartDate(Date licenseStartDate) {
        this.licenseStartDate = licenseStartDate;
    }

    /**
     * 获取营业执照失效日期
     *
     * @return LICENSE_END_DATE - 营业执照失效日期
     */
    public Date getLicenseEndDate() {
        return licenseEndDate;
    }

    /**
     * 设置营业执照失效日期
     *
     * @param licenseEndDate 营业执照失效日期
     */
    public void setLicenseEndDate(Date licenseEndDate) {
        this.licenseEndDate = licenseEndDate;
    }

    /**
     * 获取法人代表
     *
     * @return LEGAL_REPRESENTATIVE - 法人代表
     */
    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    /**
     * 设置法人代表
     *
     * @param legalRepresentative 法人代表
     */
    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative == null ? null : legalRepresentative.trim();
    }

    /**
     * 获取注册资本
     *
     * @return REGISTERED_CAPITAL - 注册资本
     */
    public Integer getRegisteredCapital() {
        return registeredCapital;
    }

    /**
     * 设置注册资本
     *
     * @param registeredCapital 注册资本
     */
    public void setRegisteredCapital(Integer registeredCapital) {
        this.registeredCapital = registeredCapital;
    }

    /**
     * 获取是否多证合一0否1是
     *
     * @return MULTIPLE - 是否多证合一0否1是
     */
    public Integer getMultiple() {
        return multiple;
    }

    /**
     * 设置是否多证合一0否1是
     *
     * @param multiple 是否多证合一0否1是
     */
    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    /**
     * 获取组织机构代码证文件地址
     *
     * @return ORGANIZATION_CODE_CERTIFICATE_URI - 组织机构代码证文件地址
     */
    public String getOrganizationCodeCertificateUri() {
        return organizationCodeCertificateUri;
    }

    /**
     * 设置组织机构代码证文件地址
     *
     * @param organizationCodeCertificateUri 组织机构代码证文件地址
     */
    public void setOrganizationCodeCertificateUri(String organizationCodeCertificateUri) {
        this.organizationCodeCertificateUri = organizationCodeCertificateUri == null ? null : organizationCodeCertificateUri.trim();
    }

    /**
     * 获取组织机构代码
     *
     * @return ORGANIZATION_CODE - 组织机构代码
     */
    public String getOrganizationCode() {
        return organizationCode;
    }

    /**
     * 设置组织机构代码
     *
     * @param organizationCode 组织机构代码
     */
    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    /**
     * 获取组织机构代码证生效日期
     *
     * @return ORGANIZATION_CODE_START_DATE - 组织机构代码证生效日期
     */
    public Date getOrganizationCodeStartDate() {
        return organizationCodeStartDate;
    }

    /**
     * 设置组织机构代码证生效日期
     *
     * @param organizationCodeStartDate 组织机构代码证生效日期
     */
    public void setOrganizationCodeStartDate(Date organizationCodeStartDate) {
        this.organizationCodeStartDate = organizationCodeStartDate;
    }

    /**
     * 获取组织机构代码证失效日期
     *
     * @return ORGANIZATION_CODE_END_DATE - 组织机构代码证失效日期
     */
    public Date getOrganizationCodeEndDate() {
        return organizationCodeEndDate;
    }

    /**
     * 设置组织机构代码证失效日期
     *
     * @param organizationCodeEndDate 组织机构代码证失效日期
     */
    public void setOrganizationCodeEndDate(Date organizationCodeEndDate) {
        this.organizationCodeEndDate = organizationCodeEndDate;
    }

    /**
     * 获取税务登记证文件地址
     *
     * @return TAX_REGISTERATION_CERTIFICATE_URI - 税务登记证文件地址
     */
    public String getTaxRegisterationCertificateUri() {
        return taxRegisterationCertificateUri;
    }

    /**
     * 设置税务登记证文件地址
     *
     * @param taxRegisterationCertificateUri 税务登记证文件地址
     */
    public void setTaxRegisterationCertificateUri(String taxRegisterationCertificateUri) {
        this.taxRegisterationCertificateUri = taxRegisterationCertificateUri == null ? null : taxRegisterationCertificateUri.trim();
    }

    /**
     * 获取税务登记证号码（税号）
     *
     * @return DUTY_PARAGRAPH - 税务登记证号码（税号）
     */
    public String getDutyParagraph() {
        return dutyParagraph;
    }

    /**
     * 设置税务登记证号码（税号）
     *
     * @param dutyParagraph 税务登记证号码（税号）
     */
    public void setDutyParagraph(String dutyParagraph) {
        this.dutyParagraph = dutyParagraph == null ? null : dutyParagraph.trim();
    }

    /**
     * 获取税务登记证号码生效日期
     *
     * @return TAX_REGISTERATION_START_DATE - 税务登记证号码生效日期
     */
    public Date getTaxRegisterationStartDate() {
        return taxRegisterationStartDate;
    }

    /**
     * 设置税务登记证号码生效日期
     *
     * @param taxRegisterationStartDate 税务登记证号码生效日期
     */
    public void setTaxRegisterationStartDate(Date taxRegisterationStartDate) {
        this.taxRegisterationStartDate = taxRegisterationStartDate;
    }

    /**
     * 获取税务登记证号码失效日期
     *
     * @return TAX_REGISTERATION_END_DATE - 税务登记证号码失效日期
     */
    public Date getTaxRegisterationEndDate() {
        return taxRegisterationEndDate;
    }

    /**
     * 设置税务登记证号码失效日期
     *
     * @param taxRegisterationEndDate 税务登记证号码失效日期
     */
    public void setTaxRegisterationEndDate(Date taxRegisterationEndDate) {
        this.taxRegisterationEndDate = taxRegisterationEndDate;
    }

    /**
     * @return CREATE_USER
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * @return CREATE_DATE
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return UPDATE_USER
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * @return UPDATE_DATA
     */
    public Date getUpdateData() {
        return updateData;
    }

    /**
     * @param updateData
     */
    public void setUpdateData(Date updateData) {
        this.updateData = updateData;
    }

    /**
     * 获取经营范围
     *
     * @return BUSINESS_SCOPE - 经营范围
     */
    public String getBusinessScope() {
        return businessScope;
    }

    /**
     * 设置经营范围
     *
     * @param businessScope 经营范围
     */
    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope == null ? null : businessScope.trim();
    }
}