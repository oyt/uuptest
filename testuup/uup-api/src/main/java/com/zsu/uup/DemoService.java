package com.zsu.uup;

import java.util.List;
import java.util.Map;

import com.zsu.uup.entity.DemoInfo;

public interface DemoService {
	
	public List<DemoInfo> getDemoList();
	
	public int insertDemo(DemoInfo demoInfo);
	
	public int updateDemo(DemoInfo demoInfo);
	
	public int deleteDemo(int t1id);
	
	public List<DemoInfo> getDemoById(int t1id);
	
	public List<DemoInfo> getDemoByName(String t1name);
	
	public int deleteDemoList(List<DemoInfo> dlist);
	
	public int updateDemoList(List<DemoInfo> dlist);
	
	public int insertDemoList(List<DemoInfo> dlist);
	
	public Map<String, String> selectDemoMap(Map<String, String> dmap);
}
