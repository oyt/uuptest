package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_enterprise_info_check")
public class EnterpriseCheckInfo implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 公司信息审核状态，0审核中1已审核2已撤回
     */
    @Column(name = "CHECK_STATUS")
    private Integer checkStatus;

    /**
     * 公司id，未创建的公司id初始化为CHECK_ID
     */
    @Column(name = "CORP_ID")
    private String corpId;

    /**
     * 公司logo
     */
    @Column(name = "CORP_LOGO")
    private String corpLogo;

    /**
     * 公司全称
     */
    @Column(name = "CORP_NAME")
    private String corpName;

    /**
     * 公司简称
     */
    @Column(name = "CORP_SHORT_NAME")
    private String corpShortName;

    /**
     * 公司英文名
     */
    @Column(name = "CORP_ENGLISH_NAME")
    private String corpEnglishName;

    /**
     * 企业身份，位运算存储标识
     */
    @Column(name = "CORP_IDENTITIES")
    private Integer corpIdentities;

    /**
     * 经营模式，位运算存储标识
     */
    @Column(name = "CORP_BUSINESS_MODEL")
    private Integer corpBusinessModel;

    /**
     * 主营品种
     */
    @Column(name = "CORP_BUSINESS_CATEGORY")
    private String corpBusinessCategory;

    /**
     * 省
     */
    @Column(name = "CORP_PROVINCE")
    private String corpProvince;

    /**
     * 市
     */
    @Column(name = "CORP_CITY")
    private String corpCity;

    /**
     * 地区
     */
    @Column(name = "CORP_DISTRICE")
    private String corpDistrice;

    /**
     * 详细地址
     */
    @Column(name = "CORP_ADDRESS")
    private String corpAddress;

    /**
     * 经度
     */
    @Column(name = "CORP_LONGITUDE")
    private Double corpLongitude;

    /**
     * 纬度
     */
    @Column(name = "CORP_LATITUDE")
    private Double corpLatitude;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    public EnterpriseCheckInfo(Integer id, String uuid, Integer checkStatus, String corpId, String corpLogo, String corpName, String corpShortName, String corpEnglishName, Integer corpIdentities, Integer corpBusinessModel, String corpBusinessCategory, String corpProvince, String corpCity, String corpDistrice, String corpAddress, Double corpLongitude, Double corpLatitude, String createUser, Date createDate, String updateUser, Date updateDate) {
        this.id = id;
        this.uuid = uuid;
        this.checkStatus = checkStatus;
        this.corpId = corpId;
        this.corpLogo = corpLogo;
        this.corpName = corpName;
        this.corpShortName = corpShortName;
        this.corpEnglishName = corpEnglishName;
        this.corpIdentities = corpIdentities;
        this.corpBusinessModel = corpBusinessModel;
        this.corpBusinessCategory = corpBusinessCategory;
        this.corpProvince = corpProvince;
        this.corpCity = corpCity;
        this.corpDistrice = corpDistrice;
        this.corpAddress = corpAddress;
        this.corpLongitude = corpLongitude;
        this.corpLatitude = corpLatitude;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
    }

    public EnterpriseCheckInfo() {
        super();
    }

    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取公司信息审核状态，0审核中1已审核2已撤回
     *
     * @return CHECK_STATUS - 公司信息审核状态，0审核中1已审核2已撤回
     */
    public Integer getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置公司信息审核状态，0审核中1已审核2已撤回
     *
     * @param checkStatus 公司信息审核状态，0审核中1已审核2已撤回
     */
    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取公司id，未创建的公司id初始化为CHECK_ID
     *
     * @return CORP_ID - 公司id，未创建的公司id初始化为CHECK_ID
     */
    public String getCorpId() {
        return corpId;
    }

    /**
     * 设置公司id，未创建的公司id初始化为CHECK_ID
     *
     * @param corpId 公司id，未创建的公司id初始化为CHECK_ID
     */
    public void setCorpId(String corpId) {
        this.corpId = corpId == null ? null : corpId.trim();
    }

    /**
     * 获取公司logo
     *
     * @return CORP_LOGO - 公司logo
     */
    public String getCorpLogo() {
        return corpLogo;
    }

    /**
     * 设置公司logo
     *
     * @param corpLogo 公司logo
     */
    public void setCorpLogo(String corpLogo) {
        this.corpLogo = corpLogo == null ? null : corpLogo.trim();
    }

    /**
     * 获取公司全称
     *
     * @return CORP_NAME - 公司全称
     */
    public String getCorpName() {
        return corpName;
    }

    /**
     * 设置公司全称
     *
     * @param corpName 公司全称
     */
    public void setCorpName(String corpName) {
        this.corpName = corpName == null ? null : corpName.trim();
    }

    /**
     * 获取公司简称
     *
     * @return CORP_SHORT_NAME - 公司简称
     */
    public String getCorpShortName() {
        return corpShortName;
    }

    /**
     * 设置公司简称
     *
     * @param corpShortName 公司简称
     */
    public void setCorpShortName(String corpShortName) {
        this.corpShortName = corpShortName == null ? null : corpShortName.trim();
    }

    /**
     * 获取公司英文名
     *
     * @return CORP_ENGLISH_NAME - 公司英文名
     */
    public String getCorpEnglishName() {
        return corpEnglishName;
    }

    /**
     * 设置公司英文名
     *
     * @param corpEnglishName 公司英文名
     */
    public void setCorpEnglishName(String corpEnglishName) {
        this.corpEnglishName = corpEnglishName == null ? null : corpEnglishName.trim();
    }

    /**
     * 获取企业身份，位运算存储标识
     *
     * @return CORP_IDENTITIES - 企业身份，位运算存储标识
     */
    public Integer getCorpIdentities() {
        return corpIdentities;
    }

    /**
     * 设置企业身份，位运算存储标识
     *
     * @param corpIdentities 企业身份，位运算存储标识
     */
    public void setCorpIdentities(Integer corpIdentities) {
        this.corpIdentities = corpIdentities;
    }

    /**
     * 获取经营模式，位运算存储标识
     *
     * @return CORP_BUSINESS_MODEL - 经营模式，位运算存储标识
     */
    public Integer getCorpBusinessModel() {
        return corpBusinessModel;
    }

    /**
     * 设置经营模式，位运算存储标识
     *
     * @param corpBusinessModel 经营模式，位运算存储标识
     */
    public void setCorpBusinessModel(Integer corpBusinessModel) {
        this.corpBusinessModel = corpBusinessModel;
    }

    /**
     * 获取主营品种
     *
     * @return CORP_BUSINESS_CATEGORY - 主营品种
     */
    public String getCorpBusinessCategory() {
        return corpBusinessCategory;
    }

    /**
     * 设置主营品种
     *
     * @param corpBusinessCategory 主营品种
     */
    public void setCorpBusinessCategory(String corpBusinessCategory) {
        this.corpBusinessCategory = corpBusinessCategory == null ? null : corpBusinessCategory.trim();
    }

    /**
     * 获取省
     *
     * @return CORP_PROVINCE - 省
     */
    public String getCorpProvince() {
        return corpProvince;
    }

    /**
     * 设置省
     *
     * @param corpProvince 省
     */
    public void setCorpProvince(String corpProvince) {
        this.corpProvince = corpProvince == null ? null : corpProvince.trim();
    }

    /**
     * 获取市
     *
     * @return CORP_CITY - 市
     */
    public String getCorpCity() {
        return corpCity;
    }

    /**
     * 设置市
     *
     * @param corpCity 市
     */
    public void setCorpCity(String corpCity) {
        this.corpCity = corpCity == null ? null : corpCity.trim();
    }

    /**
     * 获取地区
     *
     * @return CORP_DISTRICE - 地区
     */
    public String getCorpDistrice() {
        return corpDistrice;
    }

    /**
     * 设置地区
     *
     * @param corpDistrice 地区
     */
    public void setCorpDistrice(String corpDistrice) {
        this.corpDistrice = corpDistrice == null ? null : corpDistrice.trim();
    }

    /**
     * 获取详细地址
     *
     * @return CORP_ADDRESS - 详细地址
     */
    public String getCorpAddress() {
        return corpAddress;
    }

    /**
     * 设置详细地址
     *
     * @param corpAddress 详细地址
     */
    public void setCorpAddress(String corpAddress) {
        this.corpAddress = corpAddress == null ? null : corpAddress.trim();
    }

    /**
     * 获取经度
     *
     * @return CORP_LONGITUDE - 经度
     */
    public Double getCorpLongitude() {
        return corpLongitude;
    }

    /**
     * 设置经度
     *
     * @param corpLongitude 经度
     */
    public void setCorpLongitude(Double corpLongitude) {
        this.corpLongitude = corpLongitude;
    }

    /**
     * 获取纬度
     *
     * @return CORP_LATITUDE - 纬度
     */
    public Double getCorpLatitude() {
        return corpLatitude;
    }

    /**
     * 设置纬度
     *
     * @param corpLatitude 纬度
     */
    public void setCorpLatitude(Double corpLatitude) {
        this.corpLatitude = corpLatitude;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取修改人
     *
     * @return UPDATE_USER - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_DATE - 修改时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}