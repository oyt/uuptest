package com.zsu.uup;

import java.util.Map;

import com.zsu.uup.entity.UserCheckInfo;
import com.zsu.uup.pageinfo.PageInfo;
/**
 * 
 * <p>Title: UserCheckInfoService</p>  
 * <p>Description: </p> 
 * @author Kouxu (Create on:2018年6月19日)
 * @version 1.0
 * @fileName UserCheckInfoService.java
 */
public interface UserCheckInfoService {
	/**
	 * 
	 * <p>Title: getUserCheckList(个人资料审核历史记录)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public PageInfo<UserCheckInfo> getUserCheckList(Map<String, String> map);
}
