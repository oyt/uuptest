package com.zsu.uup;

import java.util.List;
import java.util.Map;

import com.zsu.uup.entity.TestInfoDto;

/**
 * <p>
 * 
 * </p>
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * @author Tao.Ouyang (Create on:2018年6月5日)
 * @version 1.0
 * @fileName UserService.java
 */
public interface UserService {

    public List<String> getListUserinfo();

    /**
     * @author Tao.Ouyang (Create on:2018年6月7日)
     * @version 1.0
     * @param name 用户名
     * @return
     */
    public String getUsername(String name);

    /**
     * <p>
     * 
     * </p>
     * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
     * @author Tao.Ouyang (Create on:2018年6月19日)
     * @version 1.0
     * @param map
     * @return
     */
    public com.zsu.uup.pageinfo.PageInfo<TestInfoDto> getListUser(Map<String, String> map);

}
