/**
 * oyt
 */
package com.zsu.uup.pageinfo;

/**
 * <p>
 * 
 * </p>
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * @author Tao.Ouyang (Create on:2018年6月19日)
 * @version 1.0
 * @fileName PageConfig.java
 */
public class PageConfig {

    // 当前页
    public static final Integer PAGE_NO        = 1;

    //每页条数
    public static final Integer PAGE_ROW_COUNT = 10;

}
