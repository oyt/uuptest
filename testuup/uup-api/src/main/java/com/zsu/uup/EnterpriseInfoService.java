package com.zsu.uup;

import java.util.Map;


import com.zsu.uup.entity.EnterpriseInfo;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 
 * <p>Title: EnterpriseInfoService</p>  
 * <p>Description: </p> 
 * @author Kouxu (Create on:2018年6月20日)
 * @version 1.0
 * @fileName EnterpriseInfoService.java
 */
public interface EnterpriseInfoService {
	/**
	 * 
	 * <p>Title: getEnterpriseList(获取平台企业列表)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public PageInfo<EnterpriseInfo> getEnterpriseList(Map<String,String> map);
}
