package com.zsu.uup;

import java.util.List;
import com.zsu.uup.entity.IndustryClass;


public interface IndustryClassService {
	/**
	 * 
	 * <p>Title: getAllIndustryClass</p>  
	 * 		获得所有的行业分类
	 * @author WangShiPing (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	List<IndustryClass> getAllIndustryClass();
}
