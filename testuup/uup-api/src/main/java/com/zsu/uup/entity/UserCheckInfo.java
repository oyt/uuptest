package com.zsu.uup.entity;

/*import com.zsu.common.base.BaseEntity;*/
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_user_info_check")
public class UserCheckInfo implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 身份证文件地址
     */
    @Column(name = "USER_ID_CARD_URI")
    private String userIdCardUri;

    /**
     * 身份证号
     */
    @Column(name = "USER_ID_NUMBER")
    private String userIdNumber;

    /**
     * 真实姓名
     */
    @Column(name = "USER_REAL_NAME")
    private String userRealName;

    /**
     * 审核状态0审核中1通过2不通过
     */
    @Column(name = "CHECK_STATUS")
    private Integer checkStatus;

    /**
     * 用户id
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    public UserCheckInfo(Integer id, String uuid, String userIdCardUri, String userIdNumber, String userRealName, Integer checkStatus, String createUser, Date createDate, String updateUser, Date updateDate) {
        this.id = id;
        this.uuid = uuid;
        this.userIdCardUri = userIdCardUri;
        this.userIdNumber = userIdNumber;
        this.userRealName = userRealName;
        this.checkStatus = checkStatus;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
    }

    public UserCheckInfo() {
        super();
    }

    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取身份证文件地址
     *
     * @return USER_ID_CARD_URI - 身份证文件地址
     */
    public String getUserIdCardUri() {
        return userIdCardUri;
    }

    /**
     * 设置身份证文件地址
     *
     * @param userIdCardUri 身份证文件地址
     */
    public void setUserIdCardUri(String userIdCardUri) {
        this.userIdCardUri = userIdCardUri == null ? null : userIdCardUri.trim();
    }

    /**
     * 获取身份证号
     *
     * @return USER_ID_NUMBER - 身份证号
     */
    public String getUserIdNumber() {
        return userIdNumber;
    }

    /**
     * 设置身份证号
     *
     * @param userIdNumber 身份证号
     */
    public void setUserIdNumber(String userIdNumber) {
        this.userIdNumber = userIdNumber == null ? null : userIdNumber.trim();
    }

    /**
     * 获取真实姓名
     *
     * @return USER_REAL_NAME - 真实姓名
     */
    public String getUserRealName() {
        return userRealName;
    }

    /**
     * 设置真实姓名
     *
     * @param userRealName 真实姓名
     */
    public void setUserRealName(String userRealName) {
        this.userRealName = userRealName == null ? null : userRealName.trim();
    }

    /**
     * 获取审核状态0审核中1通过2不通过
     *
     * @return CHECK_STATUS - 审核状态0审核中1通过2不通过
     */
    public Integer getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置审核状态0审核中1通过2不通过
     *
     * @param checkStatus 审核状态0审核中1通过2不通过
     */
    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取用户id
     *
     * @return CREATE_USER - 用户id
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置用户id
     *
     * @param createUser 用户id
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * @return CREATE_DATE
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return UPDATE_USER
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * @return UPDATE_DATE
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}