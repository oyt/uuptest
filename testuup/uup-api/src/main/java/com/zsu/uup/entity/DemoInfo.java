package com.zsu.uup.entity;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "test1")
public class DemoInfo implements Serializable{
	@Id
	private Integer t1id;
	private String t1name;
	
	private static final long serialVersionUID = 1L;
	public Integer getT1id() {
		return t1id;
	}

	public void setT1id(Integer t1id) {
		this.t1id = t1id;
	}

	public String getT1name() {
		return t1name;
	}

	public void setT1name(String t1name) {
		this.t1name = t1name;
	}

	public DemoInfo(Integer t1id, String t1name) {
		super();
		this.t1id = t1id;
		this.t1name = t1name;
	}

	public DemoInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
}
