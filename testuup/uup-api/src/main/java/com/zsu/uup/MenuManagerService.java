package com.zsu.uup;

import java.util.List;

import com.zsu.uup.entity.MenuManagerDto;

/**
 * 用户中心菜单管理
 * <p>
 * Title: MenuManagerService
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author wangyongge (Create on:2018年6月19日)
 * @version 1.0
 * @fileName MenuManagerService.java
 */
public interface MenuManagerService {
	/**
	 * 查询所有菜单
	 */
	public List<MenuManagerDto> queryAllMenuManager();
    /**
     * 查询顶级菜单
     * <p>Title: selectTopmenu</p>  
     * 
     * @author wangyongge (Create on:2018年6月28日)
     * @version 1.0
     * @return
     */
	public List<MenuManagerDto> selectTopmenu();
	/**
	 * 顶级菜单查询二级菜单
	 * <p>Title: selectChildmenu</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月28日)
	 * @version 1.0
	 * @param uuid 顶级菜单id
	 * @return
	 */
	public List<MenuManagerDto> selectChildmenu(String uuid);
	
	/**
	 * 二级菜单查询三级菜单
	 * <p>Title: selectGrandSonmenu</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月28日)
	 * @version 1.0
	 * @param uuid 二级菜单id
	 * @return
	 */
	public List<MenuManagerDto> selectGrandSonmenu(String uuid);
	/**
	 * 查询父级菜单
	 * <p>Title: selectMenuManagerDtoByUUId</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月29日)
	 * @version 1.0
	 * @param uuid
	 * @return
	 */
	public List<MenuManagerDto> selectMenuManagerDtoByUUId(String uuid);

	// 根据UUId查询关联子级菜单
	public List<MenuManagerDto> selectMenuManagerDtoByUUIdOrParentId(String uuid);

	// 添加用户一级菜单
	public int insertMenuManager(MenuManagerDto menuManagerDto);
	
	/**
	 * 添加子级菜单
	 * <p>Title: insertChildMenu</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月29日)
	 * @version 1.0
	 * @param menuManagerDto
	 * @return
	 */
	public int insertChildMenu(MenuManagerDto menuManagerDto);
	/**
	 * 根据uuid删除用户父级菜单
	 * <p>Title: deleteParentMenu</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月29日)
	 * @version 1.0
	 * @param uuid
	 * @return
	 */
	public int deleteParentMenu(String uuid);

	// 根据parentid删除用户子级菜单
	public int deleteChildMenu(String parentid);
	//更新（删除）菜单
	public int deleteMenuManager(MenuManagerDto menuManagerDto);
}
