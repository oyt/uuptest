package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Table(name = "zs_pd_industry_class")
public class IndustryClass implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 父ID
     */
    @Column(name = "IC_FID")
    private String icFid;

    /**
     * 分类名称
     */
    @Column(name = "IC_NAME")
    private String icName;

    /**
     * 数量单位
     */
    @Column(name = "IC_QUANTITY_UNIT")
    private String icQuantityUnit;

    /**
     * 排序
     */
    @Column(name = "IC_SORT")
    private Integer icSort;

    /**
     * 备注
     */
    @Column(name = "IC_REMARK")
    private String icRemark;

    /**
     * 是否删除
     */
    @Column(name = "IC_IS_DELETE")
    private Integer icIsDelete;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 更新人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 更新时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

	public IndustryClass(Integer id, String uuid, String icFid, String icName, String icQuantityUnit, Integer icSort, String icRemark, Integer icIsDelete, String createUser, Date createDate, String updateUser, Date updateDate) {
        this.id = id;
        this.uuid = uuid;
        this.icFid = icFid;
        this.icName = icName;
        this.icQuantityUnit = icQuantityUnit;
        this.icSort = icSort;
        this.icRemark = icRemark;
        this.icIsDelete = icIsDelete;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
    }

    public IndustryClass() {
        super();
    }

    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取父ID
     *
     * @return IC_FID - 父ID
     */
    public String getIcFid() {
        return icFid;
    }

    /**
     * 设置父ID
     *
     * @param icFid 父ID
     */
    public void setIcFid(String icFid) {
        this.icFid = icFid == null ? null : icFid.trim();
    }

    /**
     * 获取分类名称
     *
     * @return IC_NAME - 分类名称
     */
    public String getIcName() {
        return icName;
    }

    /**
     * 设置分类名称
     *
     * @param icName 分类名称
     */
    public void setIcName(String icName) {
        this.icName = icName == null ? null : icName.trim();
    }

    /**
     * 获取数量单位
     *
     * @return IC_QUANTITY_UNIT - 数量单位
     */
    public String getIcQuantityUnit() {
        return icQuantityUnit;
    }

    /**
     * 设置数量单位
     *
     * @param icQuantityUnit 数量单位
     */
    public void setIcQuantityUnit(String icQuantityUnit) {
        this.icQuantityUnit = icQuantityUnit == null ? null : icQuantityUnit.trim();
    }

    /**
     * 获取排序
     *
     * @return IC_SORT - 排序
     */
    public Integer getIcSort() {
        return icSort;
    }

    /**
     * 设置排序
     *
     * @param icSort 排序
     */
    public void setIcSort(Integer icSort) {
        this.icSort = icSort;
    }

    /**
     * 获取备注
     *
     * @return IC_REMARK - 备注
     */
    public String getIcRemark() {
        return icRemark;
    }

    /**
     * 设置备注
     *
     * @param icRemark 备注
     */
    public void setIcRemark(String icRemark) {
        this.icRemark = icRemark == null ? null : icRemark.trim();
    }

    /**
     * 获取是否删除
     *
     * @return IC_IS_DELETE - 是否删除
     */
    public Integer getIcIsDelete() {
        return icIsDelete;
    }

    /**
     * 设置是否删除
     *
     * @param icIsDelete 是否删除
     */
    public void setIcIsDelete(Integer icIsDelete) {
        this.icIsDelete = icIsDelete;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新人
     *
     * @return UPDATE_USER - 更新人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置更新人
     *
     * @param updateUser 更新人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取更新时间
     *
     * @return UPDATE_DATE - 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间
     *
     * @param updateDate 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}