package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_user_to_role")
public class UserToRoleInfo implements Serializable {
    /**
     * 用户id
     */
    @Column(name = "USER_ID")
    private String userId;

    /**
     * 公司id
     */
    @Column(name = "CORP_ID")
    private String corpId;

    /**
     * 角色id
     */
    @Column(name = "ROLE_ID")
    private String roleId;

    /**
     * 员工工号
     */
    @Column(name = "JOB_NUMBER")
    private String jobNumber;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    private static final long serialVersionUID = 1L;

    public UserToRoleInfo(String userId, String corpId, String roleId, String jobNumber, Date createDate) {
        this.userId = userId;
        this.corpId = corpId;
        this.roleId = roleId;
        this.jobNumber = jobNumber;
        this.createDate = createDate;
    }

    public UserToRoleInfo() {
        super();
    }

    /**
     * 获取用户id
     *
     * @return USER_ID - 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 获取公司id
     *
     * @return CORP_ID - 公司id
     */
    public String getCorpId() {
        return corpId;
    }

    /**
     * 设置公司id
     *
     * @param corpId 公司id
     */
    public void setCorpId(String corpId) {
        this.corpId = corpId == null ? null : corpId.trim();
    }

    /**
     * 获取角色id
     *
     * @return ROLE_ID - 角色id
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * 设置角色id
     *
     * @param roleId 角色id
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    /**
     * 获取员工工号
     *
     * @return JOB_NUMBER - 员工工号
     */
    public String getJobNumber() {
        return jobNumber;
    }

    /**
     * 设置员工工号
     *
     * @param jobNumber 员工工号
     */
    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber == null ? null : jobNumber.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}