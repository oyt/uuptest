package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_user_info")
public class UserInfo implements Serializable {
    /**
     * 自增id
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 用户id
     */
    @Column(name = "USER_ID")
    private String userId;

    /**
     * 用户名
     */
    @Column(name = "USER_NAME")
    private String userName;

    /**
     * 手机号
     */
    @Column(name = "USER_PHONE")
    private String userPhone;

    /**
     * 邮箱
     */
    @Column(name = "USER_EMAIL")
    private String userEmail;

    /**
     * 密码
     */
    @Column(name = "USER_PASSWORD")
    private String userPassword;

    /**
     * 头像地址
     */
    @Column(name = "USER_HEADIMGURL")
    private String userHeadimgurl;

    /**
     * 性别0保密1男2女
     */
    @Column(name = "USER_SEX")
    private Integer userSex;

    /**
     * 联系座机
     */
    @Column(name = "USER_TEL")
    private String userTel;

    /**
     * 真实姓名
     */
    @Column(name = "USER_REAL_NAME")
    private String userRealName;

    /**
     * 身份证号
     */
    @Column(name = "USER_ID_NUMBER")
    private String userIdNumber;

    /**
     * 身份证文件地址
     */
    @Column(name = "USER_ID_CARD_URI")
    private String userIdCardUri;

    /**
     * 职能
     */
    @Column(name = "USER_FUNCTION")
    private Integer userFunction;

    /**
     * 微信id
     */
    @Column(name = "WECHAT_OPENID")
    private String wechatOpenid;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    /**
     * 微信昵称
     */
    @Column(name = "WECHAT_NICKNAME")
    private byte[] wechatNickname;

    private static final long serialVersionUID = 1L;

    public UserInfo(Integer id, String uuid, String userId, String userName, String userPhone, String userEmail, String userPassword, String userHeadimgurl, Integer userSex, String userTel, String userRealName, String userIdNumber, String userIdCardUri, Integer userFunction, String wechatOpenid, String createUser, Date createDate, String updateUser, Date updateDate, byte[] wechatNickname) {
        this.id = id;
        this.uuid = uuid;
        this.userId = userId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userHeadimgurl = userHeadimgurl;
        this.userSex = userSex;
        this.userTel = userTel;
        this.userRealName = userRealName;
        this.userIdNumber = userIdNumber;
        this.userIdCardUri = userIdCardUri;
        this.userFunction = userFunction;
        this.wechatOpenid = wechatOpenid;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
        this.wechatNickname = wechatNickname;
    }

    public UserInfo() {
        super();
    }

    /**
     * 获取自增id
     *
     * @return ID - 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增id
     *
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取用户id
     *
     * @return USER_ID - 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 获取用户名
     *
     * @return USER_NAME - 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名
     *
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 获取手机号
     *
     * @return USER_PHONE - 手机号
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * 设置手机号
     *
     * @param userPhone 手机号
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone == null ? null : userPhone.trim();
    }

    /**
     * 获取邮箱
     *
     * @return USER_EMAIL - 邮箱
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * 设置邮箱
     *
     * @param userEmail 邮箱
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    /**
     * 获取密码
     *
     * @return USER_PASSWORD - 密码
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * 设置密码
     *
     * @param userPassword 密码
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    /**
     * 获取头像地址
     *
     * @return USER_HEADIMGURL - 头像地址
     */
    public String getUserHeadimgurl() {
        return userHeadimgurl;
    }

    /**
     * 设置头像地址
     *
     * @param userHeadimgurl 头像地址
     */
    public void setUserHeadimgurl(String userHeadimgurl) {
        this.userHeadimgurl = userHeadimgurl == null ? null : userHeadimgurl.trim();
    }

    /**
     * 获取性别0保密1男2女
     *
     * @return USER_SEX - 性别0保密1男2女
     */
    public Integer getUserSex() {
        return userSex;
    }

    /**
     * 设置性别0保密1男2女
     *
     * @param userSex 性别0保密1男2女
     */
    public void setUserSex(Integer userSex) {
        this.userSex = userSex;
    }

    /**
     * 获取联系座机
     *
     * @return USER_TEL - 联系座机
     */
    public String getUserTel() {
        return userTel;
    }

    /**
     * 设置联系座机
     *
     * @param userTel 联系座机
     */
    public void setUserTel(String userTel) {
        this.userTel = userTel == null ? null : userTel.trim();
    }

    /**
     * 获取真实姓名
     *
     * @return USER_REAL_NAME - 真实姓名
     */
    public String getUserRealName() {
        return userRealName;
    }

    /**
     * 设置真实姓名
     *
     * @param userRealName 真实姓名
     */
    public void setUserRealName(String userRealName) {
        this.userRealName = userRealName == null ? null : userRealName.trim();
    }

    /**
     * 获取身份证号
     *
     * @return USER_ID_NUMBER - 身份证号
     */
    public String getUserIdNumber() {
        return userIdNumber;
    }

    /**
     * 设置身份证号
     *
     * @param userIdNumber 身份证号
     */
    public void setUserIdNumber(String userIdNumber) {
        this.userIdNumber = userIdNumber == null ? null : userIdNumber.trim();
    }

    /**
     * 获取身份证文件地址
     *
     * @return USER_ID_CARD_URI - 身份证文件地址
     */
    public String getUserIdCardUri() {
        return userIdCardUri;
    }

    /**
     * 设置身份证文件地址
     *
     * @param userIdCardUri 身份证文件地址
     */
    public void setUserIdCardUri(String userIdCardUri) {
        this.userIdCardUri = userIdCardUri == null ? null : userIdCardUri.trim();
    }

    /**
     * 获取职能
     *
     * @return USER_FUNCTION - 职能
     */
    public Integer getUserFunction() {
        return userFunction;
    }

    /**
     * 设置职能
     *
     * @param userFunction 职能
     */
    public void setUserFunction(Integer userFunction) {
        this.userFunction = userFunction;
    }

    /**
     * 获取微信id
     *
     * @return WECHAT_OPENID - 微信id
     */
    public String getWechatOpenid() {
        return wechatOpenid;
    }

    /**
     * 设置微信id
     *
     * @param wechatOpenid 微信id
     */
    public void setWechatOpenid(String wechatOpenid) {
        this.wechatOpenid = wechatOpenid == null ? null : wechatOpenid.trim();
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取修改人
     *
     * @return UPDATE_USER - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_DATE - 修改时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取微信昵称
     *
     * @return WECHAT_NICKNAME - 微信昵称
     */
    public byte[] getWechatNickname() {
        return wechatNickname;
    }

    /**
     * 设置微信昵称
     *
     * @param wechatNickname 微信昵称
     */
    public void setWechatNickname(byte[] wechatNickname) {
        this.wechatNickname = wechatNickname;
    }
}