package com.zsu.uup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "zs_uup_enterprise_agent_brand")
public class EnterpriseAgentBrandInfo implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "UUID")
    private String uuid;

    /**
     * 公司id
     */
    @Column(name = "CORP_ID")
    private String corpId;

    /**
     * 品牌名称
     */
    @Column(name = "BRAND_NAME")
    private String brandName;

    /**
     * 授权单位
     */
    @Column(name = "BRAND_LICENSE_UNITS")
    private String brandLicenseUnits;

    /**
     * 经营范围
     */
    @Column(name = "BRAND_AGENCY_SCOPE")
    private String brandAgencyScope;

    /**
     * 代理等级
     */
    @Column(name = "BRAND_PROXY_RANK")
    private String brandProxyRank;

    /**
     * 有效期开始时间
     */
    @Column(name = "BRAND_START_DATE")
    private Date brandStartDate;

    /**
     * 有效期结束时间
     */
    @Column(name = "BRAND_END_DATE")
    private Date brandEndDate;

    /**
     * 上传文件名
     */
    @Column(name = "BRAND_FILE_NAME")
    private String brandFileName;

    /**
     * 文件地址
     */
    @Column(name = "BRAND_FILE_URI")
    private String brandFileUri;

    /**
     * 备注
     */
    @Column(name = "BRAND_REMARK")
    private String brandRemark;

    /**
     * 状态0审核中1已审核2审核未通过
     */
    @Column(name = "CHECK_STATUS")
    private Integer checkStatus;

    /**
     * 是否已删除
     */
    @Column(name = "IS_DELETE")
    private Integer isDelete;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    public EnterpriseAgentBrandInfo(Integer id, String uuid, String corpId, String brandName, String brandLicenseUnits, String brandAgencyScope, String brandProxyRank, Date brandStartDate, Date brandEndDate, String brandFileName, String brandFileUri, String brandRemark, Integer checkStatus, Integer isDelete, String createUser, Date createDate, String updateUser, Date updateDate) {
        this.id = id;
        this.uuid = uuid;
        this.corpId = corpId;
        this.brandName = brandName;
        this.brandLicenseUnits = brandLicenseUnits;
        this.brandAgencyScope = brandAgencyScope;
        this.brandProxyRank = brandProxyRank;
        this.brandStartDate = brandStartDate;
        this.brandEndDate = brandEndDate;
        this.brandFileName = brandFileName;
        this.brandFileUri = brandFileUri;
        this.brandRemark = brandRemark;
        this.checkStatus = checkStatus;
        this.isDelete = isDelete;
        this.createUser = createUser;
        this.createDate = createDate;
        this.updateUser = updateUser;
        this.updateDate = updateDate;
    }

    public EnterpriseAgentBrandInfo() {
        super();
    }

    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    /**
     * 获取公司id
     *
     * @return CORP_ID - 公司id
     */
    public String getCorpId() {
        return corpId;
    }

    /**
     * 设置公司id
     *
     * @param corpId 公司id
     */
    public void setCorpId(String corpId) {
        this.corpId = corpId == null ? null : corpId.trim();
    }

    /**
     * 获取品牌名称
     *
     * @return BRAND_NAME - 品牌名称
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * 设置品牌名称
     *
     * @param brandName 品牌名称
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName == null ? null : brandName.trim();
    }

    /**
     * 获取授权单位
     *
     * @return BRAND_LICENSE_UNITS - 授权单位
     */
    public String getBrandLicenseUnits() {
        return brandLicenseUnits;
    }

    /**
     * 设置授权单位
     *
     * @param brandLicenseUnits 授权单位
     */
    public void setBrandLicenseUnits(String brandLicenseUnits) {
        this.brandLicenseUnits = brandLicenseUnits == null ? null : brandLicenseUnits.trim();
    }

    /**
     * 获取经营范围
     *
     * @return BRAND_AGENCY_SCOPE - 经营范围
     */
    public String getBrandAgencyScope() {
        return brandAgencyScope;
    }

    /**
     * 设置经营范围
     *
     * @param brandAgencyScope 经营范围
     */
    public void setBrandAgencyScope(String brandAgencyScope) {
        this.brandAgencyScope = brandAgencyScope == null ? null : brandAgencyScope.trim();
    }

    /**
     * 获取代理等级
     *
     * @return BRAND_PROXY_RANK - 代理等级
     */
    public String getBrandProxyRank() {
        return brandProxyRank;
    }

    /**
     * 设置代理等级
     *
     * @param brandProxyRank 代理等级
     */
    public void setBrandProxyRank(String brandProxyRank) {
        this.brandProxyRank = brandProxyRank == null ? null : brandProxyRank.trim();
    }

    /**
     * 获取有效期开始时间
     *
     * @return BRAND_START_DATE - 有效期开始时间
     */
    public Date getBrandStartDate() {
        return brandStartDate;
    }

    /**
     * 设置有效期开始时间
     *
     * @param brandStartDate 有效期开始时间
     */
    public void setBrandStartDate(Date brandStartDate) {
        this.brandStartDate = brandStartDate;
    }

    /**
     * 获取有效期结束时间
     *
     * @return BRAND_END_DATE - 有效期结束时间
     */
    public Date getBrandEndDate() {
        return brandEndDate;
    }

    /**
     * 设置有效期结束时间
     *
     * @param brandEndDate 有效期结束时间
     */
    public void setBrandEndDate(Date brandEndDate) {
        this.brandEndDate = brandEndDate;
    }

    /**
     * 获取上传文件名
     *
     * @return BRAND_FILE_NAME - 上传文件名
     */
    public String getBrandFileName() {
        return brandFileName;
    }

    /**
     * 设置上传文件名
     *
     * @param brandFileName 上传文件名
     */
    public void setBrandFileName(String brandFileName) {
        this.brandFileName = brandFileName == null ? null : brandFileName.trim();
    }

    /**
     * 获取文件地址
     *
     * @return BRAND_FILE_URI - 文件地址
     */
    public String getBrandFileUri() {
        return brandFileUri;
    }

    /**
     * 设置文件地址
     *
     * @param brandFileUri 文件地址
     */
    public void setBrandFileUri(String brandFileUri) {
        this.brandFileUri = brandFileUri == null ? null : brandFileUri.trim();
    }

    /**
     * 获取备注
     *
     * @return BRAND_REMARK - 备注
     */
    public String getBrandRemark() {
        return brandRemark;
    }

    /**
     * 设置备注
     *
     * @param brandRemark 备注
     */
    public void setBrandRemark(String brandRemark) {
        this.brandRemark = brandRemark == null ? null : brandRemark.trim();
    }

    /**
     * 获取状态0审核中1已审核2审核未通过
     *
     * @return CHECK_STATUS - 状态0审核中1已审核2审核未通过
     */
    public Integer getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置状态0审核中1已审核2审核未通过
     *
     * @param checkStatus 状态0审核中1已审核2审核未通过
     */
    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取是否已删除
     *
     * @return IS_DELETE - 是否已删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否已删除
     *
     * @param isDelete 是否已删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建人
     *
     * @return CREATE_USER - 创建人
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建人
     *
     * @param createUser 创建人
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取修改人
     *
     * @return UPDATE_USER - 修改人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改人
     *
     * @param updateUser 修改人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * 获取修改时间
     *
     * @return UPDATE_DATE - 修改时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置修改时间
     *
     * @param updateDate 修改时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}