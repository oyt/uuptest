package com.zsu.uup.toSql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class CompanyUtil {


	//sql server 企业信息表
	public String getCompanyInfo(){
		String companyInfo = "  select   zc.CI_Id,zc.CI_LogoUrl,zc.CI_name," +
				"(case when (zc.CI_ShortName is null or zc.CI_ShortName='') then '' else zc.CI_ShortName end ) as CI_ShortName," +
				"zc.CI_BizType,(CI_BizSales+CI_BizTypeMemo) as BizSales,"
        		+ "zc.CI_ProvinceID,zc.CI_Province,zc.CI_DistrictID,zc.CI_District,zc.CI_CityID,zc.CI_City,zc.CI_Address,zm.longitude ,zm.Latitude "
        		+ " FROM ZS_CorpInfo zc right join ZS_UserInfo zu on zc.CI_Id = zu.CI_Id right join ZS_MapPosition zm on zm.UI_LoginName = zu.UI_LoginName  WHERE zc.CI_State = 2 ";
        return companyInfo;
    }

	//sql server 企业信息表
	public String getCompanyInfo1(){
		String companyInfo =   "select   zscinfo.CI_Id,zscinfo.CI_LogoUrl,zscinfo.CI_name," +
		"(case when (zscinfo.CI_ShortName is null or zscinfo.CI_ShortName='') then '' else zscinfo.CI_ShortName end ) as CI_ShortName," +
				"zscinfo.CI_BizType,(CI_BizSales+CI_BizTypeMemo) as BizSales,"+
				"zscinfo.CI_ProvinceID,zscinfo.CI_Province,zscinfo.CI_DistrictID,zscinfo.CI_District,zscinfo.CI_CityID,zscinfo.CI_City,zscinfo.CI_Address,'' as longitude ,'' as Latitude "+
				" from ZS_CorpInfo zscinfo where zscinfo.CI_Id in (\n" +
				"select bbbb.CI_Id from (\n" +
				"select zsc.CI_Id from  ZS_CorpInfo zsc  WHERE zsc.CI_State = 2 group by zsc.CI_Id\n" +
				") as bbbb\n" +
				"where \n" +
				"not EXISTS(\n" +
				"\tselect   zc.CI_Id\n" +
				"\t   FROM ZS_CorpInfo zc right join ZS_UserInfo zu \n" +
				"\t   on zc.CI_Id = zu.CI_Id right join ZS_MapPosition zm \n" +
				"\t   on zm.UI_LoginName = zu.UI_LoginName  WHERE zc.CI_State = 2 \n" +
				"\t   and bbbb.CI_Id = zc.CI_Id\n" +
				")  \n" +
				") ";
		return companyInfo;
	}
	//mysql 企业信息表
	public String setCompanyInf(){
		String compInfo = "INSERT into zs_uup_enterprise_info "
				+ "(cid_test," +
				"CORP_ID," +
				"CORP_LOGO," +
				"CORP_NAME," +
				"CORP_SHORT_NAME," +
				"CORP_IDENTITIES," +
				"CORP_BUSINESS_MODEL," +
				"CORP_BUSINESS_CATEGORY," +
				"PROVINCE,"+
				"PROVINCE_CODE," +
				"CITY_CODE," +
				"CITY," +
				"DISTRICE_CODE," +
				"DISTRICE," +
				"CORP_ADDRESS," +
				"CORP_LONGITUDE," +
				"CORP_LATITUDE," +
				"CORP_ENGLISH_NAME)"
				+ " values(?,REPLACE(UUID(),'-',''),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		return compInfo;
	}
	
	//企业查询返回的数据集合
	public String[] companyMap(ResultSet result){
		Map<String,Object> map  = new HashMap<String, Object>();
		String[] str = new String[17];
		try {
			int id = result.getInt("CI_Id");
			str[0]= id+"";
			String logoUrl = result.getString("CI_LogoUrl");
			str[1]= logoUrl+"";
			String name = result.getString("CI_name");
			str[2]= name+"";
			String shortName = result.getString("CI_ShortName");
			str[3]= shortName+"";
			String BizType = result.getString("CI_BizType");
			//销售（企业身份：经销商  16，经营模式：经销代理 2） 采购(企业身份：经销商，塑料制品，其他8336   经营模式 ：生产加工，经销代理 3)
			if(BizType != null && !BizType.equals("")){
				if(BizType.equals("1")){
					str[4]="16";
					str[5]="2";
				}else if(BizType.equals("2")){
					str[4]="8336";
					str[5]="3";
				}else{
					str[4]="8336";
					str[5]="3";
				}

			}else{
				str[4]="8336";
				str[5]="3";
			}
			String BizSales = result.getString("BizSales");
			str[6]= BizSales+"";
			String provinceId = result.getString("CI_ProvinceID");
			str[7]= provinceId+"";
			String province = result.getString("CI_Province");
			str[8]= province+"";
			String districtId = result.getString("CI_DistrictID");
			str[9]= districtId+"";
			String district = result.getString("CI_District");
			str[10]= district+"";
			String cityId = result.getString("CI_CityID");
			str[11]= cityId+"";
			String city = result.getString("CI_City");
			str[12]= city+"";
			String address = result.getString("CI_Address");
			str[13]= address+"";
			String longitude = result.getString("longitude");
			str[14]= longitude+"";
			String Latitude = result.getString("Latitude");
			str[15]= Latitude+"";
			str[16]= "";

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	//sql server工商信息表
	public String getBussiness(){
//		String bussinessInfo = "select zca.CI_Id, ddd.CI_name,  zca.Cai_BusinessType , ddd.CI_ProvinceID,ddd.CI_Province,ddd.CI_DistrictID,ddd.CI_District,\n" +
//				"ddd.CI_CityID,ddd.CI_City,ddd.CI_Address,\n" +
//				"zca.Cai_RegistrationNo, zca.Cai_BusinessTerm , zca.Cai_LegalRepresentative,zca.Cai_Capital,zca.Cai_BusinessScope\n" +
//				"from ZS_CorpAuthenticateInfo zca\n" +
//				"left join \n" +
//				"(\n" +
//				"select zcui.UI_LoginName,zc.CI_name,zc.CI_Id,zc.CI_ProvinceID,zc.CI_Province,zc.CI_DistrictID,zc.CI_District,zc.CI_CityID,zc.CI_City,zc.CI_Address from \n" +
//				"ZS_CorpInfo zc right join ZS_UserInfo zu on zc.CI_Id = zu.CI_Id right join ZS_CorpToUserIcon zcui on zcui.UI_LoginName = zu.UI_LoginName\n" +
//				"where UII_Id = 1 and zc.CI_State = 2\n" +
//				") as ddd on ddd.CI_Id = zca.CI_Id";

		String bussinessInfo = "select abs.* from (\n" +
				"select zca.CI_Id, ddd.CI_name,  zca.Cai_BusinessType , ddd.CI_ProvinceID,ddd.CI_Province,ddd.CI_DistrictID,ddd.CI_District,\n" +
				"ddd.CI_CityID,ddd.CI_City,ddd.CI_Address,\n" +
				"zca.Cai_RegistrationNo, zca.Cai_BusinessTerm , zca.Cai_LegalRepresentative,zca.Cai_Capital,zca.Cai_BusinessScope\n" +
				"from ZS_CorpAuthenticateInfo zca\n" +
				"left join \n" +
				"(\n" +
				"select zcui.UI_LoginName,zc.CI_name,zc.CI_Id,zc.CI_ProvinceID,zc.CI_Province,zc.CI_DistrictID,zc.CI_District,zc.CI_CityID,zc.CI_City,zc.CI_Address from \n" +
				"ZS_CorpInfo zc right join ZS_UserInfo zu on zc.CI_Id = zu.CI_Id right join ZS_CorpToUserIcon zcui on zcui.UI_LoginName = zu.UI_LoginName\n" +
				"where UII_Id = 1 and zc.CI_State = 2\n" +
				") as ddd on ddd.CI_Id = zca.CI_Id\n" +
				")as abs\n" +
				"where abs.CI_name is not null";

		return bussinessInfo;
	}



	//mysql 工商信息表
	public String setBussiness(){
		String compInfo = "INSERT into zs_uup_business_info "
				+ "(" +
				"CORP_ID," +
				"BUSINESS_NAME," +
				"BUSINESS_TYPE," +
				"PROVINCE,"+
				"PROVINCE_CODE," +
				"CITY_CODE," +
				"CITY," +
				"DISTRICE_CODE," +
				"DISTRICE," +
				"ADDRESS," +
				"CREDIT_CODE," +
				"LICENSE_START_DATE," +
				"LICENSE_END_DATE,"+
				"LEGAL_REPRESENTATIVE,"+
				"REGISTERED_CAPITAL,"+
				"BUSINESS_SCOPE,"
				+"MULTIPLE"
				+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1)";
		return compInfo;
	}


	/**
	 * -- 公司ID,名称，    公司类型    , 注册省code                                                                     ,社会统一信用代码  ,营业执照生效日期,   法人代表            ,注册资本   ,经营范围
	 * --CI_Id,CI_name,Cai_BusinessType,CI_ProvinceID,CI_Province,CI_DistrictID,CI_District,CI_CityID,CI_City,CI_Address,Cai_RegistrationNo,Cai_BusinessTerm,Cai_LegalRepresentative,Cai_Capital,Cai_BusinessScope
	 * */
	//工商信息查询返回的数据集合
	public String[] bussinessMap(ResultSet result){
		Map<String,Object> map  = new HashMap<String, Object>();
		String[] str = new String[16];
		try {
			int id = result.getInt("CI_Id");
			str[0]= id+"";
			String name = result.getString("CI_name");
			str[1]= "";
			String BusinessType = result.getString("Cai_BusinessType");
			str[2]= BusinessType+"";
			String provinceId = result.getString("CI_ProvinceID");
			str[3]= provinceId+"";
			String province = result.getString("CI_Province");
			str[4]= province+"";
			String districtId = result.getString("CI_DistrictID");
			str[5]= districtId+"";
			String district = result.getString("CI_District");
			str[6]= district+"";
			String cityId = result.getString("CI_CityID");
			str[7]= cityId+"";
			String city = result.getString("CI_City");
			str[8]= city+"";
			String address = result.getString("CI_Address");
			str[9]= address+"";
			String RegistrationNo = result.getString("Cai_RegistrationNo");
			str[10]= RegistrationNo+"";
			String BusinessTerm = result.getString("Cai_BusinessTerm");
			if (BusinessTerm.indexOf("至")!=-1){
				str[11]= BusinessTerm.split("至")[0];
				str[12]= BusinessTerm.split("至")[1];
			}
			String LegalRepresentative = result.getString("Cai_LegalRepresentative");
			str[13]= LegalRepresentative+"";
			String Capital = result.getString("Cai_Capital");
			str[14]= Capital+"";
			String BusinessScope = result.getString("Cai_BusinessScope");
			str[15]= BusinessScope+"";

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	
	//sql server 资质证书表
	public String  getQuality(){
		String qualityInfo = "select UI_LoginName,'诚信资质类型' as aqfname,'诚信资质类型' as aqftype,AQ_AddTime,AQ_Attachment from PF_AssociationQualification where AQ_State =1";
		return qualityInfo;
	}

    //sql server 资质证书表2
    public String  getQuality2(){
        String qualityInfo = "select UI_LoginName,'诚信资质类型' as aqfname,'诚信资质类型' as aqftype,HQ_AddTime,HQ_Attachment from PF_HonestyQualification where HQ_State !=2 and HT_Id in (4,5,6)";
        return qualityInfo;
    }

	//mysql 资质证书表
	public String setQuality(){
		String compInfo = "INSERT into zs_uup_qualification_certificate "
				+ "(cid_test," +
				"CORP_ID," +
				"CORP_LOGO," +
				"CORP_NAME," +
				"CORP_SHORT_NAME," +
				"CORP_IDENTITIES," +
				"CORP_BUSINESS_MODEL," +
				"CORP_BUSINESS_CATEGORY," +
				"PROVINCE,"+
				"PROVINCE_CODE," +
				"CITY_CODE," +
				"CITY," +
				"DISTRICE_CODE," +
				"DISTRICE," +
				"CORP_ADDRESS," +
				"CORP_LONGITUDE," +
				"CORP_LATITUDE," +
				"CORP_ENGLISH_NAME)"
				+ " values(?,REPLACE(UUID(),'-',''),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		return compInfo;
	}


	//资质证书表 查询返回的数据集合
	public String[] qualityMap(ResultSet result){
		Map<String,Object> map  = new HashMap<String, Object>();
		String[] str = new String[17];
		try {
			int id = result.getInt("CI_Id");
			str[0]= id+"";
			String logoUrl = result.getString("CI_LogoUrl");
			str[1]= logoUrl+"";
			String name = result.getString("CI_name");
			str[2]= name+"";
			String shortName = result.getString("CI_ShortName");
			str[3]= shortName+"";
			String BizType = result.getString("CI_BizType");
			//销售（企业身份：经销商  16，经营模式：经销代理 2） 采购(企业身份：经销商，塑料制品，其他8336   经营模式 ：生产加工，经销代理 3)
			if(BizType != null && !BizType.equals("")){
				if(BizType.equals("1")){
					str[4]="16";
					str[5]="2";
				}else if(BizType.equals("2")){
					str[4]="8336";
					str[5]="3";
				}else{
					str[4]="8336";
					str[5]="3";
				}

			}else{
				str[4]="8336";
				str[5]="3";
			}
			String BizSales = result.getString("BizSales");
			str[6]= BizSales+"";
			String provinceId = result.getString("CI_ProvinceID");
			str[7]= provinceId+"";
			String province = result.getString("CI_Province");
			str[8]= province+"";
			String districtId = result.getString("CI_DistrictID");
			str[9]= districtId+"";
			String district = result.getString("CI_District");
			str[10]= district+"";
			String cityId = result.getString("CI_CityID");
			str[11]= cityId+"";
			String city = result.getString("CI_City");
			str[12]= city+"";
			String address = result.getString("CI_Address");
			str[13]= address+"";
			String longitude = result.getString("longitude");
			str[14]= longitude+"";
			String Latitude = result.getString("Latitude");
			str[15]= Latitude+"";
			str[16]= "";

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return str;
	}


	//品牌代理表
	public String getppdl(){
		String qinfo = "select UI_LoginName,'其他','其他','其他',PQ_Remark,PQ_Attachment from PF_ProxyQualification where PQ_State = 1";
		return qinfo;
	}

	//mysql 品牌代理表
	public String setppdl(){
		String compInfo = "INSERT into zs_uup_enterprise_agent_brand "
				+ "(cid_test," +
				"CORP_ID," +
				"CORP_LOGO," +
				"CORP_NAME," +
				"CORP_SHORT_NAME," +
				"CORP_IDENTITIES," +
				"CORP_BUSINESS_MODEL," +
				"CORP_BUSINESS_CATEGORY," +
				"PROVINCE,"+
				"PROVINCE_CODE," +
				"CITY_CODE," +
				"CITY," +
				"DISTRICE_CODE," +
				"DISTRICE," +
				"CORP_ADDRESS," +
				"CORP_LONGITUDE," +
				"CORP_LATITUDE," +
				"CORP_ENGLISH_NAME)"
				+ " values(?,REPLACE(UUID(),'-',''),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		return compInfo;
	}


	//品牌代理表 查询返回的数据集合
	public String[] ppdlMap(ResultSet result){
		Map<String,Object> map  = new HashMap<String, Object>();
		String[] str = new String[17];
		try {
			int id = result.getInt("CI_Id");
			str[0]= id+"";
			String logoUrl = result.getString("CI_LogoUrl");
			str[1]= logoUrl+"";
			String name = result.getString("CI_name");
			str[2]= name+"";
			String shortName = result.getString("CI_ShortName");
			str[3]= shortName+"";
			String BizType = result.getString("CI_BizType");
			//销售（企业身份：经销商  16，经营模式：经销代理 2） 采购(企业身份：经销商，塑料制品，其他8336   经营模式 ：生产加工，经销代理 3)
			if(BizType != null && !BizType.equals("")){
				if(BizType.equals("1")){
					str[4]="16";
					str[5]="2";
				}else if(BizType.equals("2")){
					str[4]="8336";
					str[5]="3";
				}else{
					str[4]="8336";
					str[5]="3";
				}

			}else{
				str[4]="8336";
				str[5]="3";
			}
			String BizSales = result.getString("BizSales");
			str[6]= BizSales+"";
			String provinceId = result.getString("CI_ProvinceID");
			str[7]= provinceId+"";
			String province = result.getString("CI_Province");
			str[8]= province+"";
			String districtId = result.getString("CI_DistrictID");
			str[9]= districtId+"";
			String district = result.getString("CI_District");
			str[10]= district+"";
			String cityId = result.getString("CI_CityID");
			str[11]= cityId+"";
			String city = result.getString("CI_City");
			str[12]= city+"";
			String address = result.getString("CI_Address");
			str[13]= address+"";
			String longitude = result.getString("longitude");
			str[14]= longitude+"";
			String Latitude = result.getString("Latitude");
			str[15]= Latitude+"";
			str[16]= "";

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return str;
	}


    //中塑企业认证表
    public String getzsrzb(){
        String qinfo = "select UI_LoginName,'诚信资质类型' as aqfname,'诚信资质类型' as aqftype,HQ_AddTime,HQ_Attachment from PF_HonestyQualification where HQ_State !=2 and HT_Id in (1,2,3)";
        return qinfo;
    }

	//mysql 中塑企业认证表
	public String setzsrzb(){
		String compInfo = "INSERT into zs_uup_enterprise_info "
				+ "(cid_test," +
				"CORP_ID," +
				"CORP_LOGO," +
				"CORP_NAME," +
				"CORP_SHORT_NAME," +
				"CORP_IDENTITIES," +
				"CORP_BUSINESS_MODEL," +
				"CORP_BUSINESS_CATEGORY," +
				"PROVINCE,"+
				"PROVINCE_CODE," +
				"CITY_CODE," +
				"CITY," +
				"DISTRICE_CODE," +
				"DISTRICE," +
				"CORP_ADDRESS," +
				"CORP_LONGITUDE," +
				"CORP_LATITUDE," +
				"CORP_ENGLISH_NAME)"
				+ " values(?,REPLACE(UUID(),'-',''),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		return compInfo;
	}


	//中塑企业认证表 查询返回的数据集合
	public String[] zsrzbMap(ResultSet result){
		Map<String,Object> map  = new HashMap<String, Object>();
		String[] str = new String[17];
		try {
			int id = result.getInt("CI_Id");
			str[0]= id+"";
			String logoUrl = result.getString("CI_LogoUrl");
			str[1]= logoUrl+"";
			String name = result.getString("CI_name");
			str[2]= name+"";
			String shortName = result.getString("CI_ShortName");
			str[3]= shortName+"";
			String BizType = result.getString("CI_BizType");
			//销售（企业身份：经销商  16，经营模式：经销代理 2） 采购(企业身份：经销商，塑料制品，其他8336   经营模式 ：生产加工，经销代理 3)
			if(BizType != null && !BizType.equals("")){
				if(BizType.equals("1")){
					str[4]="16";
					str[5]="2";
				}else if(BizType.equals("2")){
					str[4]="8336";
					str[5]="3";
				}else{
					str[4]="8336";
					str[5]="3";
				}

			}else{
				str[4]="8336";
				str[5]="3";
			}
			String BizSales = result.getString("BizSales");
			str[6]= BizSales+"";
			String provinceId = result.getString("CI_ProvinceID");
			str[7]= provinceId+"";
			String province = result.getString("CI_Province");
			str[8]= province+"";
			String districtId = result.getString("CI_DistrictID");
			str[9]= districtId+"";
			String district = result.getString("CI_District");
			str[10]= district+"";
			String cityId = result.getString("CI_CityID");
			str[11]= cityId+"";
			String city = result.getString("CI_City");
			str[12]= city+"";
			String address = result.getString("CI_Address");
			str[13]= address+"";
			String longitude = result.getString("longitude");
			str[14]= longitude+"";
			String Latitude = result.getString("Latitude");
			str[15]= Latitude+"";
			str[16]= "";

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	
}

