package com.zsu.uup.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.beetl.ext.fn.Json;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.ProductInfoService;
import com.zsu.uup.entity.ProductInfo;
import com.zsu.uup.pageinfo.PageInfo;

@RequestMapping(value = "GetProduct")
@Controller
public class ProductInfoController {
	
	@Reference
	private ProductInfoService priductinfoService;
	
	

	/**
	 * <p>
	 * Title: getAllProductInfo
	 * </p>
	 * 获得产品信息
	 * @author WangShiPing (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/GetProductInfo", method = RequestMethod.GET)
	@ResponseBody
	public String getAllProductInfo() {
		List<ProductInfo> plist = priductinfoService.getAllProductInfo();
		for (ProductInfo p : plist) {
			System.out.println(p.getId() + "--" + p.getProductNameZh() + "	");
		}
		return "success";
	}
	/**
	 * 
	 * <p>Title: getProductInfoPaging</p>  
	 * 		分页查询产品信息
	 * @author WangShiPing (Create on:2018年6月20日)
	 * @version 1.0
	 * @param req Request请求
	 * @param resp  响应
	 * @param limit 用于分页参数
	 * @param count 用于分页参数
	 * @return
	 */
	 
	@RequestMapping(value="/GetProductInfoPaging",method=RequestMethod.GET)
	public ModelAndView getProductInfoPaging(HttpServletRequest req,HttpServletResponse resp,String limit,String count){
		System.out.println("ajax传过来的数据："+limit+"---"+count);
		ModelAndView model = new ModelAndView("PersonFile/NewFile");
		Map<String, String> map = new HashMap<String, String>();
		map.put(limit, count);
		PageInfo<ProductInfo> plist = priductinfoService.getListProductInfo(map);
		System.out.println(plist);
		model.addObject("plist", plist.getRows());
		return model;
	}

//	@RequestMapping(value = "/getjson", method = RequestMethod.GET)
//	@ResponseBody
//	public String getjson() {
//		JSONObject json = new JSONObject();
//		json.put("test", "test");
//		return json.toString();
//	}
	/**
	 * 用于到前台界面通过ajax请求拿到数据
	 * <p>Title: goDeskPage</p>  
	 * 
	 * @author WangShiPing (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/goDeskPage", method = RequestMethod.GET)
	public ModelAndView goDeskPage(){
		return new ModelAndView("PersonFile/NewFile");
	}
	/**
	 * 
	 * <p>Title: getProductAndGeneral</p>  
	 * 		查询产品详细信息
	 * @author WangShiPing (Create on:2018年6月20日)
	 * @version 1.0
	 * @param id 
	 * @return 
	 */
	@RequestMapping(value = "/getProductAndGeneral", method = RequestMethod.GET)
	@ResponseBody
	public String getProductAndGeneral(){
		//需要接收前台传入的id
		int i=1;
		int count = priductinfoService.getProductInfoAndGeneralDes(i);
		System.out.println(count);
		return "success";
	}
	
	
	public String getProductAndGenerala(){
		//需要接收前台传入的id
		int i=1;
		int count = priductinfoService.getProductInfoAndGeneralDes(i);
		System.out.println(count);
		return "success";
	}
	
	
	
}
