package com.zsu.uup.toSql;

import com.aliyun.oss.OSSClient;

import java.util.function.Function;

public class ZJCPOClient {

    private static final String endpoint        = "http://oss-cn-shanghai.aliyuncs.com";
    private static final String accessKeyId     = "LTAIqR73sQgPLtnh";
    private static final String accessKeySecret = "BaGpLhR1m83vyCqel7fdoQUbHuS4Au";
    private static final String bucket          = "21cp-online-image";
    private static final String filePackageUrl  = "test/";
    private static final String imgPackageUrl   = "img/";





    private OSSClient client;

    public OSSClient getClient() {
        if (client == null) {
            client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        }
        return client;
    }

    private Object[] _args;

    public Object[] getArgs() {
        return _args;
    }

    public Object getArg(int index) {
        return _args[index];
    }

    public String getBucket() {
        return bucket;
    }

    private ZJCPOClient(Object[] args) {
        this._args = args;
    }

    /**
     * ossclient的简单封装，省去了授权过程
     *
     * @param <T>
     * */
    public static <T> T connect(Function<ZJCPOClient, T> function, Object... args) {
        ZJCPOClient ossClient = null;
        try {
            ossClient = new ZJCPOClient(args);
            return function.apply(ossClient);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (ossClient != null && ossClient.client != null) {
                ossClient.client.shutdown();
            }
        }
    }


    /**
     *
     * @param objectName
     * @return
     */
    public static boolean ossFileDelete(String objectName){

        return connect(ZJCPOClient -> {
            if(ZJCPOClient.getClient().doesObjectExist(ZJCPOClient.getBucket(), objectName)){
                ZJCPOClient.getClient().deleteObject(ZJCPOClient.getBucket(), objectName);
                return true;
            }else{
                return false;
            }
        });
    }

}
