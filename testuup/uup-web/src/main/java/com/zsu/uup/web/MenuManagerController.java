package com.zsu.uup.web;

import java.util.List;

import org.bouncycastle.mail.smime.handlers.multipart_signed;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.common.json.JSONArray;
import com.alibaba.dubbo.common.json.JSONObject;
import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.MenuManagerService;
import com.zsu.uup.entity.MenuManagerDto;

/**
 * 用户中心菜单管理
 * <p>
 * Title: MenuManagerController
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author wangyongge (Create on:2018年6月19日)
 * @version 1.0
 * @fileName MenuManagerController.java
 */
@RequestMapping(value = "menuManager")
@Controller
public class MenuManagerController {
	@Reference
	public MenuManagerService menuManagerService;

	/**
	 * 测试
	 * <p>Title: userCenterMenuManager</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月29日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/userCenterMenu", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView userCenterMenuManager() {
		ModelAndView model=new ModelAndView("menuManager/UserCenterMenuManager");
		List<MenuManagerDto> menuManagerList = menuManagerService.selectTopmenu();
		model.addObject("menuManagerList", menuManagerList);	
		return model;

	}
	//二级菜单
	@RequestMapping(value = "/childMenuManager", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView childMenuManager(String uuid) {
		ModelAndView model=new ModelAndView("menuManager/UserCenterMenuManager");
		List<MenuManagerDto> childList = menuManagerService.selectChildmenu(uuid);
		model.addObject("childList", childList);	
		return model;

	}
	//三级菜单
	@RequestMapping(value = "/grandMenuManager", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView grandMenuManager(String uuid) {
		ModelAndView model=new ModelAndView("menuManager/UserCenterMenuManager");
		List<MenuManagerDto> grandList = menuManagerService.selectGrandSonmenu(uuid);
		model.addObject("grandList", grandList);	
		return model;

	}
	
	/**
	 * 查询所有用户菜单
	 * <p>
	 * Title: QueryAllMenuManager
	 * </p>
	 * 
	 * @author wangyongge (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/queryMenu", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView queryAllMenuManager() {
		ModelAndView model=new ModelAndView("menuManager/menu");
		List<MenuManagerDto> menuManagerList = menuManagerService.selectTopmenu();
		List<MenuManagerDto> childList = menuManagerService.selectChildmenu("1");
		List<MenuManagerDto> grandList = menuManagerService.selectGrandSonmenu("2");
		model.addObject("menuManagerList", menuManagerList);	
		model.addObject("childList", childList);
		model.addObject("grandList", grandList);
		return model;

	}
	/**
	 * 全部菜单查询
	 * <p>Title: queryAllUserMenuManager</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月29日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/queryMenuManager", method = RequestMethod.GET)
	@ResponseBody
	public List<MenuManagerDto> queryAllUserMenuManager() {
		//ModelAndView model=new ModelAndView("menuManager/menu");
		List<MenuManagerDto> menuManagerList = menuManagerService.queryAllMenuManager();
		//model.addObject("menuManagerList", menuManagerList);	
		
		return menuManagerList;
	}


	// 根据uuid查询关联子级菜单
	@RequestMapping(value = "/selectById", method = RequestMethod.GET)
	@ResponseBody
	public List<MenuManagerDto> selectMenuManagerById() {
		List<MenuManagerDto> managerDtosList = menuManagerService
				.selectMenuManagerDtoByUUIdOrParentId("1");
		System.out.println(managerDtosList);
		for (MenuManagerDto managerDto : managerDtosList) {
			System.out.println(managerDto);
		}
		return managerDtosList;
	}

	/**
	 * 添加顶级菜单
	 * <p>Title: insertMenuManager</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月29日)
	 * @version 1.0
	 * @param menuManagerDto
	 * @return
	 */
	@RequestMapping(value = "/addMenu", method = RequestMethod.POST)
	@ResponseBody
	public MenuManagerDto insertMenuManager(MenuManagerDto menuManagerDto) {
		/*MenuManagerDto menuManagerDto = new MenuManagerDto();
		menuManagerDto.setMenuName("首页");*/
		int result = menuManagerService.insertMenuManager(menuManagerDto);
		if (result > 0) {
			System.out.println("成功");
		}
		return menuManagerDto;

	}
	
	
	/**
	 * 添加子级菜单
	 * <p>Title: insertChildMenu</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月29日)
	 * @version 1.0
	 * @param menuManagerDto
	 * @return
	 */
		@RequestMapping(value = "/addchildMenu", method = RequestMethod.POST)
		@ResponseBody
		public MenuManagerDto insertChildMenu(MenuManagerDto menuManagerDto) {
			int result = menuManagerService.insertChildMenu(menuManagerDto);
			if (result > 0) {
				System.out.println("成功");
			}
			return menuManagerDto;

		}


}
