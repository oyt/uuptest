package com.zsu.uup.toSql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.mysql.jdbc.PreparedStatement;

public class userInfoUtil {

    /**
     * 1.获取公司信息数据（公司审核信息）
     *
     * 2.获取用户信息数据
     *
     * 3.获取公司和会员组关系数据
     *
     * 4.获取所有服务
     *
     * */
	//用户和公司关联查询
	public String getCompanyAndUser(){
	String cAndUser="select ri.CORP_ID,ui.USER_ID from zs_uup_enterprise_info ri inner join zs_uup_user_info ui on ri.cid_test=ui.cid_test";
	 return cAndUser;
	}
	//添加用户和公司中间表
	public String addCompanyAndUser(){	
	String addcAnduser="insert into zs_uup_user_to_role(user_id,corp_id) values(?,?)";
	return addcAnduser;
	}
   //查询所有用户信息
   public  String getUserInfo(){
	   String getUser="SELECT CI_Id,UI_LoginName,UI_LoginPassword,UI_NickName,UI_Sex,UI_Tel,UI_MobilePhone,UI_Email from ZS_UserInfo where ui_state in('0','1','2')";
     return getUser;
   }
   //返回集合数据
   public Map<String, Object> userInfoMap(ResultSet result){
	   Map<String, Object> userMap=new HashMap<String, Object>();
	   try {
		int ciId=result.getInt("CI_ID");
		userMap.put("ciId", ciId);
		String uiLoginName=result.getString("UI_LoginName");
		userMap.put("uiLoginName", uiLoginName);
		String uiLoginPassword=result.getString("UI_LoginPassword");
		userMap.put("uiLoginPassword", uiLoginPassword);
		String uiNickName=result.getString("UI_NickName");
		userMap.put("uiNickName", uiNickName);
		int uiSex=result.getInt("UI_Sex");
		userMap.put("uiSex", uiSex);
		String uiTel=result.getString("UI_Tel");
		userMap.put("uiTel", uiTel);
		String uiMobilePhone=result.getString("UI_MobilePhone");
		userMap.put("uiMobilePhone", uiMobilePhone);
		String uiEmail=result.getString("UI_Email");
		userMap.put("uiEmail", uiEmail);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	   return userMap;
   }
   //添加用户信息
   public  String getAddUserInfo(){
	   String addUser="insert into zs_uup_user_info(user_id,cid_test,user_name,user_password,user_real_name,user_sex,user_tel,user_phone,user_email) values(reples(uuid(),'-',''),?,?,?,?,?,?,?,?)";
       return addUser;
   }
	
	//数据库连接信息
	private static final  String URL = "jdbc:sqlserver://192.168.3.11:1433;DatabaseName=CPODataArk";
	private static final String USER = "cp21";
	private static final String PASSWORD = "21cp.com";
	
	private static Connection conn=null;
    //静态代码块（将加载驱动、连接数据库放入静态块中）
    static{
        try {
            //1.加载驱动程序
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //2.获得数据库的连接
            conn=(Connection)DriverManager.getConnection(URL,USER,PASSWORD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
  //对外提供一个方法来获取数据库连接
    public static Connection getConnection(){
        return conn;
    }
    
    
    //测试
  /*  public static void main(String[] args) throws Exception{
        
        //3.通过数据库链接操作数据库
        Statement stmt = conn.createStatement();
        //ResultSet executeQuery(String sqlString)：执行查询数据库的SQL语句   ，返回一个结果集（ResultSet）对象。
        //公司对应的用户        
        //String sql="select ci_id,UI_LoginName from ZS_UserInfo";
        ResultSet rs = stmt.executeQuery("SELECT UI_Id,CI_Id,UI_LoginName,UI_LoginPassword,UI_NickName,UI_Sex,UI_Tel,UI_MobilePhone,UI_Email,UI_AddTime from ZS_UserInfo");
		while(rs.next()){
		System.out.println(rs.getString("UI_LoginName")+","+rs.getString("UI_LoginPassword")+","+rs.getString("UI_NickName")+","+rs.getInt("UI_Sex")+","+rs.getString("UI_Tel")+","+rs.getString("UI_MobilePhone")+","+rs.getString("UI_Email")+","+rs.getString("UI_AddTime")+","+rs.getString("UI_Position"));
		}
    }*/
    public void insertInfo(ResultSet result){
    	userInfoUtil user = new userInfoUtil();
    	PreparedStatement ps = null;
		try {
			int i = 0;
			while(result.next()){
				//用户数据插入sql
				String userSql = user.getAddUserInfo();
				
				ps = (PreparedStatement) conn.prepareStatement(userSql);

				ps.executeUpdate();
				System.out.println(i++ +"     it work -------");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

	public static void main(String[] args) {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE,1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date=cal.getTime();
		System.out.println(sdf.format(date));
	}


}
