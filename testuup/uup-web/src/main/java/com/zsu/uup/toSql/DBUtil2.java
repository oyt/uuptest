package com.zsu.uup.toSql;

import java.sql.*;


public class DBUtil2 {
	
	//数据库连接信息
	private static final  String URL = "jdbc:sqlserver://192.168.3.11:1433;DatabaseName=MemberRatingDB";
	private static final String USER = "cp21";
	private static final String PASSWORD = "21cp.com";
	private static Connection conn=null;
    //静态代码块（将加载驱动、连接数据库放入静态块中）
    static{
        try {
            //1.加载驱动程序
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //2.获得数据库的连接
            conn=(Connection)DriverManager.getConnection(URL,USER,PASSWORD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
  //对外提供一个方法来获取数据库连接
    public static Connection getConnection(){
        return conn;
    }

    public ResultSet getResult() {
    	Statement stmt = null;
    	ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			CompanyUtil com = new CompanyUtil();
			rs = stmt.executeQuery(com.getCompanyInfo());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
    }
}
