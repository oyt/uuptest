package com.zsu.uup.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.RoleInfoService;
import com.zsu.uup.entity.RoleInfoDto;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 角色管理
 * <p>
 * Title: RoleInfoController
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author wangyongge (Create on:2018年6月20日)
 * @version 1.0
 * @fileName RoleInfoController.java
 */
@RequestMapping(value = "roleInfo")
@Controller
public class RoleInfoController {
	@Reference
	public RoleInfoService roleInfoService;

	/**
	 * 查询所有角色管理
	 * <p>
	 * Title: seletAllRoleInfo
	 * </p>
	 * 
	 * @author wangyongge (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/queryAllRole", method = RequestMethod.GET)
	@ResponseBody
	public List<RoleInfoDto> seletAllRoleInfo() {
		List<RoleInfoDto> roleInfoDtosList = roleInfoService
				.selectAllRoleInfo();
		return roleInfoDtosList;
	}

	/**
	 * 添加角色名称
	 * <p>
	 * Title: insertRoleInfo
	 * </p>
	 * 
	 * @author wangyongge (Create on:2018年6月22日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/addRole", method = RequestMethod.GET)
	@ResponseBody
	public RoleInfoDto insertRoleInfo() {

		RoleInfoDto roleInfoDto = new RoleInfoDto();
		roleInfoDto.setRoleName("老板");
		roleInfoDto.setFunctionInfo("牛逼人物");
		roleInfoDto.setCreateUser("wwwwwwwwwwwwww");
		int result = roleInfoService.insertRoleInfo(roleInfoDto);
		if (result > 0) {
			System.out.println("success");
		}
		return roleInfoDto;
	}

	/**
	 * 更新角色
	 * <p>
	 * Title: updateRoleInfo
	 * </p>
	 * 
	 * @author wangyongge (Create on:2018年6月22日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/updateRole", method = RequestMethod.GET)
	@ResponseBody
	public RoleInfoDto updateRoleInfo() {
		RoleInfoDto roleInfoDto = new RoleInfoDto();
		roleInfoDto.setUuid("001");
		roleInfoDto.setRoleName("大大老板");
		roleInfoDto.setFunctionInfo("很是牛逼的人物！");
		int result = roleInfoService.updateRoleInfo(roleInfoDto);
		if (result > 0) {
			System.out.println("SUCCESS");
		}
		return roleInfoDto;
	}
	/**
	 * 删除用户角色
	 * <p>Title: deleteRoleInfo</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月27日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/deleteRole", method = RequestMethod.GET)
	@ResponseBody
	public RoleInfoDto deleteRoleInfo() {
		RoleInfoDto roleInfoDto = new RoleInfoDto();
		roleInfoDto.setUuid("001");
		roleInfoDto.setIsDelete(1);
		int result = roleInfoService.deleteRoleInfo(roleInfoDto);
		if (result > 0) {
			System.out.println("SUCCESS");
		}
		return roleInfoDto;
	}	
	
    /**
     * 分页查询
     * <p>Title: getRoleInfoList</p>  
     * 
     * @author wangyongge (Create on:2018年6月24日)
     * @version 1.0
     * @return
     */
	@RequestMapping(value = "/getRole", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView getRoleInfoList() {
		ModelAndView model = new ModelAndView("file/FileTest");
		Map<String, String> map = new HashMap<String, String>();
		PageInfo<RoleInfoDto> roList = roleInfoService.getRoleInfoList(map);
		model.addObject(roList.getRows());
		return model;
	}
	/**
	 * 角色添加权限
	 * <p>Title: addRoleAuth</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月27日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/addRoleAuth", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> addRoleAuth(){
		Map<String, Object> roleMap=new HashMap<String, Object>();
		List<String> lists=new ArrayList<String>();
		lists.add("8dbc211f78ef11e8b41500163e0ae163");
		lists.add("9760a21d78ef11e8b41500163e0ae163");
		roleMap.put("roleId", "001");
		roleMap.put("authId", lists);
		roleMap.put("isDelete", 0);
		int result=roleInfoService.addRoleAuth(roleMap);
		if(result > 0){
			System.out.println("SUCCESS");
		}
		return roleMap;
	}
	/**
	 * 删除角色权限
	 * <p>Title: deletRoleAuth</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月27日)
	 * @version 1.0
	 */
	@RequestMapping(value = "/deleteRoleAuth", method = RequestMethod.GET)
	@ResponseBody
	public void deletRoleAuth(){	
		int result=roleInfoService.deleteRoleAuth("001");
		if(result > 0){
			System.out.println("SUCCESS");
		}
	}

}
