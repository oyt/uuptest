package com.zsu.uup.toSql;

import com.alibaba.dubbo.common.utils.StringUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MysqlUtil {

	
	private static final  String URL1 = "jdbc:mysql://101.132.123.246:3307/zs_main_test?allowMultiQueries=true&useUnicode=true&characterEncoding=utf8&autoReconnect=true&failOverReadOnly=false&useSSL=false&rewriteBatchedStatements=true";
	private static final String USER1 = "testuser";
	private static final String PASSWORD1 = "testuser123!";
	
	private static Connection conn=null;
    //静态代码块（将加载驱动、连接数据库放入静态块中）
    static{
        try {
            //1.加载驱动程序
            Class.forName("com.mysql.jdbc.Driver");
            //2.获得数据库的连接
            conn=(Connection)DriverManager.getConnection(URL1,USER1,PASSWORD1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
  //对外提供一个方法来获取数据库连接
    public static Connection getConnection(){
        return conn;
    }

    /**
	 * 公司数据1
	 * */
    public synchronized void insertInfo(ResultSet result , String name){
    	CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil.getConnection();
		PreparedStatement ps = null;
		String compSql = comp.setCompanyInf();
		int insertindex = 0;
		Long startTime = System.currentTimeMillis();

		try {
			ps = conn.prepareStatement(compSql);
			while(result.next()){
				//企业数据插入sql
				insertindex++;
				String[] str = comp.companyMap(result);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				ps.addBatch();
				System.out.println(result.getRow());
			}
			try{
				ps.executeBatch();
			}catch(Exception e){
				System.out.println(e.getMessage());
			}

			Long endTime = System.currentTimeMillis();
			System.out.println("OK,用时：" + (endTime - startTime));
			System.out.println(result.getRow() +"-------"+name);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(conn!=null){
				                 try {
									 ps.close();
					                     conn.close();
					                 } catch (SQLException e) {
					                     e.printStackTrace();
					                     throw new RuntimeException(e);
					                 }
				            }
		}
    }





    /**
	 * 公司数据2
	 * */
	public synchronized void insertInfo1(ResultSet result , String name){
		CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil.getConnection();
		try {
			while(result.next()){
				//企业数据插入sql
				String compSql = comp.setCompanyInf();
				String[] str = comp.companyMap(result);
				PreparedStatement ps = conn.prepareStatement(compSql);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				try{
					ps.executeUpdate();
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				System.out.println(result.getRow());
			}
			System.out.println(result.getRow() +"-------"+name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 工商信息
	 * */
	public synchronized void insertInfo6(ResultSet result , String name){
		CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil.getConnection();
		try {
			while(result.next()){
				//插入工商信息
				String compSql = comp.setBussiness();
				String[] str = comp.bussinessMap(result);
				PreparedStatement ps = conn.prepareStatement(compSql);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				try{
					ps.executeUpdate();
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				System.out.println(result.getRow());
			}
			System.out.println(result.getRow() +"-------"+name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	/**
	 * 资质证书表1
	 * */
	public synchronized void insertInfo2(ResultSet result , String name){
		CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil.getConnection();
		try {
			while(result.next()){
				String compSql = comp.setQuality();
				String[] str = comp.qualityMap(result);
				PreparedStatement ps = conn.prepareStatement(compSql);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				try{
					ps.executeUpdate();
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				System.out.println(result.getRow());
			}
			System.out.println(result.getRow() +"-------"+name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 资质证书表2
	 * */
	public synchronized void insertInfo3(ResultSet result , String name){
		CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil.getConnection();
		try {
			while(result.next()){
				//插入工商信息
				String compSql = comp.setQuality();
				String[] str = comp.qualityMap(result);
				PreparedStatement ps = conn.prepareStatement(compSql);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				try{
					ps.executeUpdate();
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				System.out.println(result.getRow());
			}
			System.out.println(result.getRow() +"-------"+name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	/**
	 * 品牌代理表
	 * */
	public synchronized void insertInfo4(ResultSet result , String name){
		CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil.getConnection();
		try {
			while(result.next()){
				String compSql = comp.setppdl();
				String[] str = comp.ppdlMap(result);
				PreparedStatement ps = conn.prepareStatement(compSql);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				try{
					ps.executeUpdate();
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				System.out.println(result.getRow());
			}
			System.out.println(result.getRow() +"-------"+name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 中塑企业认证表
	 * */
	public synchronized void insertInfo5(ResultSet result , String name){
		CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil.getConnection();
		try {
			while(result.next()){
				String compSql = comp.setzsrzb();
				String[] str = comp.zsrzbMap(result);
				PreparedStatement ps = conn.prepareStatement(compSql);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				try{
					ps.executeUpdate();
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				System.out.println(result.getRow());
			}
			System.out.println(result.getRow() +"-------"+name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static boolean isMessyCode(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			// 当从Unicode编码向某个字符集转换时，如果在该字符集中没有对应的编码，则得到0x3f（即问号字符?）
			//从其他字符集向Unicode编码转换时，如果这个二进制数在该字符集中没有标识任何的字符，则得到的结果是0xfffd
			//System.out.println("--- " + (int) c);
			/*if ((int) c == 0x3f) {
				// 存在乱码
				//System.out.println("存在乱码 " + (int) c);
				return true;
			}*/
			if((int) c == 0xfffd){
				return true;
			}
		}
		return false;
	}
    //表情过滤
	public static boolean findEmoji(String content) {
		//[🀄-🈚]|[👦-👩]|[👦🏻-👩🏿]|[🙅🏻-🙏🏿]|[🀀-🟿]|[🤐-🧀]|[😀-🙏]|[🚀-🛶]
		Pattern pattern = Pattern.compile("[^\\u0000-\\uFFFF]");
		Matcher matcher = pattern.matcher(content);
		if (matcher.find()) {
			return true;
		} else {
			return false;
		}
	}

    /**
     * 用户信息
     */
    public synchronized void insertUser(ResultSet result , String name){
        UserUtil userUtil=new UserUtil();
		PreparedStatement ps = null;
        conn=MysqlUtil.getConnection();
		String compSql = userUtil.insertUserInfo();
		int count = 0;
		Long startTime = System.currentTimeMillis();
        try {
			ps = conn.prepareStatement(compSql);
            while(result.next()){
            	count++;
                String[] str = userUtil.userInfoMap(result);
               /*  if(findEmoji(str[5])){
               //表情过滤
					 System.out.println(str[12] + "===="+count);
				 }*/
			   for (int i = 0,j = 1; i < str.length; i++,j++) {
				ps.setString(j, str[i]);
			   }
			   ps.addBatch();
               if (count != 0 && count %10000==0){
					try{
						ps.executeBatch();
						ps.clearBatch();
						Long endTime = System.currentTimeMillis();
						System.out.println("OK,用时：" + (endTime - startTime));
						System.out.println(result.getRow() +"-------"+name);
						System.out.println("-----------------------"+count);
					}catch(Exception e){
						System.out.println(e.getMessage());
					}
				}
            }
			ps.executeBatch();
			ps.clearBatch();
			Long endTime = System.currentTimeMillis();
			System.out.println("OK,用时：" + (endTime - startTime));
			System.out.println(result.getRow() +"========"+name);
			System.out.println("============================="+count);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
			try {
				if(result != null){
					result.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(ps!=null){
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(conn!=null){
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
    }


    /**
     * 默认角色信息
     * @return
     */
    public ResultSet getRoleInfo(){
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            RoleInfoUtil roleUtil=new RoleInfoUtil();
            String sql=roleUtil.getDefaultRoleInfo();
            rs=stmt.executeQuery(sql);
            System.out.println(sql+"```````````````````````````");
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


	/**
	 * 企业信息
	 * @return
	 */
	public ResultSet getCompany(){
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			RoleInfoUtil roleUtil=new RoleInfoUtil();
			String sql=roleUtil.getCompany();
			rs=stmt.executeQuery(sql);
			System.out.println(sql+"```````````````````````````");
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

    /**
     * 角色信息
     */
    public synchronized void insertRole(ResultSet comp ,ResultSet role, String name){
        RoleInfoUtil roleInfoUtil=new RoleInfoUtil();
        conn=MysqlUtil.getConnection();
		PreparedStatement ps=null;
        ArrayList list= new ArrayList();
		String compSql = roleInfoUtil.insertRoleInfo();
		Long startTime = System.currentTimeMillis();
        int corpIndex=0;
        try {
			ps = conn.prepareStatement(compSql);
            while (role.next()){
                String[] strRole = roleInfoUtil.roleInfoMap(role);
                list.add(strRole);
            }
            while(comp.next()){
            	corpIndex ++;
                //企业列表
                String[] company = roleInfoUtil.companyMap(comp);
                for (int i = 0; i < company.length; i++) {
					for (int k =0 ; k < list.size(); k++) {
						String[] strRole2 = (String[])list.get(k);
						ps.setString(1, strRole2[0]);
						ps.setString(2, strRole2[1]);
						ps.setString(3, strRole2[2]);
						ps.setString(4, strRole2[3]);
						ps.setString(5,company[i]);
						ps.addBatch();
					}
					if(corpIndex !=0 && corpIndex % 10000 == 0){
						try{
							ps.executeBatch();
							ps.clearBatch();
							Long endTime = System.currentTimeMillis();
							System.out.println("OK,用时：" + (endTime - startTime));
							System.out.println(comp.getRow() +"-------"+name);
							System.out.println("======="+corpIndex);
						}catch(Exception e){
							System.out.println(e.getMessage());
						}
					}
                }
			}
			ps.executeBatch();
			ps.clearBatch();
			Long endTime = System.currentTimeMillis();
			System.out.println("OK,用时：" + (endTime - startTime));
			System.out.println(comp.getRow() +"-------"+name);
			System.out.println("======="+corpIndex);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
			try {
				if(role!=null){
					role.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(ps!=null){
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(conn!=null){
				conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
    }


    /**
     * 获取公司用户角色信息
     * @return
     */
    public ResultSet getUserToCompany(){
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            UserAndCompanyAndRoleUtil util=new UserAndCompanyAndRoleUtil();
            String sql=util.getUserAndCompany();
            rs=stmt.executeQuery(sql);
            System.out.println(sql+"```````````````````````````");
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


    /**
     * 添加用户和公司,角色中间表
     */
    public synchronized void insertUserAndCompany(ResultSet result , String name){
        UserAndCompanyAndRoleUtil util=new UserAndCompanyAndRoleUtil();
		PreparedStatement ps = null;
        conn=MysqlUtil.getConnection();
		String compSql = util.insertUserToComp();
		Long startTime = System.currentTimeMillis();
        int userAndCompIndex = 0;
        try {
			ps = conn.prepareStatement(compSql);
            while(result.next()){
            	userAndCompIndex ++;
				String[] str = util.userAndRoleMap(result);
                for (int i = 0,j = 1; i < str.length; i++,j++) {
                    ps.setString(j, str[i]);
                    ps.addBatch();
                    if(userAndCompIndex != 0 && userAndCompIndex%1000 == 0 ){
						try{
							ps.executeBatch();
							ps.clearBatch();
							Long endTime = System.currentTimeMillis();
							System.out.println("OK,用时：" + (endTime - startTime));
							System.out.println(result.getRow() +"-------"+name);
						}catch(Exception e){
							System.out.println(e.getMessage());
						}
					}
                }
                System.out.println(result.getRow());
            }
			ps.executeBatch();
			ps.clearBatch();
			Long endTime = System.currentTimeMillis();
			System.out.println("OK,用时：" + (endTime - startTime));
            System.out.println(result.getRow() +"-------"+name);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
			try {
				if(result!=null){
					result.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(ps!=null){
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(conn!=null){
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
    }


	/**
	 * 未审核用户信息
	 * @return
	 */
	public ResultSet getCheckUserInfo(){
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			UserInfoCheckUtil checkUtil=new UserInfoCheckUtil();
			String sql=checkUtil.getCheckUserInfo();
			rs=stmt.executeQuery(sql);
			System.out.println(sql+"```````````````````````````");
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}


	/**
	 * 用户审核信息表
	 */
	public synchronized void insertCheckUser(ResultSet result , String name){
		UserInfoCheckUtil userInfoCheckUtil=new UserInfoCheckUtil();
		PreparedStatement ps = null;
		conn=MysqlUtil.getConnection();
		String compSql = userInfoCheckUtil.insertUserCheckInfo();
		int count = 0;
		Long startTime = System.currentTimeMillis();
		try {
			ps = conn.prepareStatement(compSql);
			while(result.next()){
				count++;
				String[] str =userInfoCheckUtil.userCheckInfoMap(result);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					ps.setString(j, str[i]);
				}
				ps.addBatch();
				if (count != 0 && count%10000==0){
					try{
						ps.executeBatch();
						ps.clearBatch();
						Long endTime = System.currentTimeMillis();
						System.out.println("OK,用时：" + (endTime - startTime));
						System.out.println(result.getRow() +"-------"+name);
						System.out.println("======"+count);
					}catch(Exception e){
						System.out.println(e.getMessage());
					}
				}
			}
			ps.executeBatch();
			ps.clearBatch();
			Long endTime = System.currentTimeMillis();
			System.out.println("OK,用时：" + (endTime - startTime));
			System.out.println(result.getRow() +"-------"+name);
			System.out.println("======"+count);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				if(result!=null){
					result.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(ps!=null){
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			try {
				if(conn!=null){
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
	}


}
