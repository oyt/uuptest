/**
 * oyt
 */
package com.zsu.uup.web;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.UserService;
import com.zsu.uup.entity.TestInfoDto;

/**
 * <p>
 * 
 * </p>
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * @author Tao.Ouyang (Create on:2018年5月30日)
 * @version 1.0
 * @fileName UserController.java
 */
@RequestMapping(value = "userinfo")
@Controller
public class UserController {

    @Reference
    public UserService userService;

    @RequestMapping(value = "/getUsername", method = RequestMethod.GET)
    public ModelAndView getUserName() {

        ModelAndView model = new ModelAndView("test/NewFile");

        Map<String, String> map = new HashMap<String, String>();

        com.zsu.uup.pageinfo.PageInfo<TestInfoDto> list = userService.getListUser(map);

        System.out.println("111" + list);

        model.addObject("total", "dddd");

        return model;
    }

    @RequestMapping(value = "/getjson", method = RequestMethod.GET)
    @ResponseBody
    public String getjson() {
        JSONObject json = new JSONObject();
        json.put("test", "test");
        return json.toString();
    }

    public int getClassId() {
        return 1;
    }
}
