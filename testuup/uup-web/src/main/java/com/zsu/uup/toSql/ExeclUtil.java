package com.zsu.uup.toSql;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExeclUtil {



    /**
     * 对应关系表
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getKeyValue(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();

        String[] json = new String[2];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null || hssfRow.getCell((short)0)==null || hssfRow.getCell((short)0).getStringCellValue().trim().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)0).getStringCellValue())){
                json[0]=hssfRow.getCell((short)0).getStringCellValue();
                json[1]=hssfRow.getCell((short)1).getStringCellValue();
                reMap.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                json = new String[2];
            }
        }
        return Object;
    }


    /**
     * 获取行业分类
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getIndustryClass(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

        Map<String, String[]> Object = new HashMap<String, String[]>();

        Map<String, String[]> reMap = new HashMap<String, String[]>();
        Map<String, String[]> reMap2 = new HashMap<String, String[]>();
        Map<String, String[]> reMap3 = new HashMap<String, String[]>();


        String[] json = new String[11];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null || hssfRow.getCell((short)0)==null || hssfRow.getCell((short)0).getStringCellValue().trim().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)0).getStringCellValue().trim())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=0+"";
                json[2]=hssfRow.getCell((short)0).getStringCellValue();
                json[3]="";
                json[4]="1";
                json[5]="1";
                json[6]="";
                json[7]="0";
                json[8]="admin";
                json[9]="admin";
                json[10]=hssfRow.getCell((short)0).getStringCellValue();
                reMap.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                json = new String[11];
            }
            if(hssfRow.getCell((short)1)!=null&&!hssfRow.getCell((short)1).getStringCellValue().trim().equals("")&&!reMap2.containsKey(hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim())){
                String[] parentid = reMap.get(hssfRow.getCell((short)0).getStringCellValue());
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=parentid[0];
                json[2]=hssfRow.getCell((short)1).getStringCellValue();
                json[3]="";
                json[4]="2";
                json[5]="2";
                json[6]="";
                json[7]="0";
                json[8]="admin";
                json[9]="admin";
                String cellname = hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim();
                json[10]=cellname;
                reMap2.put(cellname,json);
                Object.put(cellname,json);
                json = new String[11];
            }

            if(hssfRow.getCell((short)2)!=null&&!hssfRow.getCell((short)2).getStringCellValue().trim().equals("")&&!reMap3.containsKey(hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim()+">>"+hssfRow.getCell((short)2).getStringCellValue().trim())){
                String cellname2 = hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim();
                String[] parentid = reMap2.get(cellname2);
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=parentid[0];
                json[2]=hssfRow.getCell((short)2).getStringCellValue().trim();
                json[3]="";
                json[4]="3";
                json[5]="3";
                json[6]="";
                json[7]="0";
                json[8]="admin";
                json[9]="admin";
                String cellname = hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim()+">>"+hssfRow.getCell((short)2).getStringCellValue().trim();
                json[10]=cellname;
                reMap3.put(cellname,json);

                Object.put(cellname,json);
                json = new String[11];
            }

        }

        return Object;
    }



    /**
     * 获取厂家管理
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getManufactor(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

        Map<String, String[]> Object = new HashMap<String, String[]>();

        Map<String, String[]> Object2 = new HashMap<String, String[]>();

        Map<String, String[]> reMap = new HashMap<String, String[]>();

        String[] json = new String[15];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null) {
                continue;
            }
            if(null != hssfRow.getCell((short)2) && !reMap.containsKey(hssfRow.getCell((short)2).getStringCellValue()) &&  !"".equals(hssfRow.getCell((short)2).getStringCellValue())){
//                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
//                json[0]=uuid;
                json[0]=hssfRow.getCell((short)2).getStringCellValue();//名称
                json[1]=hssfRow.getCell((short)1) == null ? "":hssfRow.getCell((short)1).getStringCellValue();//英文名称
                json[2]=hssfRow.getCell((short)3) == null ? "":hssfRow.getCell((short)3).getStringCellValue();//简称
                json[3]=hssfRow.getCell((short)6) == null ? "":hssfRow.getCell((short)6).getStringCellValue();//地址
                json[4]=hssfRow.getCell((short)7) == null ? "":hssfRow.getCell((short)7).getStringCellValue();//网址
                json[5]="";//关键词
                Double num = hssfRow.getCell((short)0).getNumericCellValue();
                String name = String.valueOf(num.intValue());
                File file = OSSUtil.getFileName(name);
                if(null == file || !file.exists()){
                    json[6]="";//logo
                }else{
                    Map<String,Object> map =  OSSUtil.pcUpload1(file,"manufactor","logo");
                    System.out.println(file);
                    if("500".equals(map.get("code")) || "400".equals(map.get("code"))){
                        System.out.println("logo上传失败： "+file);
                        json[6]="";//logo
                    }else{
                        Map<String,String> item = new HashMap<>();
                        item = (Map<String,String>)map.get("data");
                        json[6]=item.get("url");//logo
                    }
                }
                json[7]=hssfRow.getCell((short)9) == null?"":hssfRow.getCell((short)9).getStringCellValue();//详情
                json[8]="0";
                json[9]=hssfRow.getCell((short)5) == null?"":hssfRow.getCell((short)5).getStringCellValue();
                json[10]="";
                json[11]="";
                json[12]="";
                json[13]="admin";
                json[14]="admin";
                reMap.put(hssfRow.getCell((short)2).getStringCellValue(),json);
                json = new String[15];
                Object.put(hssfRow.getCell((short)2).getStringCellValue(),json);
                System.out.println(hssfRow.getCell((short)2).getStringCellValue()+"---"+rowNum);
            }else {
                System.out.println(""+rowNum);
            }
        }


        return Object;
    }


    /**
     * 获取品牌  疑问
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getBrand(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

        Map<String, String[]> Object = new HashMap<String, String[]>();

        Map<String, String[]> reMap = new HashMap<String, String[]>();

        /**
         *   `UUID` varchar(50) NOT NULL,
         *   `BRAND_NAME` varchar(50) NOT NULL COMMENT '名称',
         *   `BRAND_ENGLISH_NAME` varchar(36) DEFAULT NULL COMMENT '英文名',
         *   `BRAND_INITIALS` varchar(20) DEFAULT NULL COMMENT '首字母',
         *   `BRAND_LOGO` varchar(200) DEFAULT NULL COMMENT 'logo',
         *   `BRAND_BIG_LOGO` varchar(200) DEFAULT NULL COMMENT '大图',
         *   `BRAND_REMARK` text COMMENT '故事',
         *   `BRAND_SORT` int(11) DEFAULT NULL COMMENT '排序',
         *   `BRAND_IS_DISPLAY` int(11) DEFAULT NULL COMMENT '是否显示',
         *   `BRAND_IS_DELETE` int(11) DEFAULT NULL COMMENT '是否删除',
         *   `CREATE_USER` varchar(50) DEFAULT NULL COMMENT '创建人',
         *   `CREATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
         *   `UPDATE_USER` varchar(50) DEFAULT NULL COMMENT '更新人',
         *   `UPDATE_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
         * */
        String[] json = new String[19];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)2))){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)2).getStringCellValue();
                json[2]=hssfRow.getCell((short)1).getStringCellValue();
                json[3]=hssfRow.getCell((short)3).getStringCellValue();
                json[4]=hssfRow.getCell((short)6).getStringCellValue();
                json[5]=hssfRow.getCell((short)7).getStringCellValue();
                json[6]="";
                json[7]=hssfRow.getCell((short)0).getStringCellValue();
                json[8]=hssfRow.getCell((short)8).getStringCellValue();
                json[9]="0";
                json[10]=hssfRow.getCell((short)5).getStringCellValue();
                json[11]="";
                json[12]="";
                json[13]="";
                json[14]="";
                json[15]="";
                json[16]="";
                json[17]="admin";
                json[18]="admin";
                reMap.put(hssfRow.getCell((short)2).getStringCellValue(),json);
                json = new String[19];
                Object.put(hssfRow.getCell((short)2).getStringCellValue(),json);
            }
        }

        return Object;
    }


    /**
     * 产品符号列表
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getProductSymbol(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);

        Map<String, String[]> Object = new HashMap<String, String[]>();

        Map<String, String[]> reMap = new HashMap<String, String[]>();


        String[] json = new String[10];
        System.out.println("最大行数 = "+hssfSheet.getLastRowNum());
        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)4)) && null != hssfRow.getCell((short)4) && !hssfRow.getCell((short)4).getStringCellValue().trim().equals("")){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)4).getStringCellValue();
                json[2]=hssfRow.getCell((short)6) == null?"":hssfRow.getCell((short)6).getStringCellValue();
                json[3]=hssfRow.getCell((short)7) == null?"":hssfRow.getCell((short)7).getStringCellValue();
                String cellname = hssfRow.getCell((short)1).getStringCellValue()+">>"+hssfRow.getCell((short)2).getStringCellValue()+">>"+hssfRow.getCell((short)3).getStringCellValue();
                MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                String sql = "select uuid from zs_pd_industry_class where INDUSTRY_NAME_CONNECT = '"+cellname+"'";
                String id = mysqlUtil2.getSqlRest(sql);

                json[4]=id;
                json[5]="0";
                json[6]="admin";
                json[7]="admin";
                json[8]=hssfRow.getCell((short)5) == null?"":hssfRow.getCell((short)5).getStringCellValue();
                json[9]=hssfRow.getCell((short)8) == null?"":hssfRow.getCell((short)8).getStringCellValue();
                reMap.put(hssfRow.getCell((short)4).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)4).getStringCellValue(),json);
                json = new String[10];
            }
        }

        return Object;
    }


    /**
     * 特性列表
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getCharacteristic(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();

        String[] json = new String[7];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)2).getStringCellValue())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)2).getStringCellValue();
                json[2]=hssfRow.getCell((short)0).getStringCellValue();
                if(hssfRow.getCell((short)3)!=null && !hssfRow.getCell((short)3).getStringCellValue().equals("")){
                    json[3]=hssfRow.getCell((short)3).getStringCellValue();
                }else {
                    json[3]="";
                }
                json[4]="0";
                json[5]="admin";
                json[6]="admin";
                reMap.put(hssfRow.getCell((short)2).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)2).getStringCellValue(),json);
                json = new String[7];
            }else {
                System.out.println(hssfRow.getCell((short)2).getStringCellValue());
            }
        }

        return Object;
    }


    /**
     * 加工方法列表
     * @param fname
     * @return
     * @throws IOException
     * */
    public Map<String, String[]> getProcessing(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();

        String[] json = new String[7];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null  || hssfRow.getCell((short)1)==null || hssfRow.getCell((short)1).getStringCellValue().toString().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)1).getStringCellValue())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)1).getStringCellValue();
                json[2]=hssfRow.getCell((short)0).getStringCellValue();
                if(hssfRow.getCell((short)2)!=null && !hssfRow.getCell((short)2).getStringCellValue().toString().equals("")){
                    json[3]=hssfRow.getCell((short)2).getStringCellValue();
                }else {
                    json[3]="";
                }
                json[4]="0";
                json[5]="admin";
                json[6]="admin";
                reMap.put(hssfRow.getCell((short)1).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)1).getStringCellValue(),json);
                json = new String[7];
            }
        }
        return Object;
    }

    /**
     * 用途项列表
     * @param fname
     * @return
     * @throws IOException
     * */
    public Map<String, String[]> getPurpose(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();

        String[] json = new String[9];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null  || hssfRow.getCell((short)3)==null || hssfRow.getCell((short)3).getStringCellValue().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)3).getStringCellValue())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]="";
                json[2]=hssfRow.getCell((short)3).getStringCellValue();
                json[3]="";
                json[4]="";
                String id = null;
                if(hssfRow.getCell((short)2)!=null && !hssfRow.getCell((short)2).getStringCellValue().trim().equals("")){
                    String cellname = hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim()+">>"+hssfRow.getCell((short)2).getStringCellValue().trim();
                    MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                    String sql = "select uuid from zs_pd_industry_class where INDUSTRY_NAME_CONNECT = '"+cellname+"'";
                    id = mysqlUtil2.getSqlRest(sql);

                }else if (id==null && hssfRow.getCell((short)1)!=null && !hssfRow.getCell((short)1).getStringCellValue().trim().equals("")){
                    String cellname = hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim();
                    MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                    String sql = "select uuid from zs_pd_industry_class where INDUSTRY_NAME_CONNECT = '"+cellname+"'";
                    id = mysqlUtil2.getSqlRest(sql);
                }else if(id==null &&hssfRow.getCell((short)0)!=null && !hssfRow.getCell((short)0).getStringCellValue().trim().equals("")){
                    String cellname = hssfRow.getCell((short)0).getStringCellValue().trim();
                    MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                    String sql = "select uuid from zs_pd_industry_class where INDUSTRY_NAME_CONNECT = '"+cellname+"'";
                    id = mysqlUtil2.getSqlRest(sql);
                }
                if(id!=null){
                    json[5]=id;
                }else{
                    json[5]="";
                }
                json[6]="0";
                json[7]="admin";
                json[8]="admin";
                reMap.put(hssfRow.getCell((short)3).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)3).getStringCellValue(),json);
                json = new String[9];
            }
        }
        return Object;
    }


    /**
     * * 性能项目分组
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getPerformanceGroup(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();

        String[] json = new String[3];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)1))){
                json[0]=hssfRow.getCell((short)1).getStringCellValue();
                json[1]="admin";
                json[2]="admin";
                reMap.put(hssfRow.getCell((short)1).getStringCellValue(),json);
                json = new String[3];
                Object.put(hssfRow.getCell((short)1).getStringCellValue(),json);
            }
        }
        return Object;
    }



    /**
     * * 性能项目列表
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String,Map<String, String[]>> getPerformance(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();
        Map<String,Map<String, String[]>> list = new HashMap<String,Map<String, String[]>>();

        String[] json = new String[7];

        String[] unit = new String[3];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null || hssfRow.getCell((short)0)==null || hssfRow.getCell((short)1)==null || hssfRow.getCell((short)0).getStringCellValue().trim().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)0).getStringCellValue().trim()+hssfRow.getCell((short)1).getStringCellValue().trim())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)0).getStringCellValue();
                if(hssfRow.getCell((short)3)!= null && !hssfRow.getCell((short)3).getStringCellValue().trim().equals("")){
                    json[2]=hssfRow.getCell((short)3).getStringCellValue();
                }else {
                    json[2]="";
                }
                if(hssfRow.getCell((short)1)!= null && !hssfRow.getCell((short)1).getStringCellValue().trim().equals("")){
                    json[3]=hssfRow.getCell((short)1).getStringCellValue().trim();
                }else {
                    json[3]="";
                }
                json[4]="0";
                json[5]="admin";
                json[6]="admin";
                reMap.put(hssfRow.getCell((short)0).getStringCellValue().trim()+hssfRow.getCell((short)1).getStringCellValue().trim(),json);
                json = new String[7];
            }
            if(hssfRow.getCell((short)2)!=null && !hssfRow.getCell((short)2).getStringCellValue().trim().equals("")){
                String[] str = reMap.get(hssfRow.getCell((short)0).getStringCellValue().trim()+hssfRow.getCell((short)1).getStringCellValue().trim());
                unit[0] = str[0];
                MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                String sql = "select uuid from zs_pd_units where UNITS_SYMBOL = '"+hssfRow.getCell((short)2).getStringCellValue().trim()+"'";
                String id = mysqlUtil2.getSqlRest(sql);
                if(id != null){
                    unit[1] = id;
                    if(hssfRow.getCell((short)4)!=null && hssfRow.getCell((short)4).getNumericCellValue() == 1.0){
                        unit[2] = "1";
                    }else {
                        unit[2] = "0";
                    }

                    Object.put(rowNum+"",unit);
                }
                unit = new String[3];
            }
        }
        list.put("Object",Object);
        list.put("reMap",reMap);
        return list;
    }


    /**
     * 添加剂列表
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getAdditive(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();

        String[] json = new String[9];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null || hssfRow.getCell((short)3)==null || hssfRow.getCell((short)3).getStringCellValue().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)3).getStringCellValue())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;

                String id = null;
                if(hssfRow.getCell((short)2)!=null && !hssfRow.getCell((short)2).getStringCellValue().trim().equals("")){
                    String cellname = hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim()+">>"+hssfRow.getCell((short)2).getStringCellValue().trim();
                    MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                    String sql = "select uuid from zs_pd_industry_class where INDUSTRY_NAME_CONNECT = '"+cellname+"'";
                    id = mysqlUtil2.getSqlRest(sql);

                }else if (id==null && hssfRow.getCell((short)1)!=null && !hssfRow.getCell((short)1).getStringCellValue().trim().equals("")){
                    String cellname = hssfRow.getCell((short)0).getStringCellValue().trim()+">>"+hssfRow.getCell((short)1).getStringCellValue().trim();
                    MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                    String sql = "select uuid from zs_pd_industry_class where INDUSTRY_NAME_CONNECT = '"+cellname+"'";
                    id = mysqlUtil2.getSqlRest(sql);
                }else if(id==null &&hssfRow.getCell((short)0)!=null && !hssfRow.getCell((short)0).getStringCellValue().trim().equals("")){
                    String cellname = hssfRow.getCell((short)0).getStringCellValue().trim();
                    MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
                    String sql = "select uuid from zs_pd_industry_class where INDUSTRY_NAME_CONNECT = '"+cellname+"'";
                    id = mysqlUtil2.getSqlRest(sql);
                }


                if(id!=null){
                    json[1]=id;
                }else{
                    json[1]="";
                }
                json[2]=hssfRow.getCell((short)3).getStringCellValue();
                json[3]="";
                json[4]="";
                if(hssfRow.getCell((short)4)!=null && !hssfRow.getCell((short)4).getStringCellValue().equals("")){
                    json[5]=hssfRow.getCell((short)4).getStringCellValue();
                }else {
                    json[5]="";
                }
                json[6]="0";
                json[7]="admin";
                json[8]="admin";
                reMap.put(hssfRow.getCell((short)3).getStringCellValue(),json);
                json = new String[9];
                Object.put(hssfRow.getCell((short)3).getStringCellValue(),json);
            }
        }
        return Object;
    }


    /**
     * 填充（增强）料列表
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getFiller(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();
        String[] json = new String[6];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null || hssfRow.getCell((short)4)==null || hssfRow.getCell((short)4).getStringCellValue().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)4).getStringCellValue())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)4).getStringCellValue();
                json[2]="";
                json[3]="0";
                json[4]="admin";
                json[5]="admin";
                reMap.put(hssfRow.getCell((short)4).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)4).getStringCellValue(),json);
                json = new String[6];
            }
        }
        return Object;
    }

    /**
     * 测试方法管理
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getTestMethod(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();
        String[] json = new String[8];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)0))){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)0).getStringCellValue();
                json[2]="";
                json[3]="";
                json[4]="";
                json[5]="0";
                json[6]="admin";
                json[7]="admin";
                reMap.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                json = new String[8];
            }
        }
        return Object;
    }


    /**
     * 计量单位维护
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getUnits(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();
        String[] json = new String[6];

        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)0)) && null != hssfRow.getCell((short)0)){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                //单位格式？？？
                json[1]=hssfRow.getCell((short)0).getStringCellValue();
                json[2]=hssfRow.getCell((short)1) == null?"":hssfRow.getCell((short)1).getStringCellValue();
                json[3]="0";
                json[4]="admin";
                json[5]="admin";
                reMap.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                Object.put(hssfRow.getCell((short)0).getStringCellValue(),json);
                json = new String[6];
            }
        }
        return Object;
    }


    /**
     * 外观维护
     * @param fname
     * @return
     * @throws IOException
     */
    public Map<String, String[]> getAppearance(String fname) throws IOException{
        InputStream is = new FileInputStream(fname);
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
        Map<String, String[]> Object = new HashMap<String, String[]>();
        Map<String, String[]> reMap = new HashMap<String, String[]>();
        String[] json = new String[6];


        for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null || hssfRow.getCell((short)1)==null || hssfRow.getCell((short)1).getStringCellValue().equals("")) {
                continue;
            }
            if(!reMap.containsKey(hssfRow.getCell((short)1).getStringCellValue())){
                String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
                json[0]=uuid;
                json[1]=hssfRow.getCell((short)1).getStringCellValue();
                json[2]="";
                json[3]="0";
                json[4]="admin";
                json[5]="admin";
                reMap.put(hssfRow.getCell((short)1).getStringCellValue(),json);
                json = new String[6];
                Object.put(hssfRow.getCell((short)1).getStringCellValue(),json);
            }
        }
        return Object;
    }

}
