package com.zsu.uup.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping(value = "FileDownLoad")
@Controller
public class FileDownLoad {
	@RequestMapping(value = "/downloading", method = RequestMethod.POST)
	@ResponseBody
	public void DownLoad(HttpServletResponse response,
			HttpServletRequest request) {
		try {
			String fileName = request.getParameter("filename");
			fileName = new String(fileName.getBytes("iso-8859-1"), "utf-8");
			String filesavepath = "D:\\Picture";
			String path = filesavepath;
			File file = new File(path + "\\" + fileName);
			if (!file.exists()) {
				request.setAttribute("Warning", "您所下载的资源已删除！");
				request.getRequestDispatcher("/4.jpg");
			}
			String realname = fileName.substring(fileName.indexOf("_") + 1);
			response.setHeader("content-disposition", "attachment;filename="
					+ URLEncoder.encode(realname, "UTF-8"));
			FileInputStream in = new FileInputStream(path + "\\" + fileName);
			OutputStream out = response.getOutputStream();
			// 创建缓冲区
			byte buffer[] = new byte[1024];
			int len = 0;
			// 循环将输入流中的内容读取到缓冲区当中
			while ((len = in.read(buffer)) > 0) {
				// 输出缓冲区的内容到浏览器，实现文件下载
				out.write(buffer, 0, len);
			}
			// 关闭文件输入流
			in.close();
			// 关闭输出流
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping(value = "/download")
	public ModelAndView upload() {
		ModelAndView model = new ModelAndView("file/FileUpload");
		return model;
	}
	
}
