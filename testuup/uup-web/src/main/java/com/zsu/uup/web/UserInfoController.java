package com.zsu.uup.web;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.Page;
import com.zsu.uup.UserInfoService;
import com.zsu.uup.entity.UserInfo;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 
 * <p>
 * Title: UserInfoController
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Kouxu (Create on:2018年6月19日)
 * @version 1.0
 * @fileName UserInfoController.java
 */
@RequestMapping(value = "UserInfo")
@Controller
public class UserInfoController {

	@Reference
	private UserInfoService userinfoService;

	
	String ph = "^((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0,5-9]))\\d{8}$";
	String em = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
	/**
	 * 
	 * <p>
	 * Title: getUserInfo(个人审核资料 待审核)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/getUserInfoList", method = RequestMethod.GET)
	public ModelAndView getUserInfo() {
		ModelAndView model = new ModelAndView("file/NewFile");
		Map<String, String> map = new HashMap<String, String>();
		PageInfo<UserInfo> userList = userinfoService.getUserInfoList(map);
		model.addObject("userList",userList.getRows());
		return model;
	}

	/**
	 * 
	 * <p>
	 * Title: getUserInfoById(根据ID获取个人审核资料详情)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getUserInfoById", method = RequestMethod.GET)
	@ResponseBody
	public List<UserInfo> getUserInfoById(Integer id) {
		List<UserInfo> ulist = userinfoService.getUserInfoListById(id);
		if (ulist.size() == 0) {
			System.out
					.println("success=======================================");
		}
		return ulist;
	}

	/**
	 * 
	 * <p>
	 * Title: getUserInfoEnd(个人资料审核 已审核)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/getUserInfoEnd", method = RequestMethod.GET)
	public ModelAndView getUserInfoEnd() {
		ModelAndView model = new ModelAndView("file/NewFile");
		Map<String, String> map = new HashMap<String, String>();
		PageInfo<UserInfo> ulistend = userinfoService.getUserInfoListEnd(map);
		model.addObject("ulistend",ulistend.getRows());
		return model;
	}
	
	/**
	 * 
	 * <p>Title: getUserInfoAlltthimg(获取平台上的账号列表)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value ="/getUserInfoAllthing",method = RequestMethod.GET)
	public ModelAndView getUserInfoAlltthimg(){
		ModelAndView model = new ModelAndView("file/NewFile");
		Map<String,String> map = new HashMap<String, String>();
		PageInfo<UserInfo> ulistall = userinfoService.getUserInfoAllthing(map);
		model.addObject("ulistall",ulistall.getRows());
		return model;
	}
	
	/**
	 * 注册页面
	 * <p>Title: registerPage</p>  
	 * @author zhang.xinxin (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value="registerPage")
	public ModelAndView registerPage(){
		ModelAndView mav  = new ModelAndView("login/register");
		return mav;
	}
	
	
	/**
	 * <p>Title: userInsert</p>  
	 * 注册
	 * @author zhang.xinxin (Create on:2018年6月19日)
	 * @version 1.0
	 * @param request获取前端传值
	 * @param model
	 * @return
	 */
	@RequestMapping(value="userInsert")
	@ResponseBody
	public ModelAndView userInsert(HttpServletRequest request,Model model){
		ModelAndView mav = new ModelAndView("logon/register");
		String userName = request.getParameter("userName");
		String passWd = request.getParameter("passWd");
		UserInfo userInfoDto = new UserInfo();
		
		userInfoDto.setUserId("00001");
		userInfoDto.setUserPassword(passWd);
		userInfoDto.setCreateDate(new Date());	
		if(userName != null){
			
			if(Pattern.matches(ph, userName)){//判断为手机号
			
				userInfoDto.setUserPhone(userName);
		
			}else if(Pattern.matches(em, userName)){//判断为邮箱
			
				userInfoDto.setUserEmail(userName);

			}else{
			
				userInfoDto.setUserName(userName);

			}
		
			Integer num =userinfoService.insertUserRegister(userInfoDto);
			if(num!=null&&num>0){
				mav.addObject("result", "添加成功");
			}else{
				mav.addObject("result", "添加失败");
			}
			return mav;
		}else{
			return new ModelAndView("login/register");
		}
		
		
		
	}
	
	/**
	 * <p>Title: loginPage</p>  
	 * 登录页面
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value="loginPage")
	public ModelAndView loginPage(){
		ModelAndView mav = new ModelAndView("login/login");
		mav.addObject("result", "");
		mav.addObject("cookie", "");
		return mav;
	}
	
	/**
	 * 
	 * <p>Title: login</p>  
	 * 用户登录
	 * @author zhang.xinxin (Create on:2018年6月19日)
	 * @version 1.0
	 * @param request获取前端传值
	 * @param response返回Cookie
	 * @return
	 */
	@RequestMapping(value="login")
	public ModelAndView login(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav = new ModelAndView("login/login");
		Map<String,Object> map = new HashMap<String, Object>();
		String userName = request.getParameter("userName");
		String passWd = request.getParameter("passWd");
		map.put("userName", userName);
		map.put("passWd", passWd);
		if(userName != null){
			map.put("userName", userName);
			if(Pattern.matches(ph, userName)){//判断为手机号
			
				map.put("code",1);
			}else if(Pattern.matches(em, userName)){//判断为邮箱
			
				map.put("code",2);
			}
			if(userinfoService.queryLoginName(map)==null){
				mav.addObject("result", "用户名不存在");
				mav.addObject("cookie", "cookie为空");
			}else{
				if(userinfoService.queryLoginInfo(map)!=null){
					Cookie nameCookie;
					Cookie pasCookie;
					try {
						nameCookie = new Cookie("user", URLEncoder.encode(userName,"utf-8"));
						pasCookie = new Cookie("psw",passWd);
						nameCookie.setMaxAge(5*60);
						pasCookie.setMaxAge(5*60);
						response.addCookie(nameCookie);
						response.addCookie(pasCookie);
						mav.addObject("cookie", "添加cookie成功");
					} catch (UnsupportedEncodingException e) {
						mav.addObject("cookie", "添加cookie失败");
						e.printStackTrace();
					}
					
					mav.addObject("result", "登录成功");
				}else{
					mav.addObject("cookie", "cookie为空");
					mav.addObject("result", "登录密码错误");
				}
			}
		}
			return mav;
		}
	
	
	/**
	 * 判断找回密码中输入的账号合法性
	 * <p>Title: getUserInfoByUserName</p>  
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @param request 获取前端传值
	 * @return
	 */
	@RequestMapping(value="getUserInfoByUserName")
	public ModelAndView getUserInfoByUserName(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("login/reset");
		Map<String,Object> map = new HashMap<String, Object>();
		String userName = request.getParameter("inputName");
		if(userName == null){
			mav.addObject("userInfo", "帐号不能为空");
		}else{
			map.put("userName", userName);
			if(Pattern.matches(ph, userName)){//判断为手机号
				request.getSession().setAttribute("code", 1);
				map.put("code",1);
			}else if(Pattern.matches(em, userName)){//判断为邮箱
				request.getSession().setAttribute("code", 2);
				map.put("code",2);
			}
			request.getSession().setAttribute("userName", userName);
			UserInfo userInfoDto =  userinfoService.queryLoginName(map);
			if(userInfoDto!=null){
//				mav.addObject("userInfo", userInfoDto);
				mav = new ModelAndView("login/updatePassWd");
			}else{
				mav.addObject("userInfo", "请输入正确的账号");
			}
		}
		return mav;
		
	}
	
	
	/**
	 * <p>Title: resetPassWd</p>  
	 * 保存更改的密码
	 * @author zhang.xinxin (Create on:2018年6月20日)
	 * @version 1.0
	 * @param request获取前端传值
	 * @return
	 */
	@RequestMapping(value="resetPassWd")
	public ModelAndView  resetPassWd(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("login/login");
		Map<String,Object> map = new HashMap<String, Object>();
		String passWd = request.getParameter("passWd");
		Integer code = (Integer)request.getSession().getAttribute("code");
		//得到session的值判断是否为空
		Object obj =  request.getSession().getAttribute("userName");
		if(obj!=null&&!"".equals(obj)){
			map.put("code", code);
			map.put("passWd", passWd);
			map.put("userName", obj.toString());
			userinfoService.updateUserPassWd(map);
		}else{
			return new ModelAndView("login/register");
		}
		return mav;
	}
	
	
	
}
