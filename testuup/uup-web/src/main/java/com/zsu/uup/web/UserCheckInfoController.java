package com.zsu.uup.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.UserCheckInfoService;
import com.zsu.uup.entity.UserCheckInfo;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 
 * <p>
 * Title: UserCheckInfoController
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Kouxu (Create on:2018年6月20日)
 * @version 1.0
 * @fileName UserCheckInfoController.java
 */
@RequestMapping(value = "UserCheckInfo")
@Controller
public class UserCheckInfoController {

	@Reference
	private UserCheckInfoService userCheckInfoService;

	/**
	 * 
	 * <p>
	 * Title: getUserCheckList(个人资料审核历史纪录)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/getUserCheckList", method = RequestMethod.GET)
	public ModelAndView getUserCheckList() {
		ModelAndView model = new ModelAndView("file/NewFile");
		Map<String, String> map = new HashMap<String, String>();
		PageInfo<UserCheckInfo> uchecklist = userCheckInfoService
				.getUserCheckList(map);
		model.addObject("uchecklist", uchecklist.getRows());
		return model;
	}
}
