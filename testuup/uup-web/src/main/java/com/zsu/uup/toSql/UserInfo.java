package com.zsu.uup.toSql;

import java.sql.ResultSet;

public class UserInfo {

    public static void main(String[] args) {
     //用户表
//         Thread thread=new InsertUserInfo();
//         thread.start();
        //用户审核表

        Thread thread=new InsertCheckUserInfo();
        thread.start();
      //用户角色公司中间表
//        Thread thread=new InsertUserAndCompanyToRoleInfo();
//        thread.start();
         /**
          * 1.查询所有的用户信息
          * 2.角色表
          *     查询所有公司信息
          *     for
          *     插入默认角色
          * */
    }
    public void userInfo(){
        DBUtil dbUtil=new DBUtil();
        MysqlUtil mysqlUtil=new MysqlUtil();
        ResultSet resultSet=dbUtil.getUserInfo();
        mysqlUtil.insertUser(resultSet,"用户信息表");
    }

    /**
     * 未审核用户
     */
    public void userCheckInfo(){
        MysqlUtil mysqlUtil=new MysqlUtil();
        ResultSet resultSet=mysqlUtil.getCheckUserInfo();
        mysqlUtil.insertCheckUser(resultSet,"用户审核信息表");
    }
    /**
     * 用户角色公司中间表
     */
    public void UserAndCompanyToRoleInfo(){
        MysqlUtil mysqlUtil=new MysqlUtil();
        ResultSet resultSet=mysqlUtil.getUserToCompany();
        mysqlUtil.insertUserAndCompany(resultSet,"用户角色公司中间表");
    }

}
class InsertUserInfo extends Thread{
  UserInfo userInfo=new UserInfo();
    @Override
    public void run() {
        userInfo.userInfo();
    }
}

class InsertCheckUserInfo extends Thread{
    UserInfo userInfo=new UserInfo();
    @Override
    public void run() {
        userInfo.userCheckInfo();
    }
}

class InsertUserAndCompanyToRoleInfo extends Thread{
    UserInfo userInfo=new UserInfo();
    @Override
    public void run() {
        userInfo.UserAndCompanyToRoleInfo();
    }
}
