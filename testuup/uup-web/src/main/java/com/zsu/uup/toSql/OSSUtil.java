package com.zsu.uup.toSql;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OSSUtil {

    private static final String ossdomain       = "http://21cp-test-bucket.oss-cn-shanghai.aliyuncs.com";

    static HashMap<String, HashMap<String, HashMap<String, Object>>> conf;
    private static Pattern REX_PATTERN = Pattern.compile("^(\\d+)(mb|kb)?$");
    static {

        InputStream in = null;
        try {
            // 使用ClassLoader加载properties配置文件生成对应的输入流
            in = OSSUtil.class.getClassLoader().getResourceAsStream("uploadconfig.yml");
            if (in != null) {
                initYml(in);
            } else {
                in = OSSUtil.class.getClassLoader().getResourceAsStream("uploadconfig.properties");
                initProp(in);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                IOUtils.closeQuietly(in);
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void initProp(InputStream in) throws IOException {
        Properties properties = new Properties();
        // 使用properties对象加载输入流
        properties.load(in);
        conf = new HashMap<String, HashMap<String, HashMap<String, Object>>>();
        Set<Map.Entry<Object, Object>> entrySet = properties.entrySet();
        // 返回的属性键值对实体
        for (Map.Entry<Object, Object> entry : entrySet) {
            String[] key = entry.getKey().toString().split("\\.");
            if (!conf.containsKey(key[0])) {
                conf.put(key[0], new HashMap<String, HashMap<String, Object>>());
            }
            if (!conf.get(key[0]).containsKey(key[1])) {
                conf.get(key[0]).put(key[1], new HashMap<String, Object>());
            }
            if (!conf.get(key[0]).get(key[1]).containsKey(key[2])) {
                if (key[2].equals("maxsize")) {
                    Matcher m = REX_PATTERN.matcher(entry.getValue().toString());
                    if (m.find()) {
                        long val = Long.parseLong(m.group(1));
                        switch (m.group(2)) {
                            case "mb":
                                conf.get(key[0]).get(key[1]).put(key[2], (val * 1024 * 1024));
                                break;
                            case "kb":
                                conf.get(key[0]).get(key[1]).put(key[2], (val * 1024));
                                break;
                            default:
                                conf.get(key[0]).get(key[1]).put(key[2], (val));
                                break;
                        }
                    } else {
                        conf.get(key[0]).get(key[1]).put(key[2], entry.getValue().toString());
                    }
                } else if (key[2].equals("ext")) {
                    conf.get(key[0]).get(key[1]).put(key[2], entry.getValue().toString());
                    conf.get(key[0]).get(key[1]).put("extRegex", "^(\\.)?(" + entry.getValue().toString().replace(",", "|") + ")$");
                } else if (key[2].equals("imgcompress")) {
                    String[] arr = entry.getValue().toString().split(",");
                    conf.get(key[0]).get(key[1]).put(key[2], new int[] { Integer.parseInt(arr[0]), Integer.parseInt(arr[0]) });
                } else {
                    conf.get(key[0]).get(key[1]).put(key[2], entry.getValue().toString());
                }
            }
        }
    }

    private static void initYml(InputStream in) throws IOException {
        conf = new HashMap<String, HashMap<String, HashMap<String, Object>>>();
        StringBuffer out = new StringBuffer();
        byte[] b = new byte[4096];
        for (int n; (n = in.read(b)) != -1;) {
            out.append(new String(b, 0, n));
        }
        String module = null;
        String position = null;
        String[] arr = out.toString().split("\n");
        for (int i = 0; i < arr.length; i++) {
            String item = arr[i];
            if (item.startsWith("#") || item.trim().isEmpty()) {
                continue;
            } else if (item.startsWith("\t\t")) {
                String trimStr = item.trim();
                String[] parr = trimStr.split("=");
                String key = parr[0];
                String val = parr[1];
                if (key.equals("maxsize")) {
                    Matcher m = REX_PATTERN.matcher(val);
                    if (m.find()) {
                        long nval = Long.parseLong(m.group(1));
                        switch (m.group(2)) {
                            case "mb":
                                conf.get(module).get(position).put(key, (nval * 1024 * 1024));
                                break;
                            case "kb":
                                conf.get(module).get(position).put(key, (nval * 1024));
                                break;
                            default:
                                conf.get(module).get(position).put(key, (nval));
                                break;
                        }
                    } else {
                        conf.get(module).get(position).put(key, val.toString());
                    }
                } else if (key.equals("ext")) {
                    try{
                        conf.get(module).get(position).put(key, val.toString());
                        conf.get(module).get(position).put("extRegex", "^(\\.)?(" + val.replace(",", "|") + ")$");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                } else if (key.equals("imgcompress")) {
                    String[] compressArr = val.split(",");
                    conf.get(module).get(position).put(key, new int[] { Integer.parseInt(compressArr[0]), Integer.parseInt(compressArr[0]) });
                } else {
                    System.out.println(module+"-------"+position+"     key======"+key+"  val=========="+val);
                    conf.get(module).get(position).put(key, val);
                }
            } else if (item.startsWith("\t")) {
                position = item.trim();
                if (!conf.get(module).containsKey(position)) {
                    conf.get(module).put(position, new HashMap<String, Object>());
                }
            } else {
                module = item.trim();
                if (!conf.containsKey(module)) {
                    conf.put(module, new HashMap<String, HashMap<String, Object>>());
                }
                position = null;
            }
        }
    }

    /*
     * 校验是位置是否存在 moduleName 模块名称 positionName 位置名称
     */
    public static boolean positionExists(String moduleName, String positionName) {
        return conf.containsKey(moduleName) && conf.get(moduleName).containsKey(positionName);
    }
    /*
     * 获取文件位置允许扩展名的正则表达式 moduleName 模块名称 positionName 位置名称
     */
    public static String positionFileExtRegex(String moduleName, String positionName) {
        return (String) conf.get(moduleName).get(positionName).get("extRegex");
    }

    /*
     * 获取文件上传后返回的url规则
     */
    public static String positionUrlFormat(String moduleName, String positionName) {
        return (String) conf.get(moduleName).get(positionName).get("urlformat");
    }

    /*
     * 获取文件位置允许大小，字节 moduleName 模块名称 positionName 位置名称
     */
    public static long positionFileMaxSize(String moduleName, String positionName) {
        return (long) conf.get(moduleName).get(positionName).get("maxsize");
    }

    /*
     * 判断扩展名是否为图片
     */
    public static boolean extIsImage(String ext) {
        return Pattern.matches("^(\\.)?(jpg|png|gif|jpeg)$", ext);
    }

    /*
     * 获取图片压缩尺寸信息
     */
    public static int[] imageCompressInfo(String moduleName, String positionName) {
        try {
            if (positionExists(moduleName, positionName)) {
                HashMap<String, Object> position = conf.get(moduleName).get(positionName);
                if (position.containsKey("imgcompress")) {
                    return (int[]) position.get("imgcompress");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Map<Integer, String> moreThumb(String moduleName, String positionName) {
        try {
            if (positionExists(moduleName, positionName)) {
                HashMap<String, Object> position = conf.get(moduleName).get(positionName);
                if (position.containsKey("morethumb")) {
                    String value = position.get("morethumb").toString();
                    String[] arr=  value.split(",");
                    Map<Integer,String> map = new HashMap<>();
                    for(String str : arr){
                        String[] arr1 = str.split("@@");
                        if(arr1.length ==1){
                            map.put(Integer.valueOf(arr1[0]),"");
                        }else{
                            map.put(Integer.valueOf(arr1[0]),"_"+arr1[1]);
                        }
                    }
                    return map;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    //上传图片
    public static Map<String,Object> pcUpload( String imgUrl, String module, String position) {
        // 发送get请求
        HttpGet request = new HttpGet(imgUrl);
        // 设置请求和传输超时时间
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(50000).setConnectTimeout(50000).build();
        //设置请求头
        request.setHeader( "User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1" );
        request.setConfig(requestConfig);

        Map<String,Object> item = new HashMap<>();
        InputStream  in = null;
        try {
            URL url = new URL(imgUrl);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            String originalFilename = imgUrl.substring(imgUrl.lastIndexOf("/")+1);
            String fileUrl = positionUrlFormat(module, position).replace("{filename}", originalFilename);
            in = conn.getInputStream();

            if (ZJCPOClient.connect(client -> {
                try {
                    client.getClient().putObject(client.getBucket(), fileUrl, (InputStream) client.getArg(0));
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }, in)) {
                HashMap<String, String> result = new HashMap<String, String>();
                result.put("module", module);
                result.put("position", position);
                result.put("originalFilename", originalFilename);
                result.put("url", fileUrl);
                result.put("fileDomain", ossdomain);
                item.put("200", "上传成功");
                item.put("data",result);
                return item;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getCause() + " - "+ e.getMessage());
            item.put("500", "服务器繁忙");
            return item;
        } finally {
            try {
                if (in != null) {
                    IOUtils.closeQuietly(in);
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        item.put("400", "上传失败");
        return item;
    }



    //上传图片
    public static Map<String,Object> pcUpload1(File file,String module, String position) {
        Map<String,Object> item = new HashMap<>();
        InputStream in = null;
        try {
                in = new FileInputStream(file);
                String imgName = file.getName();
                String nameSuffix = imgName.substring(imgName.lastIndexOf("."));
                String newFileName = UUID.randomUUID().toString().replace("-","").toLowerCase() + nameSuffix;
                String fileUrl = positionUrlFormat(module, position).replace("{filename}", newFileName);

                if (ZJCPOClient.connect(client -> {
                    try {
                        client.getClient().putObject(client.getBucket(), fileUrl, (InputStream) client.getArg(0));
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }, in)) {
                    HashMap<String, String> result = new HashMap<String, String>();
                    result.put("module", module);
                    result.put("position", position);
                    result.put("originalFilename", imgName);
                    result.put("url", fileUrl);
                    result.put("fileDomain", ossdomain);
                    item.put("200", "上传成功");
                    item.put("data",result);
                    return item;
                }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getCause() + " - "+ e.getMessage());
            item.put("code", "500");
            item.put("msg", "服务器繁忙");
            return item;
        } finally {
            try {
                if (in != null) {
                    IOUtils.closeQuietly(in);
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        item.put("code", "400");
        item.put("msg", "上传失败");
        return item;
    }

    /**
     * @api {post} upload/pcupload pc端文件上传
     * @apiGroup upload
     * @apiName pcupload
     * @author lixin.sheng (Create on:2018年7月19日 14:32:32)
     * @apiVersion 0.0.1
     * @apiDescription
     * @apiParam {MultipartFile} file 文件域信息
     * @apiParam {String} module 模块名称
     * @apiParam {String} position 文件位置信息
     * @apiParamExample
     * @apiSuccess {String} msg 撤回成功&&自定义
     * @apiSuccess {int} code 200 上传成功，500系统繁忙,400 msg会返回相关错误信息
     * @apiSuccess {String} originalFilename 上传文件的原文件名称
     * @apiSuccess {String} url oss上文件的相对地址
     * @apiSuccess {String} position 文件所在的位置信息
     * @apiSuccess {String} module 模块名称
     * @apiSuccessExample {type} 返回样例:
     *                    {"code":"200","msg":"上传成功",data:{"originalFilename":"1.png","module":"identity_card","position":"reverse","url":"img/1.png"}}
     */
    @RequestMapping(value = "/pcs_Upload", method = RequestMethod.POST)
    @ResponseBody
    public static Map<String,Object> pcs_Upload(String imgUrl, String module, String position) {
        // 发送get请求
        HttpGet request = new HttpGet(imgUrl);
        // 设置请求和传输超时时间
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(50000).setConnectTimeout(50000).build();
        //设置请求头
        request.setHeader( "User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1" );
        request.setConfig(requestConfig);

        Map<String,Object> item = new HashMap<>();
        //InputStream  in = null;
        ByteArrayOutputStream baos = null;
        Map<String, InputStream> moreThumbDict = new HashMap<String, InputStream>();
        try {
            URL url = new URL(imgUrl);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            String originalFilename = imgUrl.substring(imgUrl.lastIndexOf("/")+1);
            String fileUrl = positionUrlFormat(module, position).replace("{filename}", originalFilename);
            String nameSuffix = imgUrl.substring(imgUrl.lastIndexOf(".")+1);//图片后缀
            //in = conn.getInputStream();
            BufferedImage image = ImageIO.read(url);
            baos = new ByteArrayOutputStream();
            ImageIO.write( image, nameSuffix, baos);
            baos.flush();
            int[] compress = null;
            Map<Integer, String> morethumb = null;
            if (extIsImage(nameSuffix) && ((morethumb = moreThumb(module, position)) != null || (compress = imageCompressInfo(module, position)) != null)) {
                if (morethumb != null) {
                    int extIndex = fileUrl.lastIndexOf(".");
                    String pex = fileUrl;
                    String sext = "";
                    if (extIndex > 0) {
                        pex = fileUrl.substring(0, extIndex);
                        sext = fileUrl.substring(extIndex);
                    }
                    for (Map.Entry<Integer, String> entry : morethumb.entrySet()) {
                        String newFileName2 = pex + entry.getValue() + sext;
                        moreThumbDict.put(newFileName2, new ByteArrayInputStream(zipImageFile(baos.toByteArray(), entry.getKey(), entry.getKey(), nameSuffix)));
                    }
                } else if (compress != null) {
                    moreThumbDict.put(fileUrl, new ByteArrayInputStream(zipImageFile(baos.toByteArray(), compress[0], compress[1], nameSuffix)));
                }
            } else {
                moreThumbDict.put(fileUrl, new ByteArrayInputStream(baos.toByteArray()));
            }
            if (ZJCPOClient.connect(client -> {
                try {
                    @SuppressWarnings("unchecked")
                    Map<String, InputStream> dataArg = (Map<String, InputStream>) client.getArg(0);
                    for (Map.Entry<String, InputStream> entry : dataArg.entrySet()) {
                        client.getClient().putObject(client.getBucket(), entry.getKey(), entry.getValue());
                    }
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }, moreThumbDict)) {
                HashMap<String, String> result = new HashMap<String, String>();
                result.put("module", module);
                result.put("position", position);
                result.put("originalFilename", originalFilename);
                result.put("url", fileUrl);
                result.put("fileDomain", ossdomain);
                item.put("200", "上传成功");
                item.put("data",result);
                return item;
            }


            /*if (ZJCPOClient.connect(client -> {
                try {
                    client.getClient().putObject(client.getBucket(), fileUrl, (InputStream) client.getArg(0));
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }, in)) {
                HashMap<String, String> result = new HashMap<String, String>();
                result.put("module", module);
                result.put("position", position);
                result.put("originalFilename", originalFilename);
                result.put("url", fileUrl);
                result.put("fileDomain", ossdomain);
                item.put("200", "上传成功");
                item.put("data",result);
                return item;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getCause() + " - "+ e.getMessage());
            item.put("500", "服务器繁忙");
            return item;
        } finally {
            try {
                /*if (in != null) {
                    IOUtils.closeQuietly(in);
                    in.close();
                }*/
                if(baos != null){
                    baos.close();
                }
                for (Map.Entry<String, InputStream> entry : moreThumbDict.entrySet()) {
                    try {
                        entry.getValue().close();
                    } catch (Exception e2) {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        item.put("400", "上传失败");
        return item;
    }

    public static File getFileName(String name){
        File file = new File("C:\\Users\\oyt\\Desktop\\数据\\厂家logo");
        File[] tempList = file.listFiles();
        for (int i = 0; i < tempList.length; i++) {
            if (tempList[i].isFile()) {
                String []str = tempList[i].getName().split("\\.");
                if(name.equals(str[0])){
                    return tempList[i];
                }
            }
        }
        return null;
    }

    // 对图片进行等比例压缩
    private static byte[] zipImageFile(byte[] src, int width, int height, String subfix) throws IOException {
        subfix = subfix.replace(".", "");
        ByteArrayOutputStream out = null;
        ByteArrayInputStream in = null;
        Image srcFile = null;
        BufferedImage buffImg = null;
        try {
            in = new ByteArrayInputStream(src);
            srcFile = ImageIO.read(in);
            int w = srcFile.getWidth(null);
            int h = srcFile.getHeight(null);
            if (width == 0 && height == 0) {
                return src;
            }
            if (w < width && h < height) {
                return src;
            }
            double bili;
            if (width > 0 && w > width) {
                bili = width / (double) w;
                height = (int) (h * bili);
            }
            if (height > 0 && h > height) {
                bili = height / (double) h;
                width = (int) (w * bili);
            }
            if (subfix.equals("png")) {
                buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            } else {
                buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            }
            Graphics2D graphics = buffImg.createGraphics();
            graphics.fillRect(0, 0, width, height);
            graphics.drawImage(srcFile.getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);
            out = new ByteArrayOutputStream();
            ImageIO.write(buffImg, subfix, out);
            byte[] ret = out.toByteArray();
            return ret;
        } catch (Exception e) {
            throw e;
        } finally {
            if (in != null) {
                IOUtils.closeQuietly(in);
                try {
                    in.close();
                } catch (Exception e2) {

                }
            }
            if (out != null) {
                IOUtils.closeQuietly(out);
                try {
                    out.close();
                } catch (Exception e2) {

                }
            }
            if (srcFile != null) {
                srcFile.flush();
            }
            if (buffImg != null) {
                buffImg.flush();
            }
        }
    }



    public static void main(String[] args) {
        List<String> strList = new ArrayList<>();
        strList.add("http://img5.duitang.com/uploads/item/201407/05/20140705225720_KQkMH.jpeg");
        strList.add("http://www.wallcoo.com/cartoon/abstract_colors_1920x1200_0917/wallpapers/1920x1200/abstract_lights_and_colors_seupt_red.jpg");
        strList.add("http://pic1.win4000.com/wallpaper/b/55597435bb036.jpg");
        strList.add("http://attachments.gfan.com/forum/attachments2/day_100527/100527230931d406a3c8037d60.jpg");
        strList.add("http://img4.duitang.com/uploads/item/201407/09/20140709203033_JxsCu.jpeg");
        strList.add("http://s9.knowsky.com/bizhi/l/20090808/200910296%20%2841%29.jpg");
        for(String str:strList){
            System.out.println(pcs_Upload(str,"urlFile","test_upload"));
        }
    }

}
