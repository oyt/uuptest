package com.zsu.uup.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.DemoService;
import com.zsu.uup.entity.DemoInfo;

@RequestMapping(value = "DemoInfo")
@Controller
public class DemoController {

	@Reference
	public DemoService demoService;

	@RequestMapping(value = "/getDemo", method = RequestMethod.GET)
	@ResponseBody
	public List<DemoInfo> getDemoL() {
		List<DemoInfo> delist = demoService.getDemoList();

		return delist;
	}

	@RequestMapping(value = "/insertD", method = RequestMethod.GET)
	@ResponseBody
	public String insertDemo(DemoInfo demoInfo) {
		demoService.insertDemo(demoInfo);
		return "success";
	}

	@RequestMapping(value = "/updateD", method = RequestMethod.GET)
	@ResponseBody
	public String updateDemo(DemoInfo demoInfo) {
		demoService.updateDemo(demoInfo);
		return "success";
	}

	@RequestMapping(value = "/deleteD", method = RequestMethod.GET)
	@ResponseBody
	public int deleteDemo() {
		demoService.deleteDemo(500);
		return 66666;
	}

	@RequestMapping(value = "/getDById", method = RequestMethod.GET)
	@ResponseBody
	public int getDemoById() {
		demoService.getDemoById(100);
		System.out.println(demoService.getDemoById(100) + "=============");
		return 55555;
	}

	@RequestMapping(value = "/getDByName", method = RequestMethod.GET)
	@ResponseBody
	public String getDemoByName() {
		demoService.getDemoByName("admin");
		return "aaaa";
	}

	@RequestMapping(value = "/deleteDList", method = RequestMethod.GET)
	@ResponseBody
	public int deleteDByList(List<DemoInfo> dlist) {
		dlist = new ArrayList<DemoInfo>();
		dlist.add(new DemoInfo(500, "王五"));
		dlist.add(new DemoInfo(600, "王六"));
		dlist.add(new DemoInfo(700, "王七"));
		dlist.add(new DemoInfo(800, "王八"));
		demoService.deleteDemoList(dlist);
		return 11111;
	}

	@RequestMapping(value = "/insertDList", method = RequestMethod.GET)
	@ResponseBody
	public int insertDByList() {
		List<DemoInfo> dlist = new ArrayList<DemoInfo>();
		DemoInfo demoInfo = new DemoInfo();
		demoInfo.setT1name("老五");
		dlist.add(demoInfo);
		demoService.insertDemoList(dlist);
		return 1111111111;
	}

	@RequestMapping(value = "/updateDList", method = RequestMethod.GET)
	@ResponseBody
	public int updateDByList() {
		List<DemoInfo> dlist = new ArrayList<DemoInfo>();
		dlist.add(new DemoInfo(801, "王五1"));
		demoService.updateDemoList(dlist);
		return 6666666;
	}

	public String selectDMap(Map<String, String> dmap) {
		return "success";
	}
}
