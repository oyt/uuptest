package com.zsu.uup.toSql;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserAndCompanyAndRoleUtil {

    public String getUserAndCompany(){
        //源数据库查询
        //String userAndComp="Select ui.UI_Id,ci.CI_Id from ZS_UserInfo ui INNER JOIN ZS_CorpInfo ci on ui.CI_Id = ci.CI_Id where ui.UI_State in ('0','1','2') and ci.CI_State in('2') ";
       //查询管理员
        String userAndComp="SELECT ui.USER_ID,ei.CORP_ID FROM zs_uup_user_info ui INNER JOIN zs_uup_enterprise_info ei on ui.CID_TEST=ei.CID_TEST where ui.CI_ISMAINTEST = 1";
       //非管理员
       // String userAndComp="SELECT ui.USER_ID,ei.CORP_ID FROM zs_uup_user_info ui INNER JOIN zs_uup_enterprise_info ei on ui.CID_TEST=ei.CID_TEST where ui.CI_ISMAINTEST = 0";
        return userAndComp;
    }

    //返回用户和公司集合数据
    public String[] userAndRoleMap(ResultSet result){
        String[] str = new String[2];
        try {
            String uiId=result.getString("USER_ID");
            str[0] =uiId+"";
            String ciId=result.getString("CORP_ID");
            str[1] =ciId+"";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String insertUserToComp(){
        //添加管理员
       String insertUserAndComp="insert into zs_uup_user_to_role(user_id,corp_id,role_id)values(?,?,'001')";
        //非管理员
        //String insertUserAndComp="insert into zs_uup_user_to_role(user_id,corp_id,role_id)values(?,?,'')";
        return insertUserAndComp;
    }

}
