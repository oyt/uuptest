package com.zsu.uup.web;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zsu.uup.entity.DemoInfo;

@RequestMapping(value = "Execl")
@Controller
public class Execl {

	@RequestMapping(value = "/getExeclList")
	@ResponseBody
	public String getExecl() {
		createExecl(getDemo());
		List<DemoInfo> dlist = resdExecl();
		for (DemoInfo demoInfo : dlist) {
			System.out.println("========================="+demoInfo.getT1name());
		}

		return "success";
	}

	public static List<DemoInfo> getDemo() {
		List<DemoInfo> dlist = new ArrayList<DemoInfo>();
		DemoInfo demoInfo1 = new DemoInfo(900, "11张三");
		DemoInfo demoInfo2 = new DemoInfo(900, "22李四");
		DemoInfo demoInfo3 = new DemoInfo(900, "33王五");
		dlist.add(demoInfo1);
		dlist.add(demoInfo2);
		dlist.add(demoInfo3);
		return dlist;
	}

	private static void createExecl(List<DemoInfo> dlist) {
		// 创建一个Excel文件
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 创建一个工作表
		HSSFSheet sheet = workbook.createSheet("DemoInfo");
		// 添加表头行
		HSSFRow hssfRow = sheet.createRow(0);
		// 设置单元格格式居中
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 添加表头内容
		HSSFCell headcell = hssfRow.createCell((short) 0);
		headcell.setCellValue("t1Id");
		headcell.setCellStyle(cellStyle);

		headcell = hssfRow.createCell((short) 1);
		headcell.setCellValue("t1Name");
		headcell.setCellStyle(cellStyle);

		for (int i = 0; i < dlist.size(); i++) {
			hssfRow = sheet.createRow(i + 1);
			HSSFCell cell = hssfRow.createCell((short) 0);
			cell.setCellValue(dlist.get(i).getT1id());
			cell.setCellStyle(cellStyle);

			cell = hssfRow.createCell((short) 1);
			cell.setEncoding(HSSFCell.ENCODING_UTF_16);
			// 设置cell编码解决中文高位字节截断
			cell.setCellValue(dlist.get(i).getT1name());
			cell.setCellStyle(cellStyle);
		}

		try {
			OutputStream outputStream = new FileOutputStream("D:/DemoInfo.xls");
			workbook.write(outputStream);
			outputStream.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static List<DemoInfo> resdExecl() {
		List<DemoInfo> dlist = new ArrayList<DemoInfo>();
		HSSFWorkbook workbook = null;
		try {
			InputStream inputStream = new FileInputStream("D:/DemoInfo.xls");
			workbook = new HSSFWorkbook(inputStream);
			inputStream.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int numSheet = 0; numSheet < workbook.getNumberOfSheets(); numSheet++) {
			HSSFSheet hssfSheet = workbook.getSheetAt(numSheet);
			if (hssfSheet == null) {
				continue;
			}
			for (int rowNum = 0; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
				HSSFRow hssfRow = hssfSheet.getRow(rowNum);
				if (hssfRow == null) {
					continue;
				}
				// 将单元格中的内容存入集合
				DemoInfo demoInfo = new DemoInfo();
				
				HSSFCell cell = hssfRow.getCell((short) 0);
				if (cell == null) {
					continue;
				}
				demoInfo.setT1id(cell.getCellType());

				cell = hssfRow.getCell((short) 1);
				if (cell == null) {
					continue;
				}
				demoInfo.setT1name(cell.getStringCellValue());
				dlist.add(demoInfo);
			}
		}
		return dlist;
	}

}
