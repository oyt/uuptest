package com.zsu.uup.toSql;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleInfoUtil {


    /**
     * 查询默认角色
     */
 public  String getDefaultRoleInfo(){
     String roleInfo="select role_name,function_info,is_work,state from zs_uup_infoplat_role_info where is_delete=0 and state =0 order by update_date";
     return roleInfo;
 }

    /**
     * 查询企业信息
     * @return
     */
 public String getCompany(){
     String company="SELECT CORP_ID FROM zs_uup_enterprise_info";
     return company;
 }


    //返回默认角色集合数据
    public String[] roleInfoMap(ResultSet result){
        String[] str = new String[4];
        try {
            String roleName=result.getString("ROLE_NAME");
            str[0] =roleName+"";
            String functionInfo=result.getString("FUNCTION_INFO");
            str[1] =functionInfo+"";
            int isWork=result.getInt("IS_WORK");
            str[2] =isWork+"";
            int state=result.getInt("STATE");
            str[3] =state+"";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }


    //返回企业集合数据
    public String[] companyMap(ResultSet result){
        String[] str = new String[1];
        try {
            String corpId=result.getString("CORP_ID");
            str[0] =corpId+"";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * 添加角色信息
     * @return
     */
    public String insertRoleInfo(){
     String addRoleInfo="insert into zs_uup_role_info(uuid,role_name,function_info,is_work,state,corp_id)values(REPLACE(UUID(),'-',''),?,?,?,?,?)";
     return addRoleInfo;
    }

}
