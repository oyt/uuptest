package com.zsu.uup.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.EnterpriseInfoService;
import com.zsu.uup.entity.EnterpriseInfo;
import com.zsu.uup.pageinfo.PageInfo;

@RequestMapping(value = "EnterpriseInfo")
@Controller
public class EnterpriseInfoController {
	@Reference
	private EnterpriseInfoService enterpriseInfo;
	
	@RequestMapping(value="/getEnterpriseList",method = RequestMethod.GET)
	public ModelAndView getEnterpriseList() {
		ModelAndView model = new ModelAndView("file/NewFile");
		Map<String, String> map = new HashMap<String, String>();
		PageInfo<EnterpriseInfo> enterList = enterpriseInfo.getEnterpriseList(map);
		model.addObject("enterList",enterList.getRows());
		return model;
	}
}
