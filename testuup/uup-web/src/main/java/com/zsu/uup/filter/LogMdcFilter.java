/**
 * oyt
 */
package com.zsu.uup.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * <p>
 * 
 * </p>
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * @author Tao.Ouyang (Create on:2018年6月8日)
 * @version 1.0
 * @fileName LogMdcFilter.java
 */
public class LogMdcFilter implements Filter {

    private Logger logger = LoggerFactory.getLogger(LogMdcFilter.class);

    @Override
    public void destroy() {
        logger.info("日志过滤器销毁");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        // 获得客户的网络地址
        final String address = req.getRemoteAddr();
        //获取token
        final String accessToken = req.getParameter("access_token");
        // Put it in the MDC map.
        MDC.put("accessToken", accessToken);
        MDC.put("address", address);
        try {
            chain.doFilter(req, resp);
        }
        finally {
            MDC.clear();
            ;
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        logger.info("日志过滤器初始化");
    }

}
