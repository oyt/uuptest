package com.zsu.uup.toSql;

import java.sql.*;


public class DBUtil {
	
	//数据库连接信息
	private static final  String URL = "jdbc:sqlserver://192.168.3.11:1433;DatabaseName=CPODataArk";
	private static final String USER = "cp21";
	private static final String PASSWORD = "21cp.com";
	
	private static Connection conn=null;
    //静态代码块（将加载驱动、连接数据库放入静态块中）
    static{
        try {
            //1.加载驱动程序
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //2.获得数据库的连接
            conn=(Connection)DriverManager.getConnection(URL,USER,PASSWORD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
  //对外提供一个方法来获取数据库连接
    public static Connection getConnection(){
        return conn;
    }

    /**
     * 公司信息1
     * */
    public ResultSet getCompanyInfo() {
    	Statement stmt = null;
    	ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			CompanyUtil com = new CompanyUtil();
			String sql = com.getCompanyInfo();
			System.out.println("----------------"+sql);
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
    }

    /**
     * 公司信息2
     * */
    public ResultSet getCompanyInfo1() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            CompanyUtil com = new CompanyUtil();
            String sql = com.getCompanyInfo1();
            System.out.println("----------------"+sql);
            rs = stmt.executeQuery(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


    /**
     * 工商信息
     * */
    public ResultSet getBussiness() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            CompanyUtil com = new CompanyUtil();
            String sql = com.getBussiness();
            System.out.println("----------------"+sql);
            rs = stmt.executeQuery(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /**
     * 资质证书表
     * */
    public ResultSet getQuality() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            CompanyUtil com = new CompanyUtil();
            String sql = com.getQuality();
            System.out.println("----------------"+sql);
            rs = stmt.executeQuery(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


    /**
     * 资质证书表2
     * */
    public ResultSet getQuality2() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            CompanyUtil com = new CompanyUtil();
            String sql = com.getQuality2();
            System.out.println("----------------"+sql);
            rs = stmt.executeQuery(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


    /**
     * 品牌代理表
     * */
    public ResultSet getppdl() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            CompanyUtil com = new CompanyUtil();
            String sql = com.getppdl();
            System.out.println("----------------"+sql);
            rs = stmt.executeQuery(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /**
     * 中塑企业认证表
     * */
    public ResultSet getzsqyrz() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            CompanyUtil com = new CompanyUtil();
            String sql = com.getzsrzb();
            System.out.println("----------------"+sql);
            rs = stmt.executeQuery(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /**
     * 用户信息
     * @return
     */
    public ResultSet getUserInfo(){
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            UserUtil userUtil=new UserUtil();
            String sql=userUtil.getUserInfo();
            rs=stmt.executeQuery(sql);
            System.out.println(sql);
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }






}
