package com.zsu.uup.toSql;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;
import java.util.Map;

public class MysqlUtil2 {

	/**
	 * spring.datasource.url=jdbc:mysql://101.132.123.246:3307/testoyt?allowMultiQueries=true&useUnicode=true&characterEncoding=utf8&autoReconnect=true&failOverReadOnly=false&useSSL=false
	 * #&multiStatementAllow=true
	 * spring.datasource.username=testuser
	 * spring.datasource.password=testuser123!
	 * spring.datasource.connectionProperties=druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000;config.decrypt=false
	 *
	 * */

//	private static final  String URL1 = "jdbc:mysql://39.106.70.233:3306/testoyt?allowMultiQueries=true&useUnicode=true&characterEncoding=utf8&autoReconnect=true&failOverReadOnly=false&useSSL=false&useAffectedRows=true";
//	private static final String USER1 = "root";
//	private static final String PASSWORD1 = "Root2017@";

	
	private static final  String URL1 = "jdbc:mysql://101.132.123.246:3307/zs_main?allowMultiQueries=true&useUnicode=true&characterEncoding=utf8&autoReconnect=true&failOverReadOnly=false&useSSL=false";
	private static final String USER1 = "testuser";
	private static final String PASSWORD1 = "testuser123!";
	
	private static Connection conn=null;
    //静态代码块（将加载驱动、连接数据库放入静态块中）
    static{
        try {
            //1.加载驱动程序
            Class.forName("com.mysql.jdbc.Driver");
            //2.获得数据库的连接
            conn=(Connection)DriverManager.getConnection(URL1,USER1,PASSWORD1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
  //对外提供一个方法来获取数据库连接
    public static Connection getConnection(){
        return conn;
    }

    /**
	 * 导数据
	 * */
    public synchronized void insertInfo(Map<String,String[]> map , String sql){
    	CompanyUtil comp = new CompanyUtil();
		conn = MysqlUtil2.getConnection();
		try {
			for(String key :map.keySet()){
				String[] str = map.get(key);
				PreparedStatement ps = conn.prepareStatement(sql);
				for (int i = 0,j = 1; i < str.length; i++,j++) {
					String msg = "j = "+ j +"  ----  i = "+str[i];
					System.out.println(msg);
					ps.setString(j, str[i]);
				}
				try{
					int num = ps.executeUpdate();
					System.out.println("---------"+num);
				}catch(Exception e){
					System.out.println(key);
					e.printStackTrace();
				}
			}
            System.out.println(map.size() +"-------"+sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

	/**
	 * 通用查询
	 * */
	public String getSqlRest(String sql) {
		Statement stmt = null;
		ResultSet rs = null;
		String uuid = null;
		try {
			stmt = conn.createStatement();
			System.out.println("----------------"+sql);
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				uuid = rs.getString("uuid");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return uuid;
	}


		public void logExit(String msg){
			OutputStream output = null;
			try{
				File file = new File("C:\\Users\\sunshine\\Desktop\\log.tet");
				if(!file.getParentFile().exists()){ //如果文件的目录不存在
					file.getParentFile().mkdirs(); //创建目录
				}
				output = new FileOutputStream(file);
				//将字符串变为字节数组
				byte data[] = msg.getBytes();
				output.write(data);
			}catch(Exception ex){
				ex.printStackTrace();
			}finally {
				try{
					output.close();
				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		}
    
    
}
