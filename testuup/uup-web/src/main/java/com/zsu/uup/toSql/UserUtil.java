package com.zsu.uup.toSql;

import java.sql.ResultSet;
import java.sql.SQLException;
public class UserUtil {


    /**
     * 获取用户信息表
     */
 public  String getUserInfo(){
     //查询用户信息
     /*String userInfo="Select ui.UI_Id, ui.CI_Id, ui.UI_IsMain,ui.UI_State, ui.UI_LoginName, ui.UI_LoginPassword, ui.UI_NickName, ui.UI_Sex, ui.UI_Tel, ui.UI_MobilePhone, ui.UI_Email, ui.UI_Position,cai.Cai_IDCards from\n" +
         "        ZS_UserInfo ui\n" +
         "        LEFT JOIN ZS_CorpInfo ci on ui.CI_Id = ci.CI_Id\n" +
         "        LEFT JOIN ZS_CorpAuthenticateInfo cai on ci.CI_Id = cai.CI_Id and ui.CI_Id=cai.CI_Id\n" +
         "        where ui.UI_State in ('0','1','4') and ui.UI_Id not in('872386','873493','876759','879258','884767','891924','904561','904562','904998','971877','979911'\n" +
         "            ,'980990','983868 ','986267','993830','995198','1002542','1006636','1010471','1014890','1015071') ORDER BY ui.UI_Id";*/
     //禁用用户查询
     String userInfo="Select ui.CI_Id, ui.UI_IsMain,ui.UI_State, ui.UI_LoginName, ui.UI_LoginPassword, ui.UI_NickName, ui.UI_Sex, ui.UI_Tel, ui.UI_MobilePhone, ui.UI_Email, ui.UI_Position,cai.Cai_IDCards from ZS_UserInfo ui \n" +
             "left JOIN ZS_CorpInfo ci on ui.CI_Id = ci.CI_Id \n" +
             "left JOIN ZS_CorpAuthenticateInfo cai on ci.CI_Id = cai.CI_Id and ui.CI_Id=cai.CI_Id\n" +
             "where ui.UI_State = 3";
     return userInfo;
 }


    //返回集合数据
    public String[] userInfoMap(ResultSet result){
        String[] str = new String[12];
        try {
            int ciId=result.getInt("CI_ID");
            str[0] =ciId+"";
            int uiIsMain=result.getInt("UI_IsMain");
            str[1] =uiIsMain+"";
            int uiState=result.getInt("UI_State");
            str[2] =uiState+"";
            String uiLoginName=result.getString("UI_LoginName");
            str[3] =uiLoginName+"";
            String uiLoginPassword=result.getString("UI_LoginPassword");
            str[4] =uiLoginPassword+"";
            String uiNickName=result.getString("UI_NickName");
            str[5] =uiNickName+"";
            int uiSex=result.getInt("UI_Sex");
            str[6] =uiSex+"";
            String uiTel=result.getString("UI_Tel");
            str[7] =uiTel+"";
            String uiMobilePhone=result.getString("UI_MobilePhone");
            str[8] =uiMobilePhone+"";
            String uiEmail=result.getString("UI_Email");
            str[9] =uiEmail+"";
            String uiPosition=result.getString("UI_Position");
            str[10] =uiPosition+"";
            String caiIdCards=result.getString("Cai_IDCards");
            str[11] =caiIdCards+"";
            /*int uiId=result.getInt("UI_ID");
            str[12] =uiId+"";*/
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * 添加用户信息
     * @return
     */
    public String insertUserInfo(){
       //添加未禁用用户
     //String addUserInfo="insert into zs_uup_user_info(CID_TEST,CI_ISMAINTEST,CI_StateTest,user_id,user_name,user_password,nick_name,user_sex,user_tel,user_phone,user_email,user_position,USER_ID_NUMBER)values(?,?,?,REPLACE(UUID(),'-',''),?,?,?,?,?,?,?,?,?)";


        //添加禁用用户
        String addUserInfo="insert into zs_uup_user_info(CID_TEST,CI_ISMAINTEST,CI_StateTest,user_id,user_name,user_password,nick_name,user_sex,user_tel,user_phone,user_email,user_position,USER_ID_NUMBER,IS_WORK)values(?,?,?,REPLACE(UUID(),'-',''),?,?,?,?,?,?,?,?,?,'1')";
     return addUserInfo;
    }

}
