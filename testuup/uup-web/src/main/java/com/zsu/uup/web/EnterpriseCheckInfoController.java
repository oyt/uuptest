package com.zsu.uup.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.EnterpriseCheckInfoService;
import com.zsu.uup.entity.EnterpriseCheckInfo;
import com.zsu.uup.pageinfo.PageInfo;

@RequestMapping(value = "EnterpriseCheckInfo")
@Controller
public class EnterpriseCheckInfoController {
	@Reference
	private EnterpriseCheckInfoService enterpriseCheckInfo;

	/**
	 * 
	 * <p>
	 * Title: getEnterpriseCheckList(企业资料审核 未审核)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/getEnterpriseCheckList", method = RequestMethod.GET)
	public ModelAndView getEnterpriseCheckList() {
		ModelAndView model = new ModelAndView("file/NewFile");
		Map<String, String> map = new HashMap<String, String>();
		PageInfo<EnterpriseCheckInfo> enterpriseList = enterpriseCheckInfo
				.getEnterpriseCheckList(map);
		model.addObject("enterpriseList", enterpriseList.getRows());
		return model;
	}

	/**
	 * 
	 * <p>
	 * Title: getEnterpriseCheckListEnd(企业资料审核 已审核)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/getEnterpriseCheckListEnd", method = RequestMethod.GET)
	public ModelAndView getEnterpriseCheckListEnd() {
		ModelAndView model = new ModelAndView("file/NewFile");
		Map<String, String> map = new HashMap<String, String>();
		PageInfo<EnterpriseCheckInfo> enterpriseListEnd = enterpriseCheckInfo
				.getEnterpriseCheckListEnd(map);
		model.addObject("enterpriseListEnd", enterpriseListEnd.getRows());
		return model;
	}

	@RequestMapping(value = "/getEnterpriseCheckSomethingById",method = RequestMethod.GET)
	@ResponseBody
	public List<EnterpriseCheckInfo> getEnterpriseCheckSomethingById(Integer id) {
		List<EnterpriseCheckInfo> enterpriseLis = enterpriseCheckInfo
				.getEnterpriseCheckSomethingById(0);
		System.out.println("===========================================");
		return enterpriseLis;
	}
}
