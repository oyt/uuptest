package com.zsu.uup.web;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zsu.uup.IndustryClassService;
import com.zsu.uup.entity.IndustryClass;

@RequestMapping(value="IndustryClass")
@Controller
public class IndustryClassController {
	@Reference
	private IndustryClassService industryClassService;
	/**
	 * 
	 * <p>Title: getIndustryClass</p>  
	 * 		获得行业分类信息
	 * @author WangShiPing (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value="GetIndustryClass",method=RequestMethod.GET)
	@ResponseBody
	public String getIndustryClass(){
		List<IndustryClass> ilist = industryClassService.getAllIndustryClass();
		for (IndustryClass i : ilist) {
			System.out.println(i.getIcName()+"	");
		}
		return "good nice!";
	}
}
