package com.zsu.uup.toSql;

import java.sql.ResultSet;

public class MainAction {

	
	public static void main(String[] args) {

//			//导入公司数据1
			Thread thread = new CompanyInfo();
			thread.start();
//
//			//导入公司数据2
//			Thread thread1 = new CompanyInfo1();
//			thread1.start();
//
//			//工商信息表
//			Thread thread6 = new Bussiness();
//			thread6.start();
//
//			//资质证书表
//			Thread thread2 = new Quality();
//			thread2.start();
//
//			//资质证书表2
//			Thread thread3 = new Quality2();
//			thread3.start();
//
//			//品牌代理表
//			Thread thread4 = new ppdl();
//			thread4.start();
//
//			//中塑企业认证表
//			Thread thread5 = new zsrzb();
//			thread5.start();

	}

	/**
	 * 导入公司数据1
	 * */
	public  void setCompanyInfo(){
		//插入单表数据
		DBUtil dbUtil = new DBUtil();
		MysqlUtil myUtil = new MysqlUtil();
		ResultSet rs = dbUtil.getCompanyInfo();
		myUtil.insertInfo(rs,"导入公司数据1");
	}

	/**
	 * 导入公司数据2
	 * */
	public  void setCompanyInfo1(){
		//插入单表数据
		DBUtil dbUtil = new DBUtil();
		MysqlUtil myUtil = new MysqlUtil();
		ResultSet rs = dbUtil.getCompanyInfo1();
		myUtil.insertInfo1(rs,"导入公司数据2");

	}

	/**
	 * 工商信息
	 * */
	public  void setBussiness(){
		//插入单表数据
		DBUtil dbUtil = new DBUtil();
		MysqlUtil myUtil = new MysqlUtil();
		ResultSet rs = dbUtil.getBussiness();
		myUtil.insertInfo6(rs,"工商信息");
	}

	/**
	 * 资质证书表
	 * */
	public  void getQuality(){
		//插入单表数据
		DBUtil dbUtil = new DBUtil();
		MysqlUtil myUtil = new MysqlUtil();
		ResultSet rs = dbUtil.getQuality();
		myUtil.insertInfo2(rs,"资质证书表");

	}

	/**
	 * 资质证书表2
	 * */
	public  void getQuality2(){
		//插入单表数据
		DBUtil dbUtil = new DBUtil();
		MysqlUtil myUtil = new MysqlUtil();
		ResultSet rs = dbUtil.getQuality2();
		myUtil.insertInfo3(rs,"资质证书表2");

	}

	/**
	 * 品牌代理表
	 * */
	public  void setppdl(){
		//插入单表数据
		DBUtil dbUtil = new DBUtil();
		MysqlUtil myUtil = new MysqlUtil();
		ResultSet rs = dbUtil.getppdl();
		myUtil.insertInfo4(rs,"品牌代理表");
	}

	/**
	 * 中塑企业认证表
	 * */
	public  void getzsrzb(){
		//插入单表数据
		DBUtil dbUtil = new DBUtil();
		MysqlUtil myUtil = new MysqlUtil();
		ResultSet rs = dbUtil.getzsqyrz();
		myUtil.insertInfo5(rs,"中塑企业认证表");
	}






}

class CompanyInfo extends Thread{

	MainAction  mainAction = new MainAction();
	@Override
	public void run() {
		mainAction.setCompanyInfo();
	}
}

class CompanyInfo1 extends Thread{

	MainAction  mainAction = new MainAction();
	@Override
	public void run() {
		mainAction.setCompanyInfo1();
	}

}

class Bussiness extends Thread{

	MainAction  mainAction = new MainAction();
	@Override
	public void run() {
		mainAction.setBussiness();
	}
}

class Quality extends Thread{

	MainAction  mainAction = new MainAction();
	@Override
	public void run() {
		mainAction.getQuality();
	}
}

class Quality2 extends Thread{

	MainAction  mainAction = new MainAction();
	@Override
	public void run() {
		mainAction.getQuality2();
	}
}

class ppdl extends Thread{

	MainAction  mainAction = new MainAction();
	@Override
	public void run() {
		mainAction.setppdl();
	}
}

class zsrzb extends Thread{

	MainAction  mainAction = new MainAction();
	@Override
	public void run() {
		mainAction.getzsrzb();
	}
}
