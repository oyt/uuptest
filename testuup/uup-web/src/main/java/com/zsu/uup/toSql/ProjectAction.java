package com.zsu.uup.toSql;
import java.util.Map;

public class ProjectAction {


    public static void main(String[] args) {

        //
//        getIndustryClass(); //分类
//        getFactory();//厂家
////        getTestMethod();
//        getProductSymbol();//产品符号
//        getUnits();
////        getProcessing();
//        getCharacteristic();
//        getAdditive(); //添加劑
////        getFiller();  //填料
////        getAppearance();
//        getPerformance();//性能项
//        getPurpose();  //用途

//        Thread thread0 = new IndustryClass();
//        thread0.start();
//
//        Thread thread1 = new Factory();
//        thread1.start();
//
//        Thread thread2 = new TestMethod();
//        thread2.start();
//
//        Thread thread3 = new ProductSymbol();
//        thread3.start();
//
//        Thread thread4 = new Units();
//        thread4.start();
//
//        Thread thread5 = new Processing();
//        thread5.start();
//
//        Thread thread6 = new Characteristic();
//        thread6.start();
//
//        Thread thread7 = new Additive();
//        thread7.start();
//
//        Thread thread8 = new Filler();
//        thread8.start();
//
//        Thread thread9 = new Appearance();
//        thread9.start();
//
//        Thread thread10 = new Performance();
//        thread10.start();
//
//        Thread thread11 = new Purpose();
//        thread11.start();

        //添加对应关系表

        try {
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getKeyValue("C://Users//oyt//Desktop//数据//中英//中英//加工方法.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setKeyValue();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getKeyValue("C://Users//oyt//Desktop//数据//中英//中英//特性中英.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setKeyValue();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getKeyValue("C://Users//oyt//Desktop//数据//中英//中英//填料增强材料.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setKeyValue();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getKeyValue("C://Users//oyt//Desktop//数据//中英//中英//外观.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setKeyValue();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getKeyValue("C://Users//oyt//Desktop//数据//中英//中英//用途项中英.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setKeyValue();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch (Exception e){
            e.printStackTrace();
        }





    }



    //获取行业分类
    public static void getIndustryClass()  {
        try {
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getIndustryClass("C://Users//oyt//Desktop//数据//分类.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setIndustryClass();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //测试方法
    public static void getTestMethod()  {
        try {
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getTestMethod("C://Users//oyt//Desktop//数据//测试方法.xls");
            System.out.println(str);
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setTestMethod();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //产品符号
    public static void getProductSymbol(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getProductSymbol("C://Users//oyt//Desktop//数据//产品符号.xls");
            System.out.println(str);
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setProductSymbol();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //厂家
    public static void getFactory(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getManufactor("C://Users//oyt//Desktop//数据//厂家.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setManufactor();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //单位
    public static void getUnits(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getUnits("C://Users//oyt//Desktop//数据//单位.xls");
            System.out.println(str);
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setUnits();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //加工方法 （规范名称 有重复数据 ：拉丝）
    public static void getProcessing(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getProcessing("C://Users//oyt//Desktop//数据//加工方法.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setProcessing();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //特性列表
    public static void getCharacteristic(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getCharacteristic("C://Users//oyt//Desktop//数据//特性.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setCharacteristic();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


    //添加剂
    public static void getAdditive(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getAdditive("C://Users//oyt//Desktop//数据//添加剂.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setAdditive();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //填料
    public static void getFiller(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getFiller("C://Users//oyt//Desktop//数据//填料增强材料.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setFiller();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //外观
    public static void getAppearance(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getAppearance("C://Users//oyt//Desktop//数据//外观.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setAppearance();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    //性能项 (需要设置 性能名称长度)
    public static void getPerformance(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String,Map<String, String[]>> str = execlUtil.getPerformance("C://Users//oyt//Desktop//数据//纯性能.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setPerformance();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str.get("reMap"),sql);

            String sql2 = projectInfo.setPerformanceUnit();
            mysqlUtil2.insertInfo(str.get("Object"),sql2);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


    //用途项
    public static void getPurpose(){
        try{
            ExeclUtil execlUtil = new ExeclUtil();
            Map<String, String[]> str = execlUtil.getPurpose("C://Users//oyt//Desktop//数据//用途项.xls");
            ProjectInfo projectInfo = new ProjectInfo();
            String sql = projectInfo.setPurpose();
            MysqlUtil2 mysqlUtil2 = new MysqlUtil2();
            mysqlUtil2.insertInfo(str,sql);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

}


class IndustryClass extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getIndustryClass();
    }
}

class Factory extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getFactory();
    }
}

class TestMethod extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getTestMethod();
    }
}

class ProductSymbol extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getProductSymbol();
    }
}

class Units extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getUnits();
    }
}

class Processing extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getProcessing();
    }
}


class Characteristic extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getCharacteristic();
    }
}


class Additive extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getAdditive();
    }
}

class Filler extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getFiller();
    }
}

class Appearance extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getAppearance();
    }
}

class Performance extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getPerformance();
    }
}

class Purpose extends Thread{

    ProjectAction  mainAction = new ProjectAction();
    @Override
    public void run() {
        mainAction.getPurpose();
    }
}

