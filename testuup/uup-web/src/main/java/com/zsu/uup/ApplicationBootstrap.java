/**
 * oyt
 */
package com.zsu.uup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * 
 * @author Tao.Ouyang (Create on:2018年6月4日)
 * @version 1.0
 * @fileName ApplicationBootstrap.java
 */
@SpringBootApplication(scanBasePackages = { "com.zsu.uup" })
@ImportResource(value = { "classpath:applicationContext.xml" })
public class ApplicationBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationBootstrap.class, args);
    }

}
