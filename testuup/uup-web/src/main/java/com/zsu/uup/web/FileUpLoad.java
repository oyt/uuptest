package com.zsu.uup.web;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping(value = "FileUpLoad")
@Controller
public class FileUpLoad {

	@RequestMapping(value = "uploading", method = RequestMethod.POST)
	@ResponseBody
	public String upload(MultipartFile filenName) {
		// 通过MultipartFile 对象获取文件的原文件名
		if (filenName != null) {
			String path = "D:";
			String fileName = filenName.getOriginalFilename();
			// 获取文件的后缀名
			int i = fileName.lastIndexOf(".");
			String fileNameEnd = fileName.substring(i);
			// 将路径转化为文件夹 并 判断文件夹是否存在
			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdir();
			}
			// 获取一个文件的保存路径
			String path1 = path + "\\" + fileName + "\\" + fileNameEnd;

			// 为文件这服务器中开辟一给新的空间,*没有数据
			File newFile = new File(path1);
			try {
				filenName.transferTo(newFile);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.err.println("-----服务器的路径地址为：:" + path1);
			System.err.println("-----图片名称为：:" + fileName);
			System.err.println("-----图片新路径为：:" + path);

		}
		return "success";

	}

	@RequestMapping(value = "/upload")
	public ModelAndView upload() {
		ModelAndView model = new ModelAndView("file/FileUpload");
		return model;
	}
}
