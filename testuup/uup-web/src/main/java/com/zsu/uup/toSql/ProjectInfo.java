package com.zsu.uup.toSql;

public class ProjectInfo {


    //行业分类
    public String setKeyValue(){
        String compInfo = "INSERT into keyvalue "
                + "(" +
                "nameKey," +
                "nameValue)"
                + " values(?,?)";
        return compInfo;
    }

    //行业分类
    public String setIndustryClass(){
        String compInfo = "INSERT into zs_pd_industry_class "
                + "(UUID," +
                "INDUSTRY_PARENT_ID," +
                "INDUSTRY_NAME," +
                "INDUSTRY_QUANTITY_UNIT," +
                "INDUSTRY_LEVEL," +
                "INDUSTRY_SORT," +
                "INDUSTRY_REMARK," +
                "INDUSTRY_IS_DELETE,"+
                "CREATE_USER," +
                "UPDATE_USER," +
                "INDUSTRY_NAME_CONNECT)"
                + " values(?,?,?,?,?,?,?,?,?,?,?)";
        return compInfo;
    }
    //厂家管理
    public String setManufactor(){
        String compInfo = "INSERT into zs_pd_manufactor "
                + "(UUID," +
                "MANUFACTOR_NAME," +
                "MANUFACTOR_ENGLISH_NAME," +
                "MANUFACTOR_SHORT_NAME," +
                "MANUFACTOR_ADDRESS," +
                "MANUFACTOR_WEBSITE," +
                "MANUFACTOR_KEYWORD," +
                "MANUFACTOR_LOGO," +
                "MANUFACTOR_REMARK,"+
                "MANUFACTOR_IS_DELETE,"+
                "MANUFACTOR_COUNTRY," +
                "MANUFACTOR_PROVINCE," +
                "MANUFACTOR_CITY," +
                "MANUFACTOR_DISTRICT," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(REPLACE(UUID(),'-',''),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return compInfo;
    }
    //品牌管理
    public String setBrand(){
        String compInfo = "INSERT into zs_pd_brand "
                + "(UUID," +
                "BRAND_NAME," +
                "BRAND_ENGLISH_NAME," +
                "BRAND_INITIALS," +
                "BRAND_LOGO," +
                "BRAND_BIG_LOGO," +
                "BRAND_SORT," +
                "BRAND_IS_DISPLAY," +
                "BRAND_IS_DELETE,"+
                "CREATE_USER," +
                "UPDATE_USER," +
                "BRAND_REMARK)"
                + " values(?,?,?,?,?,?,?,?,?,?,?,?)";
        return compInfo;
    }
    //产品符号列表
    public String setProductSymbol(){
        String compInfo = "INSERT into zs_pd_product_symbol "
                + "(uuid," +
                "SYMBOL_NAME," +
                "SYMBOL_CHINESE_NAME," +
                "SYMBOL_ENGLISH_NAME," +
                "SYMBOL_INDUSTRY_CLASS_UUID," +
                "SYMBOL_IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER," +
                "SYMBOL_ALIAS," +
                "SYMBOL_REMARK)"
                + " values(?,?,?,?,?,?,?,?,?,?)";
        return compInfo;
    }
    /**
     * 特性列表
     * */
    public String setCharacteristic(){
        String compInfo = "INSERT into zs_pd_characteristic "
                + "(UUID," +
                "CHARACTERISTIC_NAME," +
                "CHARACTERISTIC_GROUP," +
                "CHARACTERISTIC_ALIAS,"+
                "CHARACTERISTIC_IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?,?)";
        return compInfo;
    }

    //特性列表分组
    public String setCharacteristicGroup(){
        String compInfo = "INSERT into zs_pd_characteristic_grouping "
                + "(CHARACTERISTIC_GROUPING_NAME)"
                + " values(?)";
        return compInfo;
    }
    //加工方法列表
    public String setProcessing(){
        String compInfo = "INSERT into zs_pd_processing "
                + "(UUID," +
                "PROCESSING_NAME," +
                "PROCESSING_ENGLISH_NAME," +
                "PROCESSING_ALIAS," +
                "PROCESSING_IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?,?)";
        return compInfo;
    }


    //加工方法基础表
    public String setProcessingTechnology(){
        String compInfo = "INSERT into zs_pd_processing_technology "
                + "(UUID," +
                "TECHNOLOGY_NAME," +
                "TECHNOLOGY_ENGLISH_NAME," +
                "TECHNOLOGY_ALIAS," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?)";
        return compInfo;
    }
    //用途项列表

    public String setPurpose(){
        String compInfo = "INSERT into zs_pd_purpose "
                + "(UUID," +
                "PURPOSE_LOGO," +
                "PURPOSE_NAME," +
                "PURPOSE_ENGLISH_NAME," +
                "PURPOSE_ALIAS," +
                "PURPOSE_INDUSTRY_CLASS_UUID," +
                "PURPOSE_IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?,?,?,?)";
        return compInfo;
    }
    //性能项目列表
    public String setPerformance(){
        String compInfo = "INSERT into zs_pd_performance "
                + "(UUID," +
                "PERFORMANCE_NAME," +
                "PERFORMANCE_NICKNAME," +
                "GROUP_NAME," +
                "IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?,?)";
        return compInfo;
    }
    //性能项目列表分组
    public String setPerformanceGroup(){
        String compInfo = "INSERT into zs_pd_performance_group "
                + "(GROUP_NAME," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?)";
        return compInfo;
    }

    //性能项目列表分组
    public String setPerformanceUnit(){
        String compInfo = "INSERT into zs_pd_performance_to_units "
                + "(PERFORMANCE_ID," +
                "UNIT_ID," +
                "IS_DEFAULT)"
                + " values(?,?,?)";
        return compInfo;
    }


    //添加剂列表
    public String setAdditive(){
        String compInfo = "INSERT into zs_pd_additive "
                + "(UUID," +
                "ADDITIVE_INDUSTRY_ID," +
                "ADDITIVE_NAME," +
                "ADDITIVE_MANUFACTOR_ID," +
                "ADDITIVE_MODEL," +
                "ADDITIVE_REMARK," +
                "IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?,?,?,?)";
        return compInfo;
    }
    //填充（增强）料列表
    public String setFiller(){
        String compInfo = "INSERT into zs_pd_filler "
                + "(UUID," +
                "FILLER_NAME," +
                "FILLER_NICKNAME," +
                "IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?)";
        return compInfo;
    }
    //测试方法管理
    public String setTestMethod(){
        String compInfo = "INSERT into zs_pd_test_method "
                + "(UUID," +
                "TEST_METHOD_NAME," +
                "TEST_METHOD_FILENAME," +
                "TEST_METHOD_FILEURL," +
                "TEST_METHOD_INFO," +
                "IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?,?,?)";
        return compInfo;
    }
    //计量单位维护
    public String setUnits(){
        String compInfo = "INSERT into zs_pd_units "
                + "(UUID," +
                "UNITS_SYMBOL," +
                "UNITS_NAME," +
                "IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?)";
        return compInfo;
    }
    //外观维护
    public String setAppearance(){
        String compInfo = "INSERT into zs_pd_appearance "
                + "(UUID," +
                "APPEARANCE_NAME," +
                "APPEARANCE_NICKNAME," +
                "IS_DELETE," +
                "CREATE_USER," +
                "UPDATE_USER)"
                + " values(?,?,?,?,?,?)";
        return compInfo;
    }




}
