package com.zsu.uup.toSql;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserInfoCheckUtil {


    /**
     * 获取未审核用户
     */
 public  String getCheckUserInfo(){
     //查询未审核用户信息
   String userCheckInfo="select USER_ID,USER_ID_NUMBER,CI_StateTest,USER_SEX from zs_uup_user_info where CI_StateTest = 0";
     return userCheckInfo;
 }


    //返回集合数据
    public String[] userCheckInfoMap(ResultSet result){
        String[] str = new String[4];
        try {
            String userId=result.getString("USER_ID");
            str[0] =userId+"";
            String userIdNumber=result.getString("USER_ID_NUMBER");
            str[1] =userIdNumber+"";
            int ciStateTest=result.getInt("CI_StateTest");
            str[2] =ciStateTest+"";
            int userSex=result.getInt("USER_SEX");
            str[3] =userSex+"";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * 添加用户审核信息
     * @return
     */
    public String insertUserCheckInfo(){
     String addCheckUserInfo="INSERT into zs_uup_user_info_check(USER_ID,USER_ID_NUMBER,CHECK_STATUS,USER_SEX,ID_CARD_BEHIND_URL,USER_REAL_NAME,ID_CARD_START_INDATE) values(?,?,?,?,'null','null',now())";
     return addCheckUserInfo;
    }

}
