package com.zsu.uup.Mapper;

import com.zsu.uup.entity.UserToRoleInfo;
import tk.mybatis.mapper.common.Mapper;

public interface UserToRoleInfoMapper extends Mapper<UserToRoleInfo> {
}