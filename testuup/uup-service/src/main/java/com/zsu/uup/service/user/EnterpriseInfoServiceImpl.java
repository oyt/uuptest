package com.zsu.uup.service.user;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsu.uup.EnterpriseInfoService;
import com.zsu.uup.Mapper.EnterpriseInfoMapper;
import com.zsu.uup.entity.EnterpriseInfo;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 
 * <p>
 * Title: EnterpriseInfoServiceImpl
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Kouxu (Create on:2018年6月20日)
 * @version 1.0
 * @fileName EnterpriseInfoServiceImpl.java
 */
@Service(protocol = { "dubbo" }, timeout = 60000)
public class EnterpriseInfoServiceImpl implements EnterpriseInfoService {

	@Autowired
	private EnterpriseInfoMapper enterpriseInfo;

	/**
	 * 获取平台企业列表
	 * */
	@Override
	public PageInfo<EnterpriseInfo> getEnterpriseList(Map<String, String> map) {
		// TODO Auto-generated method stub
		PageHelper.startPage(0, 10);
		Page<EnterpriseInfo> page = enterpriseInfo.getEnterpriseList(map);
		PageInfo<EnterpriseInfo> enterList = new PageInfo<EnterpriseInfo>(0,
				10, page.getTotal(), page.getResult());
		return enterList;
	}

}
