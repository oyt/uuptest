package com.zsu.uup.Mapper;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.zsu.uup.entity.RoleInfoDto;

import tk.mybatis.mapper.common.Mapper;

/**
 * 角色管理
 * <p>Title: RoleInfoDtoMapper</p>  
 * <p>Description: </p> 
 * @author wangyongge (Create on:2018年6月20日)
 * @version 1.0
 * @fileName RoleInfoDtoMapper.java
 */
public interface RoleInfoDtoMapper extends Mapper<RoleInfoDto> {
	
	/**
	 * 查询角色列表
	 * <p>Title: selectAllRoleInfo</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月20日)
	 * @version 1.0
	 * @return
	 */
	public List<RoleInfoDto> selectAllRoleInfo();
	/**
	 * 添加角色名称
	 * <p>Title: insertRoleInfo</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月22日)
	 * @version 1.0
	 * @return
	 */
	public int insertRoleInfo(RoleInfoDto roleInfoDto);
	/**
	 * uuid查询单个用户角色
	 * <p>Title: selectRoleByUUId</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月22日)
	 * @version 1.0
	 * @param uuid
	 * @return
	 */
	public RoleInfoDto selectRoleByUUId(String uuid);
	/**
	 * 更新角色
	 * <p>Title: updateRoleInfo</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月22日)
	 * @version 1.0
	 * @param roleInfoDto
	 * @return
	 */
	public int updateRoleInfo(RoleInfoDto roleInfoDto);
	
	/**
	 * 删除角色
	 * <p>Title: deleteRoleInfo</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月22日)
	 * @version 1.0
	 * @param roleInfoDto
	 * @return
	 */
	public int deleteRoleInfo(RoleInfoDto roleInfoDto);
	/**
	 * 角色列表分页
	 * <p>Title: getRoleInfoList</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月22日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public Page<RoleInfoDto> getRoleInfoList(Map<String, String> map);
	/**
	 * 角色添加权限
	 * <p>Title: addRoleAuth</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月27日)
	 * @version 1.0
	 * @param roleAuth
	 * @return
	 */
	public int addRoleAuth(Map<String, Object> roleAuth);
	/**
	 * 删除角色权限
	 * <p>Title: deleteRoleAuth</p>  
	 * 
	 * @author wangyongge (Create on:2018年6月27日)
	 * @version 1.0
	 * @param uuid
	 * @return
	 */
	public int deleteRoleAuth(String uuid);
}