package com.zsu.uup.Mapper;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.zsu.uup.entity.ProductInfo;
import com.zsu.uup.entity.TestInfoDto;

import tk.mybatis.mapper.common.Mapper;

public interface ProductInfoMapper extends Mapper<ProductInfo> {
	/**
	 * 获取产品信息
	 * <p>Title: getAllProductInfo</p>  
	 * 
	 * @author WangShiPing (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	List<ProductInfo> getAllProductInfo();
	 /**
     * 分页查询
     * @author WangShiPing (Create on:2018年6月19日)
     * @version 1.0
     * @return
     */
     Page<ProductInfo> gettestinfo(Map<String, String> map);
     /**
 	 * 
 	 * <p>Title: getProductInfoAndGeneralDes</p>  
 	 * 		查询出产品信息，总体描述，物理性能等
 	 * @author WangShiPing (Create on:2018年6月20日)
 	 * @version 1.0
 	 * @param id	根据ID查询出产品信息
 	 * @return
 	 */
 	int getProductInfoAndGeneralDes(int id);
}