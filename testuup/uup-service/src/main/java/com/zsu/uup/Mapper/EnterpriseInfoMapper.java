package com.zsu.uup.Mapper;

import java.util.Map;

import com.github.pagehelper.Page;
import com.zsu.uup.entity.EnterpriseInfo;

import tk.mybatis.mapper.common.Mapper;

public interface EnterpriseInfoMapper extends Mapper<EnterpriseInfo> {
	/**
	 * 
	 * <p>Title: getEnterpriseList(获取平台企业列表)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public Page<EnterpriseInfo> getEnterpriseList(Map<String, String> map);
}