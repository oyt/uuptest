/**
 * oyt
 */
package com.zsu.uup;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <p>
 * 
 * </p>
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * @author Tao.Ouyang (Create on:2018年6月5日)
 * @version 1.0
 * @fileName ApplicationBootstrap.java
 */
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = { "com.zsu.uup" })
@ImportResource(value = { "classpath:spring/applicationContext.xml" })
public class ApplicationBootstrap {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();

        SpringApplication.run(ApplicationBootstrap.class, args);
    }
}
