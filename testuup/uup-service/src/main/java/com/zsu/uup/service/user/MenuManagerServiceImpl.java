package com.zsu.uup.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.zsu.uup.MenuManagerService;
import com.zsu.uup.Mapper.MenuManagerDtoMapper;
import com.zsu.uup.entity.MenuManagerDto;

/**
 * 用户中心菜单管理
 * <p>
 * Title: MenuManagerServiceImpl
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author wangyongge (Create on:2018年6月19日)
 * @version 1.0
 * @fileName MenuManagerServiceImpl.java
 */
@Service(protocol = { "dubbo" }, timeout = 60000)
public class MenuManagerServiceImpl implements MenuManagerService {

	@Autowired
	public MenuManagerDtoMapper menuManagerDtoMapper;

	/**
	 * 查询所有用户菜单
	 */
	@Override
	public List<MenuManagerDto> queryAllMenuManager() {
		return menuManagerDtoMapper.queryAllMenuManager();
	}
    /**
     * 查询顶级菜单
     */
	@Override
	public List<MenuManagerDto> selectTopmenu() {
		return menuManagerDtoMapper.selectTopmenu();
	}
	/**
	 * 顶级菜单查询二级菜单
	 */
	@Override
	public List<MenuManagerDto> selectChildmenu(String uuid) {
		return menuManagerDtoMapper.selectChildmenu(uuid);
	}
	/**
	 * 二级菜单查询三级菜单
	 */
	@Override
	public List<MenuManagerDto> selectGrandSonmenu(String uuid) {
		return menuManagerDtoMapper.selectGrandSonmenu(uuid);
	}
	// 根据uuId查询父级菜单
	@Override
	public List<MenuManagerDto> selectMenuManagerDtoByUUId(String uuid) {
		return menuManagerDtoMapper.selectMenuManagerDtoByUUId(uuid);
	}

	// 根据UUId查询关联子级菜单
	@Override
	public List<MenuManagerDto> selectMenuManagerDtoByUUIdOrParentId(String uuid) {
		return menuManagerDtoMapper.selectMenuManagerDtoByUUIdOrParentId(uuid);
	}

	// 添加用户一级菜单
	@Override
	public int insertMenuManager(MenuManagerDto menuManagerDto) {
		return menuManagerDtoMapper.insertMenuManager(menuManagerDto);
	}

	// 根据uuid删除用户父级菜单
	@Override
	public int deleteParentMenu(String uuid) {
		return menuManagerDtoMapper.deleteParentMenu(uuid);
	}

	// 根据parentid删除用户子级菜单
	@Override
	public int deleteChildMenu(String parentid) {
		return menuManagerDtoMapper.deleteChildMenu(parentid);
	}
    //更新（删除）菜单
	@Override
	public int deleteMenuManager(MenuManagerDto menuManagerDto) {
		return menuManagerDtoMapper.deleteMenuManager(menuManagerDto);
	}
	/**
	 * 添加子级菜单
	 */
	@Override
	public int insertChildMenu(MenuManagerDto menuManagerDto) {
		return menuManagerDtoMapper.insertChildMenu(menuManagerDto);
	}



}
