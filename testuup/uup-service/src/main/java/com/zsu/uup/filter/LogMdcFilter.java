/**
 * oyt
 */
package com.zsu.uup.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.alibaba.dubbo.rpc.Filter;
import com.alibaba.dubbo.rpc.Invocation;
import com.alibaba.dubbo.rpc.Invoker;
import com.alibaba.dubbo.rpc.Result;
import com.alibaba.dubbo.rpc.RpcContext;
import com.alibaba.dubbo.rpc.RpcException;

/**
 * <p>
 * 
 * </p>
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * @author Tao.Ouyang (Create on:2018年6月5日)
 * @version 1.0
 * @fileName LogMdcFilter.java
 */
public class LogMdcFilter implements Filter {

    private Logger logger = LoggerFactory.getLogger(LogMdcFilter.class);

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        logger.info("进入过滤器。。。");
        String clientIp = RpcContext.getContext().getRemoteHost();
        MDC.put("userCode", "U0001");
        MDC.put("address", clientIp);
        try {
            return invoker.invoke(invocation);
        }
        finally {
            MDC.clear();
        }
    }

}
