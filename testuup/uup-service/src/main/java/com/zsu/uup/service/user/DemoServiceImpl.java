/**
 * oyt
 */
package com.zsu.uup.service.user;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.zsu.uup.DemoService;
import com.zsu.uup.Mapper.DemoInfoMapper;
import com.zsu.uup.entity.DemoInfo;

/**
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * 
 * @author Tao.Ouyang (Create on:2018年6月5日)
 * @version 1.0
 * @fileName UserServiceImpl.java
 */
@Service(protocol = { "dubbo" }, timeout = 60000)
public class DemoServiceImpl implements DemoService {

	@Autowired
	public DemoInfoMapper demoInfoMapper;

	@Override
	public List<DemoInfo> getDemoList() {
		// TODO Auto-generated method stub
		return demoInfoMapper.getDemoList();
	}

	@Override
	public int insertDemo(DemoInfo demoInfo) {
		// TODO Auto-generated method stub
		return demoInfoMapper.insertDemo(demoInfo);
	}

	@Override
	public int updateDemo(DemoInfo demoInfo) {
		// TODO Auto-generated method stub
		return demoInfoMapper.updateDemo(demoInfo);
	}

	@Override
	public int deleteDemo(int t1id) {
		// TODO Auto-generated method stub
		return demoInfoMapper.deleteDemo(t1id);
	}

	@Override
	public List<DemoInfo> getDemoById(int t1id) {
		// TODO Auto-generated method stub
		return demoInfoMapper.getDemoById(t1id);
	}

	@Override
	public List<DemoInfo> getDemoByName(String t1name) {
		// TODO Auto-generated method stub
		return demoInfoMapper.getDemoByName(t1name);
	}

	@Override
	public int deleteDemoList(List<DemoInfo> dlist) {
		// TODO Auto-generated method stub
		return demoInfoMapper.deleteDemoList(dlist);
	}

	@Override
	public int updateDemoList(List<DemoInfo> dlist) {
		// TODO Auto-generated method stub
		return demoInfoMapper.updateDemoList(dlist);
	}

	@Override
	public int insertDemoList(List<DemoInfo> dlist) {
		// TODO Auto-generated method stub
		return demoInfoMapper.insertDemoList(dlist);
	}

	@Override
	public Map<String, String> selectDemoMap(Map<String, String> dmap) {
		// TODO Auto-generated method stub
		return demoInfoMapper.selectDemoMap(dmap);
	}

		
}
