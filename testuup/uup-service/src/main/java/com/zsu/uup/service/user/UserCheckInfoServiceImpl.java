package com.zsu.uup.service.user;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;







import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsu.uup.UserCheckInfoService;
import com.zsu.uup.Mapper.UserCheckInfoMapper;
import com.zsu.uup.entity.UserCheckInfo;
import com.zsu.uup.pageinfo.PageInfo;
/**
 * 
 * <p>Title: UserCheckInfoServiceImpl</p>  
 * <p>Description: </p> 
 * @author Kouxu (Create on:2018年6月19日)
 * @version 1.0
 * @fileName UserCheckInfoServiceImpl.java
 */
@Service(protocol = { "dubbo" }, timeout = 60000)
public class UserCheckInfoServiceImpl implements UserCheckInfoService{

	@Autowired
	private UserCheckInfoMapper usercheckinfo;
	//个人资料审核历史记录
	@Override
	public PageInfo<UserCheckInfo> getUserCheckList(Map<String, String> map) {
		// TODO Auto-generated method stub
		PageHelper.startPage(0, 10);
		Page<UserCheckInfo> page = usercheckinfo.getUserCheckList(map);
		PageInfo<UserCheckInfo> pageInfo = new PageInfo<UserCheckInfo>(0, 10, page.getTotal(), page.getResult());
		return pageInfo;
	}

}
