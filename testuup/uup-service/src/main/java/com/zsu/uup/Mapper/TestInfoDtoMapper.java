package com.zsu.uup.Mapper;

import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.github.pagehelper.Page;
import com.zsu.uup.entity.TestInfoDto;

public interface TestInfoDtoMapper extends Mapper<TestInfoDto> {

    public void getuserinfo();

    /**
     * 分页查询案例
     * @author Tao.Ouyang (Create on:2018年6月19日)
     * @version 1.0
     * @return
     */
    public Page<TestInfoDto> gettestinfo(Map<String, String> map);
}