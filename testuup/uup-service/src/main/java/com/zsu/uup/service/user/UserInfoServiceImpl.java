package com.zsu.uup.service.user;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsu.uup.UserInfoService;
import com.zsu.uup.Mapper.UserInfoMapper;
import com.zsu.uup.entity.UserInfo;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 
 * <p>
 * Title: UserInfoServiceImpl
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Kouxu (Create on:2018年6月19日)
 * @version 1.0
 * @fileName UserInfoServiceImpl.java
 */
@Service(protocol = { "dubbo" }, timeout = 60000)
public class UserInfoServiceImpl implements UserInfoService {
	@Autowired
	private UserInfoMapper userinfomapper;

	// 根据ID获取个人审核资料详情
	@Override
	public List<UserInfo> getUserInfoListById(Integer id) {
		// TODO Auto-generated method stub
		return userinfomapper.getUserInfoListById(id);
	}

	// 个人审核资料 未审核
	@Override
	public PageInfo<UserInfo> getUserInfoList(Map<String, String> map) {
		// TODO Auto-generated method stub
		PageHelper.startPage(0, 10);
		Page<UserInfo> page = userinfomapper.getUserInfoList(map);
		PageInfo<UserInfo> pageInfo = new PageInfo<UserInfo>(0, 10,
				page.getTotal(), page.getResult());
		return pageInfo;
	}

	// 个人审核资料 已审核
	@Override
	public PageInfo<UserInfo> getUserInfoListEnd(Map<String, String> map) {
		// TODO Auto-generated method stub
		PageHelper.startPage(0, 10);
		Page<UserInfo> page = userinfomapper.getUserInfoListEnd(map);
		PageInfo<UserInfo> pageInfo = new PageInfo<UserInfo>(0, 10,
				page.getTotal(), page.getResult());
		return pageInfo;
	}
	//获取平台账号列表
	@Override
	public PageInfo<UserInfo> getUserInfoAllthing(Map<String, String> map) {
		// TODO Auto-generated method stub
		PageHelper.startPage(0, 10);
		Page<UserInfo> page = userinfomapper.getUserInfoAllthing(map);
		PageInfo<UserInfo> pageInfo = new PageInfo<UserInfo>(0,10,page.getTotal(),page.getResult());
		return pageInfo;
	}
	
	@Override
	public UserInfo queryLoginInfo(Map<String, Object> map) {
		return userinfomapper.queryLoginInfo(map);
	}

	@Override
	public UserInfo queryLoginName(Map<String, Object> map) {
		return userinfomapper.queryLoginName(map);
	}

	@Override
	public Integer insertUserRegister(UserInfo userInfoDto) {
		return userinfomapper.insert(userInfoDto);
	}

	@Override
	public Integer updateUserPassWd(UserInfo userInfoDto ) {
		return userinfomapper.updateByPrimaryKey(userInfoDto);
	}

	@Override
	public void updateUserPassWd(Map<String, Object> item) {
		userinfomapper.updateUserPassWd(item);
	}

}
