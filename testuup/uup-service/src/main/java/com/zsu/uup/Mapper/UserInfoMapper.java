package com.zsu.uup.Mapper;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.zsu.uup.entity.UserInfo;
import com.zsu.uup.entity.UserCheckInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * <p>
 * Title: UserInfoMapper
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Kouxu (Create on:2018年6月19日)
 * @version 1.0
 * @fileName UserInfoMapper.java
 */

public interface UserInfoMapper extends Mapper<UserInfo> {
	/**
	 * 
	 * <p>
	 * Title: getUserInfoList(个人审核资料    待审核)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	public Page<UserInfo> getUserInfoList(Map<String, String> map);

	/**
	 * 
	 * <p>
	 * Title: getUserInfoListById(根据ID获取个人审核资料详情)
	 * </p>
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @param id
	 * @return
	 */
	public List<UserInfo> getUserInfoListById(Integer id);
	/**
	 * 
	 * <p>Title: getUserInfoListEnd(个人资料审核   已审核)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	public Page<UserInfo> getUserInfoListEnd(Map<String, String> map);
	
	/**
	 * 
	 * <p>Title: getUserInfoAllthing(获取平台账号列表)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public Page<UserInfo> getUserInfoAllthing(Map<String, String> map);
	
	
	public String login(UserInfo userInfoDto);
	
	public UserInfo queryLoginName(Map<String,Object> map);
	
	public UserInfo queryLoginInfo(Map<String,Object> map);
	
	public void updateUserPassWd(Map<String,Object> item);
	
	
	
}