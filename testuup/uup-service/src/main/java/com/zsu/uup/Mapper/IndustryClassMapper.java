package com.zsu.uup.Mapper;

import java.util.List;

import com.zsu.uup.entity.IndustryClass;

import tk.mybatis.mapper.common.Mapper;

public interface IndustryClassMapper extends Mapper<IndustryClass> {
	/**
	 * 
	 * <p>Title: getAllIndustryClass</p>  
	 * 		获得行业分类信息
	 * @author WangShiPing (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	List<IndustryClass> getAllIndustryClass();
}