package com.zsu.uup.Mapper;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;
import com.zsu.uup.entity.EnterpriseCheckInfo;

import tk.mybatis.mapper.common.Mapper;

public interface EnterpriseCheckInfoMapper extends Mapper<EnterpriseCheckInfo> {
	/**
	 * 
	 * <p>Title: getEnterpriseCheckList(企业资料审核    未审核)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public Page<EnterpriseCheckInfo> getEnterpriseCheckList(Map<String, String> map);
	/**
	 * 
	 * <p>Title: getEnterpriseCheckListEnd(企业信息审核   已审核)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param map
	 * @return
	 */
	public Page<EnterpriseCheckInfo> getEnterpriseCheckListEnd(Map<String, String> map);
	
	/**
	 * 
	 * <p>Title: getEnterpriseCheckSomethingById(根据ID获取该企业提交的审核资料详情)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月20日)
	 * @version 1.0
	 * @param id
	 * @return
	 */
	public List<EnterpriseCheckInfo> getEnterpriseCheckSomethingById(Integer id);
}