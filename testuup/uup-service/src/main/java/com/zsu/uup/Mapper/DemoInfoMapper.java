package com.zsu.uup.Mapper;

import java.util.List;
import java.util.Map;

import com.zsu.uup.entity.DemoInfo;

import tk.mybatis.mapper.common.Mapper;

public interface DemoInfoMapper extends Mapper<DemoInfo> {
	public List<DemoInfo> getDemoList();

	public Map<String, String> getDemoMap();

	public List<DemoInfo> getDemoById(int t1id);

	public int insertDemo(DemoInfo demoInfo);

	public int deleteDemo(int t1id);

	public int updateDemo(DemoInfo demoInfo);

	public List<DemoInfo> getDemoByName(String t1name);

	public int deleteDemoList(List<DemoInfo> dlist);

	public int updateDemoList(List<DemoInfo> dlist);

	public int insertDemoList(List<DemoInfo> dlist);

	public Map<String, String> selectDemoMap(Map<String, String> dmap);
}
