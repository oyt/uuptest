package com.zsu.uup.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.zsu.uup.IndustryClassService;
import com.zsu.uup.Mapper.IndustryClassMapper;
import com.zsu.uup.entity.IndustryClass;

@Service(protocol = { "dubbo" }, timeout = 60000)
public class IndustryClassServiceImpl implements IndustryClassService{
	@Autowired
	private IndustryClassMapper industryClass;
	
	@Override
	public List<IndustryClass> getAllIndustryClass() {
		// TODO Auto-generated method stub
		return industryClass.getAllIndustryClass();
	}

}
