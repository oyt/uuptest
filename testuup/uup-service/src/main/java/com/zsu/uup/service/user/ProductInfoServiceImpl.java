package com.zsu.uup.service.user;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsu.uup.ProductInfoService;
import com.zsu.uup.Mapper.ProductInfoMapper;
import com.zsu.uup.entity.ProductInfo;
import com.zsu.uup.pageinfo.PageInfo;

@Service(protocol = { "dubbo" }, timeout = 60000)
public class ProductInfoServiceImpl implements ProductInfoService {
	@Autowired
	private ProductInfoMapper productInfo;

	@Override
	public List<ProductInfo> getAllProductInfo() {
		return productInfo.getAllProductInfo();
	}

//	@Override
//	public List<ProductInfo> getListProductInfo(Map<String, String> map) {
//		PageHelper.startPage(0, 10);
//		Page<ProductInfo> page = productInfo.gettestinfo(map);
//		PageInfo<ProductInfo> pageinfo = new PageInfo<ProductInfo>(0, 10,page.getTotal(), page.getResult());
//		return pageinfo.getRows();
//	}
	
	@Override
	public PageInfo<ProductInfo> getListProductInfo(Map<String, String> map) {
		PageHelper.startPage(0, 10);
		Page<ProductInfo> page = productInfo.gettestinfo(map);
		PageInfo<ProductInfo> pageinfo = new PageInfo<ProductInfo>(0, 10,page.getTotal(), page.getResult());
		return pageinfo;
	}

	@Override
	public int getProductInfoAndGeneralDes(int id) {
		return productInfo.getProductInfoAndGeneralDes(id);
	}

}
