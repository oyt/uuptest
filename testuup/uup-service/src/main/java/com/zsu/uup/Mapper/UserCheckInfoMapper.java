package com.zsu.uup.Mapper;


import java.util.Map;

import com.github.pagehelper.Page;
import com.zsu.uup.entity.UserCheckInfo;

import tk.mybatis.mapper.common.Mapper;

public interface UserCheckInfoMapper extends Mapper<UserCheckInfo> {
	/**
	 * 
	 * <p>Title: getUserCheckList(个人资料审核历史记录)</p>  
	 * 
	 * @author Kouxu (Create on:2018年6月19日)
	 * @version 1.0
	 * @return
	 */
	public Page<UserCheckInfo> getUserCheckList(Map<String, String> map);
}