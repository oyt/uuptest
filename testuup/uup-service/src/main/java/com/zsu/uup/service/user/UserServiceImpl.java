/**
 * oyt
 */
package com.zsu.uup.service.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsu.uup.UserService;
import com.zsu.uup.Mapper.TestInfoDtoMapper;
import com.zsu.uup.entity.TestInfoDto;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * <font size=0.25>Copyright (C) 2018 Ouyeel. All Rights Reserved.</font>
 * @author Tao.Ouyang (Create on:2018年6月5日)
 * @version 1.0
 * @fileName UserServiceImpl.java
 */
@Service(protocol = { "dubbo" }, timeout = 60000)
public class UserServiceImpl implements UserService {

    @Autowired
    public TestInfoDtoMapper testInfoDtoMapper;

    @Override
    @Transactional
    public List<String> getListUserinfo() {
        List<String> list = new ArrayList<String>();
        testInfoDtoMapper.selectAll();

        return list;
    }

    public PageInfo<TestInfoDto> getListUser(Map<String, String> map) {
        PageHelper.startPage(1, 10);
        Page<TestInfoDto> page = testInfoDtoMapper.gettestinfo(map);
        PageInfo<TestInfoDto> pageinfo = new PageInfo<TestInfoDto>(1, 10, page.getTotal(), page.getResult());
        return pageinfo;

    }

    @Override
    public String getUsername(String name) {
        return null;
    }
}
