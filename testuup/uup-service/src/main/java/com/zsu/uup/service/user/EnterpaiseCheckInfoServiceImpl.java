package com.zsu.uup.service.user;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsu.uup.EnterpriseCheckInfoService;
import com.zsu.uup.Mapper.EnterpriseCheckInfoMapper;
import com.zsu.uup.entity.EnterpriseCheckInfo;
import com.zsu.uup.pageinfo.PageInfo;

/**
 * 
 * <p>
 * Title: EnterpaiseCheckInfoServiceImpl(企业资料审核)
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Kouxu (Create on:2018年6月20日)
 * @version 1.0
 * @fileName EnterpaiseCheckInfoServiceImpl.java
 */
@Service(protocol = { "dubbo" }, timeout = 60000)
public class EnterpaiseCheckInfoServiceImpl implements
		EnterpriseCheckInfoService {

	@Autowired
	private EnterpriseCheckInfoMapper enterpriseCheckMapper;

	//企业资料审核    未审核
	@Override
	public PageInfo<EnterpriseCheckInfo> getEnterpriseCheckList(
			Map<String, String> map) {
		// TODO Auto-generated method stub
		PageHelper.startPage(0, 10);
		Page<EnterpriseCheckInfo> page = enterpriseCheckMapper
				.getEnterpriseCheckList(map);
		PageInfo<EnterpriseCheckInfo> pageInfo = new PageInfo<EnterpriseCheckInfo>(
				0, 10, page.getTotal(), page.getResult());
		return pageInfo;
	}
	//企业资料审核    已审核
	@Override
	public PageInfo<EnterpriseCheckInfo> getEnterpriseCheckListEnd(
			Map<String, String> map) {
		// TODO Auto-generated method stub
		PageHelper.startPage(0, 10);
		Page<EnterpriseCheckInfo> page = enterpriseCheckMapper.getEnterpriseCheckListEnd(map);
		PageInfo<EnterpriseCheckInfo> pageInfo = new PageInfo<EnterpriseCheckInfo>(0, 10, page.getTotal(), page.getResult());
		return pageInfo;
	}
	//根据ID获取企业提供的审核资料详情
	@Override
	public List<EnterpriseCheckInfo> getEnterpriseCheckSomethingById(Integer id) {
		// TODO Auto-generated method stub
		return enterpriseCheckMapper.getEnterpriseCheckSomethingById(id);
	}
	

}
