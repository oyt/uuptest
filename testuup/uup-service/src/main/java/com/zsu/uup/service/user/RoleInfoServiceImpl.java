package com.zsu.uup.service.user;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsu.uup.RoleInfoService;
import com.zsu.uup.Mapper.RoleInfoDtoMapper;
import com.zsu.uup.entity.RoleInfoDto;
import com.zsu.uup.entity.TestInfoDto;
import com.zsu.uup.pageinfo.PageInfo;

@Service(protocol = { "dubbo" }, timeout = 60000)
public class RoleInfoServiceImpl implements RoleInfoService {
	
	@Autowired
	public RoleInfoDtoMapper roleInfoDtoMapper;
    /**
     * 查询角色列表
     */
	@Override
	public List<RoleInfoDto> selectAllRoleInfo() {
		return roleInfoDtoMapper.selectAllRoleInfo();
	}
	/**
	 * 添加角色名称
	 */
	@Override
	public int insertRoleInfo(RoleInfoDto roleInfoDto) {
		return roleInfoDtoMapper.insertRoleInfo(roleInfoDto);
	}
	/**
	 * uuid查询单个用户角色
	 */
	@Override
	public RoleInfoDto selectRoleByUUId(String uuid) {
		return roleInfoDtoMapper.selectRoleByUUId(uuid);
	}
	/**
	 * 更新角色
	 */
	@Override
	public int updateRoleInfo(RoleInfoDto roleInfoDto) {
		return roleInfoDtoMapper.updateRoleInfo(roleInfoDto);
	}
	/**
	 * 删除角色
	 */
	@Override
	public int deleteRoleInfo(RoleInfoDto roleInfoDto) {
		return roleInfoDtoMapper.deleteRoleInfo(roleInfoDto);
	}
	/**
	 * 角色列表分页
	 */
	@Override
	public PageInfo<RoleInfoDto> getRoleInfoList(Map<String, String> map) {
		PageHelper.startPage(1, 10);
        Page<RoleInfoDto> page = roleInfoDtoMapper.getRoleInfoList(map);
        PageInfo<RoleInfoDto> pageinfo = new PageInfo<RoleInfoDto>(1, 10, page.getTotal(), page.getResult());
		return pageinfo;
	}
	/**
	 * 角色添加权限
	 */
	@Override
	public int addRoleAuth(Map<String, Object> roleAuth) {
		return roleInfoDtoMapper.addRoleAuth(roleAuth);
	}
	/**
	 * 删除角色权限
	 */
	@Override
	public int deleteRoleAuth(String uuid) {
		return roleInfoDtoMapper.deleteRoleAuth(uuid);
	}

}
